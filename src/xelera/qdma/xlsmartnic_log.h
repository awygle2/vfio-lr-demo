/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright(c) 2022 Xelera Technologies GmbH
 */
#ifndef _BUILD_NATIVE_XLSMARTNIC_LOG_H
#define _BUILD_NATIVE_XLSMARTNIC_LOG_H

#include <rte_log.h>

extern int xlsmartnic_logtype_driver;
#define PMD_DRV_LOG(level, fmt, args...) \
    rte_log(RTE_LOG_##level, xlsmartnic_logtype_driver, "%s(): " fmt "\n", __func__, ##args)

#define PMD_DRV_DEV_LOG(dev, level, fmt, args...) \
    rte_log(RTE_LOG_##level, xlsmartnic_logtype_driver, "%s|%s(): " fmt "\n", dev->name, __func__, ##args)

#ifdef XLSMARTNIC_DEBUG_LOG
#define PMD_DEBUG_LOG(fmt, args...) PMD_DRV_LOG(DEBUG, fmt, ##args)
#else
#define PMD_DEBUG_LOG(fmt, args...) \
    do {                            \
    } while (0)
#endif

#endif  //_BUILD_NATIVE_XLSMARTNIC_LOG_H

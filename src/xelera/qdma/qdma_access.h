/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright(c) 2022 Xelera Technologies GmbH
 */
#ifndef _QDMA_ACCESS_H_
#define _QDMA_ACCESS_H_

#include "qdma_common.h"
#include "qdma_registers.h"

#include <qdma/xlsmartnic_log.h>
#include <qdma/xlsmartnic_regs.h>

enum ind_ctxt_cmd_op
{
    QDMA_CTXT_CMD_CLR,
    QDMA_CTXT_CMD_WR,
    QDMA_CTXT_CMD_RD,
    QDMA_CTXT_CMD_INV
};

enum ind_ctxt_cmd_sel
{
    QDMA_CTXT_SEL_SW_C2H,
    QDMA_CTXT_SEL_SW_H2C,
    QDMA_CTXT_SEL_HW_C2H,
    QDMA_CTXT_SEL_HW_H2C,
    QDMA_CTXT_SEL_CR_C2H,
    QDMA_CTXT_SEL_CR_H2C,
    QDMA_CTXT_SEL_CMPT,
    QDMA_CTXT_SEL_PFTCH,
    QDMA_CTXT_SEL_INT_COAL,
    QDMA_CTXT_SEL_RESERVED,
    QDMA_CTXT_SEL_HOST_PROFILE,
    QDMA_CTXT_SEL_TIMER,
    QDMA_CTXT_SEL_FMAP,
};

enum data_dir
{
    QDMA_C2H,
    QDMA_H2C
};


/**
 * Call definition for reading register space. Should return 0 on success and a negativ value as error code
 * Not being able to read exactly the requested amount of bytes is considered a hard error.
 */
typedef int (*pfn_read_direct_reg_t)(void* usr_ctx, uint32_t reg, uint32_t* data_ptr, uint32_t num_words);

/**
 * Call definition for writing register space. Should return 0 on success and a negativ value as error code
 * Not being able to write exactly the requested amount of bytes is considered a hard error.
 */
typedef int (*pfn_write_direct_reg_t)(void* usr_ctx, uint32_t reg, const uint32_t* data_ptr, uint32_t num_words);

/**
 * Call definition for polling a register.
 * Should return:
 * - 1 on succesfully reached value
 * - 0 on timeout
 * - < 0 on error
 */
typedef int (*pfn_poll_direct_reg_t)(void* usr_ctx, uint32_t reg, uint32_t mask, uint32_t value, uint32_t timeout_us);

typedef void (*pfn_lock_access_t)(void* usr_ctx);

typedef void (*pfn_unlock_access_t)(void* usr_ctx);

struct qdma_access_ctx
{
    void* usr_ctx;

    pfn_read_direct_reg_t  read_direct_reg_func;
    pfn_write_direct_reg_t write_direct_reg_func;
    pfn_poll_direct_reg_t  poll_direct_reg_func;
    pfn_lock_access_t lock_access_func;
    pfn_unlock_access_t unlock_access_func;
};

/* ------------------------ indirect register context fields -----------*/
union qdma_ind_ctxt_cmd
{
    uint32_t word;
    struct
    {
        uint32_t busy : 1;
        uint32_t sel : 4;
        uint32_t op : 2;
        uint32_t qid : 11;
        uint32_t rsvd : 14;
    } bits;
};

/**
 * struct qdma_indirect_ctxt_regs - Inirect Context programming registers
 */
struct qdma_indirect_ctxt_regs
{
    uint32_t                qdma_ind_ctxt_data[QDMA_REG_IND_CTXT_REG_COUNT];
    uint32_t                qdma_ind_ctxt_mask[QDMA_REG_IND_CTXT_REG_COUNT];
    union qdma_ind_ctxt_cmd cmd;
};

/**
 * @brief      Reads the version of type of QDMA endpoint.
 *
 * @param      access_ctx  The access context
 * @param      info        The information
 *
 * @return     { description_of_the_return_value }
 */
int qdma_read_hw_info(struct qdma_access_ctx* access_ctx, struct qdma_hw_info* info);


const char* qdma_type_str(enum qdma_type type);

int qdma_get_c2h_writeback_info(struct qdma_access_ctx* access_ctx, struct qdma_c2h_writeback_info* writeback_info);

/**
 * @brief      Brings the QDMA endpoint in a default state from which further initialization can be done
 *
 * @param      access_ctx  The access context
 * @param[in]  info        The information
 *
 * @return     { description_of_the_return_value }
 */
int qdma_base_init(struct qdma_access_ctx* access_ctx, const struct qdma_hw_info* info);

int qdma_clear_ctx_reg(struct qdma_access_ctx* access_ctx, uint32_t base_queue, uint32_t num_queues);

int qdma_read_global_ring_size_reg(struct qdma_access_ctx* access_ctx, uint32_t* size_array, int num_words);

int qdma_read_c2h_buffer_size_reg(struct qdma_access_ctx* access_ctx, uint32_t* size_array, int num_words);

int qdma_read_c2h_thresh_value_reg(struct qdma_access_ctx* access_ctx, uint32_t* size_array, int num_words);

int qdma_read_c2h_timer_thresh_reg(struct qdma_access_ctx* access_ctx, uint32_t* size_array, int num_words);

int qdma_global_irq_enable(struct qdma_access_ctx* access_ctx, bool state);


int qdma_init_error_reporting(struct qdma_access_ctx* access_ctx);

const struct qdma_error_descriptor* qdma_get_err_descriptor(int id);

int qdma_read_global_error_status(struct qdma_access_ctx* access_ctx, union qdma_glbl_err_stat* glbl_err_stat, bool clear);

int qdma_read_errors(struct qdma_access_ctx* access_ctx, struct qdma_error_vector* error_vector);

int qdma_query_sw_descr_ctx(struct qdma_access_ctx*   access_ctx,
                            enum data_dir             dir,
                            uint16_t                  queue_id,
                            struct qdma_sw_descr_ctx* sw_descr_ctx);

int qdma_query_hw_descr_ctx(struct qdma_access_ctx*   access_ctx,
                            enum data_dir             dir,
                            uint16_t                  queue_id,
                            struct qdma_hw_descr_ctx* hw_descr_ctx);

int qdma_query_credit_descr_ctx(struct qdma_access_ctx*       access_ctx,
                                enum data_dir                 dir,
                                uint16_t                      queue_id,
                                struct qdma_credit_descr_ctx* credit_descr_ctx);

int qdma_query_prefetch_ctx(struct qdma_access_ctx*     access_ctx,
                              uint16_t                    queue_id,
                              struct qdma_prefetch_ctx* prefetch_ctx);


int qdma_query_completion_ctx(struct qdma_access_ctx*     access_ctx,
                              uint16_t                    queue_id,
                              struct qdma_completion_ctx* completion_ctx);

int qdma_query_function_map_ctx(struct qdma_access_ctx*       access_ctx,
                                uint16_t                      queue_id,
                                struct qdma_function_map_ctx* function_map_ctx);

int qdma_clear_sw_descr_ctx(struct qdma_access_ctx*         access_ctx,
                          enum data_dir                   dir,
                          uint16_t                        queue_id);

int qdma_clear_hw_descr_ctx(struct qdma_access_ctx*         access_ctx,
                          enum data_dir                   dir,
                          uint16_t                        queue_id);

int qdma_clear_credit_descr_ctx(struct qdma_access_ctx*         access_ctx,
                          enum data_dir                   dir,
                          uint16_t                        queue_id);

int qdma_clear_prefetch_descr_ctx(struct qdma_access_ctx*         access_ctx,
                          uint16_t                        queue_id);

int qdma_clear_completion_descr_ctx(struct qdma_access_ctx*         access_ctx,
                          uint16_t                        queue_id);

int qdma_set_sw_descr_ctx(struct qdma_access_ctx*         access_ctx,
                          enum data_dir                   dir,
                          uint16_t                        queue_id,
                          const struct qdma_sw_descr_ctx* sw_descr_ctx);

int qdma_set_hw_descr_ctx(struct qdma_access_ctx*         access_ctx,
                          enum data_dir                   dir,
                          uint16_t                        queue_id,
                          const struct qdma_hw_descr_ctx* hw_descr_ctx);

int qdma_set_credit_descr_ctx(struct qdma_access_ctx*             access_ctx,
                              enum data_dir                       dir,
                              uint16_t                            queue_id,
                              const struct qdma_credit_descr_ctx* credit_descr_ctx);

int qdma_set_prefetch_ctx(struct qdma_access_ctx*     access_ctx,
                              uint16_t                    queue_id,
                              const struct qdma_prefetch_ctx* prefetch_ctx);

int qdma_set_completion_ctx(struct qdma_access_ctx*           access_ctx,
                            uint16_t                          queue_id,
                            const struct qdma_completion_ctx* completion_ctx);

int qdma_set_function_map_ctx(struct qdma_access_ctx*             access_ctx,
                              uint16_t                            queue_id,
                              const struct qdma_function_map_ctx* function_map_ctx);

int qdma_function_level_reset(struct qdma_access_ctx* access_ctx, uint32_t timeout_us);

int qdma_set_global_descr_config(struct qdma_access_ctx* access_ctx, enum qdma_global_writeback_interval wb_intvl, uint8_t max_fetch, bool ctxt_fer_dis);

int qdma_set_h2c_pcie_throt(struct qdma_access_ctx* access_ctx, uint32_t data_thresh, bool req_throt_en_data, uint16_t req_throt, bool req_throt_en_req);

int qdma_config_prefetch_engine(struct qdma_access_ctx* access_ctx, uint32_t evt_qcnt_th, uint32_t pfch_qcnt, uint16_t num_pfch, uint16_t pfch_fl_th);

int qdma_config_global_tickrate(struct qdma_access_ctx* access_ctx, uint32_t tickrate_val);

int qdma_config_c2h_wb_coal(struct qdma_access_ctx* access_ctx, uint16_t tick_val, uint16_t tick_count);

int qdma_config_pcie_access_width(struct qdma_access_ctx* access_ctx, enum qdma_pcie_access_width max_read_width, enum qdma_pcie_access_width max_write_width);

int qdma_config_pcie_ctrl(struct qdma_access_ctx* access_ctx, bool enable_relaxed_ordering);

#if 1
static __inline void qdma_update_h2c_pidx(_ioptr_t dmap_base_ioptr, uint16_t queue_id, uint16_t pidx, bool irq_arm) {

    uint32_t reg_offset = QDMA_DMAP_SEL_H2C_DSC_PIDX + (queue_id * QDMA_DMAP_REG_QUEUE_OFFSET);

    uint32_t reg_value = 0;

    volatile uint32_t* reg_dst = ((volatile uint32_t*)(dmap_base_ioptr + reg_offset));

    reg_value |= (pidx & 0xffff);

     PMD_DRV_LOG(INFO, "Writing 0x%08x to register 0x%x", reg_value, reg_offset);
    *reg_dst = reg_value;
}

static __inline void qdma_update_c2h_pidx(_ioptr_t dmap_base_ioptr, uint16_t queue_id, uint16_t pidx, bool irq_arm) {

    uint32_t reg_offset = QDMA_DMAP_SEL_C2H_DSC_PIDX + (queue_id << QDMA_DMAP_RAG_QUEUE_SHIFT);

    uint32_t reg_value = 0;

    volatile uint32_t* reg_dst = ((volatile uint32_t*)(dmap_base_ioptr + reg_offset));

    reg_value |= (pidx & 0xffff);

     PMD_DRV_LOG(INFO, "Writing 0x%08x to register 0x%x", reg_value, reg_offset);
    *reg_dst = reg_value;
}

static __inline void qdma_update_c2h_completion_cidx(_ioptr_t dmap_base_ioptr, uint16_t queue_id, uint16_t cidx, uint16_t trg_upd_word) {

    uint32_t reg_offset = QDMA_DMAP_SEL_C2H_CMPT_CIDX + (queue_id << QDMA_DMAP_RAG_QUEUE_SHIFT);

    uint32_t reg_value;

    volatile uint32_t* reg_dst = ((volatile uint32_t*)(dmap_base_ioptr + reg_offset));

    reg_value  = (cidx & 0xffff);

    // Not used since full_upd ist not set in completion context
    //reg_value |= ((uint32_t)trg_upd_word) << 16;

     PMD_DRV_LOG(INFO, "Writing 0x%08x to register 0x%x", reg_value, reg_offset);
    *reg_dst = reg_value;
}
#else
void qdma_update_h2c_pidx(_ioptr_t dmap_base_ioptr, uint16_t queue_id, uint16_t pidx, bool irq_arm);
void qdma_update_c2h_pidx(_ioptr_t dmap_base_ioptr, uint16_t queue_id, uint16_t pidx, bool irq_arm);
void qdma_update_c2h_completion_cidx(_ioptr_t dmap_base_ioptr, uint16_t queue_id, uint16_t cidx, bool irq_arm, bool enable_status_descr);

#endif

#endif  // _QDMA_ACCESS_H_

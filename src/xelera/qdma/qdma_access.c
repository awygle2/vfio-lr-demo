/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright(c) 2022 Xelera Technologies GmbH
 */
#include <asm-generic/errno.h>
#include <qdma/qdma_access.h>

#include <xlsmartnic_log.h>
#include <xlsmartnic_regs.h>

#include <string.h>


// TODO: Get rid of dpdk header. This one is used for rte_strerror which should be replaced by custom function
#include "qdma/qdma_common.h"
#include "rte_errno.h"

RTE_LOG_REGISTER(xlsmartnic_logtype_driver, pmd.net.xlsmartnic, DEBUG);

#define QDMA_C2H_DEFAULT_COUNTER_THRESH 64
#define QDMA_C2H_DEFAULT_TIMER_VALUE    100

static const char* qdma_error_group_labels[TOTAL_LEAF_ERROR_AGGREGATORS + 1] = {"Global Error Bits",
                                                                                "Descriptor Error",
                                                                                "C2H Stream Error",
                                                                                "Fatal Stream Error",
                                                                                "H2C Stream Error",
                                                                                "TRQ Error",
                                                                                "Single Bit Error",
                                                                                "Double Bit Error"};


static const uint32_t qdma_error_group_descriptor_offsets[TOTAL_LEAF_ERROR_AGGREGATORS + 1] = {
    0,
    0,
    QDMA_NUM_ERROS_GRP_DSC,
    QDMA_NUM_ERROS_GRP_DSC + QDMA_NUM_ERROS_GRP_ST_C2H,
    QDMA_NUM_ERROS_GRP_DSC + QDMA_NUM_ERROS_GRP_ST_C2H + QDMA_NUM_ERROS_GRP_ST_FATAL,
    QDMA_NUM_ERROS_GRP_DSC + QDMA_NUM_ERROS_GRP_ST_C2H + QDMA_NUM_ERROS_GRP_ST_FATAL + QDMA_NUM_ERROS_GRP_ST_H2C,
    QDMA_NUM_ERROS_GRP_DSC + QDMA_NUM_ERROS_GRP_ST_C2H + QDMA_NUM_ERROS_GRP_ST_FATAL + QDMA_NUM_ERROS_GRP_ST_H2C +
        QDMA_NUM_ERROS_GRP_TRQ,
    0,
};

static const uint32_t qdma_error_group_descriptor_length[TOTAL_LEAF_ERROR_AGGREGATORS + 1] = {
    0,
    QDMA_NUM_ERROS_GRP_DSC,
    QDMA_NUM_ERROS_GRP_ST_C2H,
    QDMA_NUM_ERROS_GRP_ST_FATAL,
    QDMA_NUM_ERROS_GRP_ST_H2C,
    QDMA_NUM_ERROS_GRP_TRQ,
    0,
    0};


// clang-format off
static const struct qdma_error_descriptor error_descriptors[] = {
    {0, QDMA_ERR_GRP_DSC, "poison", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x00000002},
    {0, QDMA_ERR_GRP_DSC, "unsupported request or abort", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x00000004},
    {0, QDMA_ERR_GRP_DSC, "unexpected byte count", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x00000008},
    {0, QDMA_ERR_GRP_DSC, "parameter mismatch", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x00000010},
    {0, QDMA_ERR_GRP_DSC, "address mismatch", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x00000020},
    {0, QDMA_ERR_GRP_DSC, "unexpected tag", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x00000040},
    {0, QDMA_ERR_GRP_DSC, "FLR error", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x00000100},
    {0, QDMA_ERR_GRP_DSC, "timeout", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x00000200},
    {0, QDMA_ERR_GRP_DSC, "poison data", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x10000},
    {0, QDMA_ERR_GRP_DSC, "flr cancel", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x80000},
    {0, QDMA_ERR_GRP_DSC, "DMA error", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x100000},
    {0, QDMA_ERR_GRP_DSC, "queue update error", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x200000},
    {0, QDMA_ERR_GRP_DSC, "descriptor fetch cancelled", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x400000},
    {0, QDMA_ERR_GRP_DSC, "double bit error", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x800000},
    {0, QDMA_ERR_GRP_DSC, "single bit error", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x1000000},
    {0, QDMA_ERR_GRP_DSC, "port id mismatch", QDMA_OFFSET_GLBL_DSC_ERR_STAT, QDMA_OFFSET_GLBL_DSC_ERR_MASK, 0x2000000},

    {0, QDMA_ERR_GRP_ST_C2H, "MTY mismatch", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x1},
    {0, QDMA_ERR_GRP_ST_C2H, "len mismatch", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x2},
    {0, QDMA_ERR_GRP_ST_C2H, "shared completion queue descriptor error", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x4},
    {0, QDMA_ERR_GRP_ST_C2H, "qid mismatch", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x8},
    {0, QDMA_ERR_GRP_ST_C2H, "descriptor error bit set", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x10},
    {0, QDMA_ERR_GRP_ST_C2H, "c2h crc error", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x40},
    {0, QDMA_ERR_GRP_ST_C2H, "MSIX FAIL response", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x80},
    {0, QDMA_ERR_GRP_ST_C2H, "descriptor count per packet > 7", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x200},
    {0, QDMA_ERR_GRP_ST_C2H, "port id mismatch", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x400},
    {0, QDMA_ERR_GRP_ST_C2H, "invalidated queue updated", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x1000},
    {0, QDMA_ERR_GRP_ST_C2H, "completion queue full", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x2000},
    {0, QDMA_ERR_GRP_ST_C2H, "bad consumer index", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x4000},
    {0, QDMA_ERR_GRP_ST_C2H, "parity error", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x8000},
    {0, QDMA_ERR_GRP_ST_C2H, "descriptor with error fetched", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x10000},
    {0, QDMA_ERR_GRP_ST_C2H, "multi bit ecc error", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x20000},
    {0, QDMA_ERR_GRP_ST_C2H, "single bit ecc error", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x40000},
    {0, QDMA_ERR_GRP_ST_C2H, "header parity error", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x80000},
    {0, QDMA_ERR_GRP_ST_C2H, "completion engine port mismatch", QDMA_OFFSET_C2H_ERR_STAT, QDMA_OFFSET_C2H_ERR_MASK, 0x100000},

    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: MTY mismatch", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x0},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: len mismatch", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x1},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: qid mismatch", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x8},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: timer fifo ram dbe", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0xf0},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: prefetch ram dbe", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x100},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: writeback context ram dbe", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x200},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: prefetch context ram dbe", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x400},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: descriptor request fifo ram dbe", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x800},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: interrupt context ram dbe", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x1000},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: writeback coalescence data ram dbe", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x4000},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: completion fifo ram dbe", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x8000},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: qid fifo ram dbe", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x10000},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: payload fifo ram dbe", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x20000},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: wpl data parity error", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x40000},
    {0, QDMA_ERR_GRP_ST_FATAL, "FATAL: avl ring fifo ram dbe", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x80000},
    {0,QDMA_ERR_GRP_ST_FATAL,"FATAL: c2h packet header ecc error", QDMA_OFFSET_C2H_FATAL_ERR_STAT, QDMA_OFFSET_C2H_FATAL_ERR_MASK, 0x100000},

    {0, QDMA_ERR_GRP_ST_H2C, "zero length descriptor", QDMA_OFFSET_H2C_ERR_STAT, QDMA_OFFSET_H2C_ERR_MASK, 0x1},
    {0, QDMA_ERR_GRP_ST_H2C, "non eop descriptor received with either sdi or mkr set", QDMA_OFFSET_H2C_ERR_STAT, QDMA_OFFSET_H2C_ERR_MASK, 0x2},
    {0, QDMA_ERR_GRP_ST_H2C, "no dma descr received with either SOP or EOP", QDMA_OFFSET_H2C_ERR_STAT, QDMA_OFFSET_H2C_ERR_MASK, 0x4},
    {0, QDMA_ERR_GRP_ST_H2C, "double bit error", QDMA_OFFSET_H2C_ERR_STAT, QDMA_OFFSET_H2C_ERR_MASK, 0x8},
    {0, QDMA_ERR_GRP_ST_H2C, "single bit error", QDMA_OFFSET_H2C_ERR_STAT, QDMA_OFFSET_H2C_ERR_MASK, 0x10},
    {0, QDMA_ERR_GRP_ST_H2C, "internal parity error", QDMA_OFFSET_H2C_ERR_STAT, QDMA_OFFSET_H2C_ERR_MASK, 0x20},

    {0, QDMA_ERR_GRP_TRQ, "csr unmapped", QDMA_OFFSET_GLBL_TRQ_ERR_STAT, QDMA_OFFSET_GLBL_TRQ_ERR_MASK, 0x1},
    {0, QDMA_ERR_GRP_TRQ, "vf access error", QDMA_OFFSET_GLBL_TRQ_ERR_STAT, QDMA_OFFSET_GLBL_TRQ_ERR_MASK, 0x2},
    {0, QDMA_ERR_GRP_TRQ, "tcp csr timeout", QDMA_OFFSET_GLBL_TRQ_ERR_STAT, QDMA_OFFSET_GLBL_TRQ_ERR_MASK, 0x8},
    {0, QDMA_ERR_GRP_TRQ, "qspc unmapped", QDMA_OFFSET_GLBL_TRQ_ERR_STAT, QDMA_OFFSET_GLBL_TRQ_ERR_MASK, 0x10},
    {0, QDMA_ERR_GRP_TRQ, "qid range error", QDMA_OFFSET_GLBL_TRQ_ERR_STAT, QDMA_OFFSET_GLBL_TRQ_ERR_MASK, 0x20},
    {0, QDMA_ERR_GRP_TRQ, "qtcp qspc timeout", QDMA_OFFSET_GLBL_TRQ_ERR_STAT, QDMA_OFFSET_GLBL_TRQ_ERR_MASK, 0x80},


    {-1, QDMA_ERR_GRP_NONE, NULL, 0, 0, 0}};

// clang-format on


static const uint32_t default_ring_sizes[QDMA_GLOBAL_RING_SIZE_CFG_COUNT] = {
    65, 129, 193, 257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385};

static const uint32_t default_buffer_sizes[QDMA_C2H_BUFFER_SIZE_CFG_COUNT] = {
    256, 512, 512, 512, 512, 512, 512, 512, 512, 512, 1024, 2048, 4096, 4096, 4096, 4096};

//static const uint32_t default_buffer_sizes[QDMA_C2H_BUFFER_SIZE_CFG_COUNT] = {
//    256, 512, 512, 512, 512, 512, 512, 512, 512, 512, 1024, 2048, 4096, 8192, 9018, 16384};

static const uint32_t default_counter_thresh_values[QDMA_C2H_COUNT_THRESH_CFG_COUNT] = {
    2, 4, 8, 16, 24, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192};

static const uint32_t default_timer_thresh_values[QDMA_C2H_TIMER_THRESH_CFG_COUNT] = {
    1, 2, 4, 5, 8, 10, 15, 20, 25, 30, 50, 75, 100, 125, 150, 200};

union qdma_cfg_ident_info
{
    uint32_t word;

    struct __attribute__((packed))
    {
        uint8_t  version;
        uint8_t  reserved;
        uint16_t cfg_ident  : 4;
        uint16_t identifier : 12;
    };
};

union qdma_system_block_id_info
{
    uint32_t word;

    struct __attribute__((packed))
    {
        uint16_t system_id;
        uint16_t hard_ip_flag : 1;
        uint16_t reserved     : 15;
    };
};

const struct qdma_error_descriptor* qdma_get_err_descriptor(int id) {
    return &error_descriptors[id];
}

const char* qdma_get_err_group_name(uint32_t group_id) {
    if (group_id <= QDMA_ERR_GRP_DBE) {
        return qdma_error_group_labels[group_id];
    } else {
        return "Unknown Error Group";
    }
}


static int qdma_clear_indirect(struct qdma_access_ctx* access_ctx, enum ind_ctxt_cmd_sel cmd_sel, uint16_t queue_id);

static int qdma_read_indirect(struct qdma_access_ctx* access_ctx,
                              enum ind_ctxt_cmd_sel   cmd_sel,
                              uint16_t                queue_id,
                              void*                   data_ptr,
                              uint32_t                num_words);

static int qdma_write_indirect(struct qdma_access_ctx* access_ctx,
                               enum ind_ctxt_cmd_sel   cmd_sel,
                               uint16_t                queue_id,
                               const void*             data_ptr,
                               uint32_t                num_words);


static int qdma_decode_ip_type(enum qdma_type* type, uint32_t blk_sysid) {
    //bool is_hard_ip = (blk_sysid & QDMA_CFG_BLOCK_SYS_ID_HARD_IP_BIT);

    uint16_t sys_id = (uint16_t) (blk_sysid & QDMA_CFG_BLOCK_SYS_ID_MASK);

    *type = QDMA_TYPE_INVALID;

    PMD_DRV_LOG(NOTICE, "QDMA sysid: 0x%02x", sys_id);

    return 0;
}

int qdma_read_hw_info(struct qdma_access_ctx* access_ctx, struct qdma_hw_info* info) {
    union qdma_cfg_ident_info ident_info;

    union qdma_system_block_id_info block_id_info;

    union qdma_glbl2_channel_inst channel_inst_info;
    union qdma_glbl2_channel_mdma channel_mdma_info;

    uint32_t misc_cap = 0;

    uint32_t channel_qdma_queue_caps = 0;

    int ret;

    ret = access_ctx->read_direct_reg_func(
        access_ctx->usr_ctx, QDMA_CFG_BLOCK_IDENTIFIER, &ident_info.word, QDMA_WORD_COUNT(ident_info));

    if (ret) {
        return ret;
    }

    if ((ident_info.word & 0xffff0000) >> 16 != QDMA_CFG_BLOCK_ID_MAGIC) {  // QDMA magic number
        PMD_DRV_LOG(ERR, "QDMA magic number not found in BLOCK_IDENTIFIER");

        return -EINVAL;
    }

    ret = access_ctx->read_direct_reg_func(
        access_ctx->usr_ctx, QDMA_CFG_BLOCK_SYSID, &block_id_info.word, QDMA_WORD_COUNT(block_id_info));

    if (ret) {
        return ret;
    }

    ret = access_ctx->read_direct_reg_func(
        access_ctx->usr_ctx, QDMA_GLBL2_MISC_CAP, &misc_cap, QDMA_WORD_COUNT(misc_cap));

    if (ret) {
        return ret;
    }

    ret = qdma_decode_ip_type(&info->type, block_id_info.word);

    if (ret) {
        return ret;
    }

    info->version    = ident_info.version;
    info->config_id  = ident_info.cfg_ident;
    info->identifier = ident_info.identifier;
    info->system_id  = block_id_info.system_id;
    info->is_hard_ip = block_id_info.hard_ip_flag;

    ret = access_ctx->read_direct_reg_func(
        access_ctx->usr_ctx, QDMA_GLBL2_CHANNEL_INST_REG, &channel_inst_info.word, QDMA_WORD_COUNT(channel_inst_info));

    if (ret) {
        return ret;
    }

    ret = access_ctx->read_direct_reg_func(
        access_ctx->usr_ctx, QDMA_GLBL2_CHANNEL_MDMA, &channel_mdma_info.word, QDMA_WORD_COUNT(channel_mdma_info));

    if (ret) {
        return ret;
    }

    ret = access_ctx->read_direct_reg_func(access_ctx->usr_ctx,
                                           QDMA_GLBL2_CHANNEL_QDMA_CAP,
                                           &channel_qdma_queue_caps,
                                           QDMA_WORD_COUNT(channel_qdma_queue_caps));

    if (ret) {
        return ret;
    }

    info->capas.has_c2h_mm     = channel_inst_info.c2h_mm_is_inst ? true : false;
    info->capas.has_h2c_mm     = channel_inst_info.h2c_mm_is_inst ? true : false;
    info->capas.has_c2h_stream = channel_inst_info.c2h_st_is_inst ? true : false;
    info->capas.has_h2c_stream = channel_inst_info.h2c_st_is_inst ? true : false;

    // Since we only support QDMA for now we treat a non QDMA DMA as non existing DMA.

    if (channel_mdma_info.c2h_mm_is_qdma == 0) {
        info->capas.has_c2h_mm = false;
    }

    if (channel_mdma_info.h2c_mm_is_qdma == 0) {
        info->capas.has_h2c_mm = false;
    }

    if (channel_mdma_info.c2h_st_is_qdma == 0) {
        info->capas.has_c2h_stream = false;
    }

    if (channel_mdma_info.h2c_st_is_qdma == 0) {
        info->capas.has_h2c_stream = false;
    }

    info->capas.max_num_supported_queues = (channel_qdma_queue_caps & 0x00fffU);

    return 0;
}

const char* qdma_type_str(enum qdma_type type) {
    switch (type) {
        case QDMA_TYPE_INVALID:
            return "invalid";
        case QDMA_TYPE_SOFT_IP_QDMA:
            return "soft_ip_qdma";
        case QDMA_TYPE_SOFT_IP_EQDMA:
            return "soft_ip_eqdma";
        case QDMA_TYPE_VERSAL_SOFT_IP:
            return "versal_soft_ip";
        case QDMA_TYPE_VERSAL_HARD_IP:
            return "versal_hard_ip";
        default:
            return "unknown type";
    }
}

int qdma_get_c2h_writeback_info(struct qdma_access_ctx* access_ctx, struct qdma_c2h_writeback_info* writeback_info) {

    // TODO: Maybe read values back from the registers?

    writeback_info->min_writeback_thresh = default_counter_thresh_values[0];
    writeback_info->max_writeback_thresh = default_counter_thresh_values[QDMA_C2H_COUNT_THRESH_CFG_COUNT - 1];

    writeback_info->min_writeback_timer_val = default_timer_thresh_values[0];
    writeback_info->max_writeback_timer_val = default_timer_thresh_values[QDMA_C2H_TIMER_THRESH_CFG_COUNT - 1];

    writeback_info->default_writeback_thresh    = QDMA_C2H_DEFAULT_COUNTER_THRESH;
    writeback_info->default_writeback_timer_val = QDMA_C2H_DEFAULT_TIMER_VALUE;

    return 0;
}

int qdma_base_init(uint8_t* access_ctx, const struct qdma_hw_info* info) {
    // Write host profile
    uint32_t                        host_profile_data[QDMA_REG_IND_CTXT_REG_COUNT];
    int                             ret;
    union qdma_c2h_pfch_cache_depth pfch_cache_depth;

    memset(host_profile_data, 0, sizeof(host_profile_data));

    PMD_DRV_LOG(NOTICE, "Initial QDMA initialization");


    PMD_DRV_LOG(NOTICE, "Writing host profile");

    ret =
        qdma_write_indirect(access_ctx, QDMA_CTXT_SEL_HOST_PROFILE, 0, host_profile_data, QDMA_REG_IND_CTXT_REG_COUNT);

    if (ret < 0) {
        PMD_DRV_LOG(ERR, "Writing host profile failed");
        return ret;
    }

    PMD_DRV_LOG(NOTICE, "Disabling interrupts");

    ret = qdma_global_irq_enable(access_ctx, false);

    if (ret) {
        PMD_DRV_LOG(ERR, "Disabling interrupts failed");
        return ret;
    }

    PMD_DRV_LOG(NOTICE, "Settings global ring size register");

    ret = access_ctx->write_direct_reg_func(
        access_ctx->usr_ctx, QDMA_GLOBAL_RING_SIZE_CFG_BASE, default_ring_sizes, QDMA_GLOBAL_RING_SIZE_CFG_COUNT);

    if (ret) {
        PMD_DRV_LOG(ERR, "Writing global ring size config failed");
        return ret;
    }

    PMD_DRV_LOG(NOTICE, "Settings C2H buffer size register");

    ret = access_ctx->write_direct_reg_func(access_ctx->usr_ctx,
                                            QDMA_C2H_BUFFER_SIZE_CFG_BASE_OFFSET,
                                            default_buffer_sizes,
                                            QDMA_C2H_BUFFER_SIZE_CFG_COUNT);

    if (ret) {
        PMD_DRV_LOG(ERR, "Writing C2H buffer size register failed");
        return ret;
    }

    PMD_DRV_LOG(NOTICE, "Settings counter threshold register");

    ret = access_ctx->write_direct_reg_func(access_ctx->usr_ctx,
                                            QDMA_C2H_COUNT_THRESH_CFG_BASE_OFFSET,
                                            default_counter_thresh_values,
                                            QDMA_C2H_COUNT_THRESH_CFG_COUNT);

    if (ret) {
        PMD_DRV_LOG(ERR, "Writing counter threshold register failed");
        return ret;
    }

    PMD_DRV_LOG(NOTICE, "Settings timer threshold register");

    ret = access_ctx->write_direct_reg_func(access_ctx->usr_ctx,
                                            QDMA_C2H_TIMER_THRESH_CFG_BASE_OFFSET,
                                            default_timer_thresh_values,
                                            QDMA_C2H_TIMER_THRESH_CFG_COUNT);

    if (ret) {
        PMD_DRV_LOG(ERR, "Writing timer threshold register failed");
        return ret;
    }

    access_ctx->read_direct_reg_func(access_ctx->usr_ctx,
                                     QDMA_C2H_PFCH_CACHE_DEPTH_BASE_OFFSET,
                                     &pfch_cache_depth.word,
                                     QDMA_WORD_COUNT(pfch_cache_depth));

    PMD_DRV_LOG(NOTICE, "Cache info: depth %u", pfch_cache_depth.cache_depth);

    ret = qdma_config_global_tickrate(access_ctx, 25);

    if (ret) {
        PMD_DRV_LOG(ERR, "Setting global tick rate failed %s", rte_strerror(ret));
        return ret;
    }
    //
    ret = qdma_set_global_descr_config(access_ctx, QDMA_DEFAULT_GLBL_WB_INTV, QDMA_DEFAULT_GLBL_MAX_DESC_FETCH, true);

    if (ret) {
        PMD_DRV_LOG(ERR, "Setting global descriptor fetch config failed %s", rte_strerror(ret));
        return ret;
    }
    //
    ret = qdma_set_h2c_pcie_throt(
        access_ctx, QDMA_DEFAULT_H2C_PCIE_THROT_DATA_THRESH, QDMA_DEFAULT_H2C_PCIE_THROT_EN_DATA, 256, false);

    if (ret) {
        PMD_DRV_LOG(ERR, "Setting h2c pcie throttling config failed %s", rte_strerror(ret));
        return ret;
    }

    //
    ret = qdma_config_prefetch_engine(
        access_ctx, (pfch_cache_depth.cache_depth / 2) - 2, (pfch_cache_depth.cache_depth / 2), 16, 256);

    if (ret) {
        PMD_DRV_LOG(ERR, "Setting prefetch engine config failed %s", rte_strerror(ret));
        return ret;
    }
    //
    ret = qdma_config_c2h_wb_coal(access_ctx, 25, 5);

    if (ret) {
        PMD_DRV_LOG(ERR, "Setting C2H writeback coalescing config failed %s", rte_strerror(ret));
        return ret;
    }

    ret = qdma_config_pcie_access_width(access_ctx, QDMA_ACCESS_WIDTH_1024, QDMA_ACCESS_WIDTH_1024);

    if (ret) {
        PMD_DRV_LOG(ERR, "Setting PCIe access width failed %s", rte_strerror(ret));
        return ret;
    }

    ret = qdma_config_pcie_ctrl(access_ctx, true);

    if (ret) {
        PMD_DRV_LOG(ERR, "Setting PCIe relaxed ordering on HW failed %s", rte_strerror(ret));
        return ret;
    }

    return 0;
}

int qdma_clear_ctx_reg(struct qdma_access_ctx* access_ctx, uint32_t base_queue, uint32_t num_queues) {
    uint32_t              index;
    enum ind_ctxt_cmd_sel indirect_reg_selector;

    PMD_DRV_LOG(NOTICE, "Clearing indirect register space");

    for (index = base_queue; index < (base_queue + num_queues); ++index) {
        for (indirect_reg_selector = QDMA_CTXT_SEL_SW_C2H; indirect_reg_selector <= QDMA_CTXT_SEL_INT_COAL;
             ++indirect_reg_selector) {
            if (qdma_clear_indirect(access_ctx, indirect_reg_selector, index) < 0) {
                PMD_DRV_LOG(
                    ERR, "Clearing context register sel %u, qid %ul failed", (uint32_t) indirect_reg_selector, index);

                return -1;
            }
        }
    }

    return 0;
}

int qdma_read_global_ring_size_reg(struct qdma_access_ctx* access_ctx, uint32_t* size_array, int num_words) {
    int ret;

    if (num_words > QDMA_GLOBAL_RING_SIZE_CFG_COUNT)
        return -EINVAL;

    ret = access_ctx->read_direct_reg_func(access_ctx->usr_ctx, QDMA_GLOBAL_RING_SIZE_CFG_BASE, size_array, num_words);

    return ret;
}

int qdma_read_c2h_buffer_size_reg(struct qdma_access_ctx* access_ctx, uint32_t* size_array, int num_words) {
    int ret;

    if (num_words > QDMA_C2H_BUFFER_SIZE_CFG_COUNT)
        return -EINVAL;

    ret = access_ctx->read_direct_reg_func(
        access_ctx->usr_ctx, QDMA_C2H_BUFFER_SIZE_CFG_BASE_OFFSET, size_array, num_words);

    return ret;
}

int qdma_read_c2h_thresh_value_reg(struct qdma_access_ctx* access_ctx, uint32_t* size_array, int num_words) {
    int ret;

    if (num_words > QDMA_C2H_COUNT_THRESH_CFG_COUNT)
        return -EINVAL;

    ret = access_ctx->read_direct_reg_func(
        access_ctx->usr_ctx, QDMA_C2H_COUNT_THRESH_CFG_BASE_OFFSET, size_array, num_words);

    return ret;
}

int qdma_read_c2h_timer_thresh_reg(struct qdma_access_ctx* access_ctx, uint32_t* size_array, int num_words) {
    int ret;

    if (num_words > QDMA_C2H_TIMER_THRESH_CFG_COUNT)
        return -EINVAL;

    ret = access_ctx->read_direct_reg_func(
        access_ctx->usr_ctx, QDMA_C2H_TIMER_THRESH_CFG_BASE_OFFSET, size_array, num_words);

    return ret;
}

int qdma_global_irq_enable(struct qdma_access_ctx* access_ctx, bool state) {

    if (!state) {
        uint32_t reg_val;
        int      ret;

        reg_val = 0U;

        ret = access_ctx->write_direct_reg_func(
            access_ctx->usr_ctx, QDMA_OFFSET_GLBL_GLBL_INTERRUPT_CFG, &reg_val, QDMA_WORD_COUNT(reg_val));

        if (ret) {
            return ret;
        }

        reg_val = 0U;

        ret = access_ctx->write_direct_reg_func(
            access_ctx->usr_ctx, QDMA_CFG_BLOCK_MSI_ENABLE, &reg_val, QDMA_WORD_COUNT(reg_val));


        return ret;
    }

    return -ENOTSUP;
}


int qdma_init_error_reporting(struct qdma_access_ctx* access_ctx) {

    int      ret;
    uint32_t group_index;
    uint32_t reg_element_offset;

    uint32_t value = QDMA_GLBL_ERR_STAT_DSC_MASK | QDMA_GLBL_ERR_STAT_TRQ_MASK | QDMA_GLBL_ERR_STAT_ST_C2H_MASK |
                     QDMA_GLBL_ERR_STAT_IND_CTX_MASK | QDMA_GLBL_ERR_STAT_BRIDGE_MASK | QDMA_GLBL_ERR_STAT_ST_H2C_MASK;

    ret = access_ctx->write_direct_reg_func(access_ctx->usr_ctx, QDMA_OFFSET_GLBL_ERR_MASK, &value, 1);

    if (ret) {
        return ret;
    }

    for (group_index = QDMA_ERR_GRP_DSC; group_index <= QDMA_ERR_GRP_DBE; ++group_index) {
        uint32_t grp_descriptor_base = qdma_error_group_descriptor_offsets[group_index];

        uint32_t flag_register_addr = error_descriptors[grp_descriptor_base].flag_register;
        uint32_t mask_register_addr = error_descriptors[grp_descriptor_base].mask_register;

        value = 0;

        for (reg_element_offset = 0; reg_element_offset < qdma_error_group_descriptor_length[group_index];
             ++reg_element_offset) {

            const struct qdma_error_descriptor* err_descr =
                &error_descriptors[grp_descriptor_base + reg_element_offset];

            // PMD_DRV_LOG(NOTICE, "Checking error descriptor %u - %s", reg_element_offset,
            // err_descr->label);

            value |= err_descr->bit_mask;
        }

        access_ctx->write_direct_reg_func(access_ctx->usr_ctx, mask_register_addr, &value, 1);
        access_ctx->write_direct_reg_func(access_ctx->usr_ctx, flag_register_addr, &value, 1);
    }

    // clear global errors

    ret = access_ctx->read_direct_reg_func(access_ctx->usr_ctx, QDMA_OFFSET_GLBL_ERR_STAT, &value, 1);

    if (ret) {
        return ret;
    }

    ret = access_ctx->write_direct_reg_func(access_ctx->usr_ctx, QDMA_OFFSET_GLBL_ERR_STAT, &value, 1);

    if (ret) {
        return ret;
    }

    return 0;
}

int qdma_read_global_error_status(struct qdma_access_ctx*   access_ctx,
                                  union qdma_glbl_err_stat* glbl_err_stat,
                                  bool                      clear) {

    uint32_t reg_value;

    if (access_ctx->read_direct_reg_func(access_ctx->usr_ctx, QDMA_OFFSET_GLBL_ERR_STAT, &reg_value, 1))
        return -1;

    if (clear) {

        if (access_ctx->write_direct_reg_func(access_ctx->usr_ctx, QDMA_OFFSET_GLBL_ERR_STAT, &reg_value, 1))
            return -1;
    }

    memcpy(glbl_err_stat, &reg_value, sizeof(uint32_t));

    return 0;
}


int qdma_read_errors(struct qdma_access_ctx* access_ctx, struct qdma_error_vector* error_vector) {

    int      ret;
    uint32_t global_error_stat;
    uint32_t group_index;
    uint32_t reg_element_offset;
    uint32_t global_clear_mask;
    uint32_t grp_err_status;
    bool     vec_full = false;
    bool     check_groups[TOTAL_LEAF_ERROR_AGGREGATORS + 1];
    uint32_t args[QDMA_MAX_ERROR_ARGS];

    memset(check_groups, 0, sizeof(bool) * (TOTAL_LEAF_ERROR_AGGREGATORS + 1));

    access_ctx->lock_access_func(access_ctx->usr_ctx);

    ret = access_ctx->read_direct_reg_func(access_ctx->usr_ctx, QDMA_OFFSET_GLBL_ERR_STAT, &global_error_stat, 1);

    if (ret) {
        goto on_error;
    }

    check_groups[QDMA_ERR_GRP_ST_FATAL] = true;
    check_groups[QDMA_ERR_GRP_DSC]      = true;
    check_groups[QDMA_ERR_GRP_ST_C2H]   = true;
    check_groups[QDMA_ERR_GRP_ST_H2C]   = true;
    check_groups[QDMA_ERR_GRP_TRQ]      = true;

    //    if (global_error_stat & QDMA_GLBL_ERR_STAT_DSC_MASK) {
    //        check_groups[QDMA_ERR_GRP_DSC]    = true;
    //        check_groups[QDMA_ERR_GRP_ST_H2C] = true;
    //        check_groups[QDMA_ERR_GRP_ST_C2H] = true;
    //    } else if (global_error_stat & QDMA_GLBL_ERR_STAT_ST_C2H_MASK) {
    //        check_groups[QDMA_ERR_GRP_ST_C2H]   = true;
    //        check_groups[QDMA_ERR_GRP_ST_FATAL] = true;
    //        check_groups[QDMA_ERR_GRP_DSC]      = true;
    //    } else if (global_error_stat & QDMA_GLBL_ERR_STAT_ST_H2C_MASK) {
    //        check_groups[QDMA_ERR_GRP_ST_H2C]   = true;
    //        check_groups[QDMA_ERR_GRP_ST_FATAL] = true;
    //        check_groups[QDMA_ERR_GRP_DSC]      = true;
    //    } else if (global_error_stat & QDMA_GLBL_ERR_STAT_TRQ_MASK) {
    //        check_groups[QDMA_ERR_GRP_TRQ] = true;
    //        check_groups[QDMA_ERR_GRP_DSC]      = true;
    //    }

    for (group_index = QDMA_ERR_GRP_DSC; group_index <= QDMA_ERR_GRP_DBE; ++group_index) {
        if (check_groups[group_index]) {
            // PMD_DRV_LOG(NOTICE, "Error flag of error group %s is set",
            // qdma_get_err_group_name(group_index));

            uint32_t grp_descriptor_base = qdma_error_group_descriptor_offsets[group_index];
            uint32_t clear_mask          = 0;
            uint32_t register_addr       = error_descriptors[grp_descriptor_base].flag_register;
            uint32_t num_valid_args      = 0;

            struct qdma_dsc_err_data dsc_err_data;
            bool                     dsc_err_data_valid = false;

            // PMD_DRV_LOG(NOTICE, "Reading specific error register %x of group %s", register_addr,
            // qdma_get_err_group_name(group_index));

            ret = access_ctx->read_direct_reg_func(access_ctx->usr_ctx, register_addr, &grp_err_status, 1);

            if (ret < 0) {
                goto on_error;
            }

            if (group_index == QDMA_ERR_GRP_TRQ) {
                union qdma_glbl_trq_err_log trq_err_log;

                ret = access_ctx->read_direct_reg_func(
                    access_ctx->usr_ctx, QDMA_OFFSET_GLBL_TRQ_LOG_A, (uint32_t*) &trq_err_log, 1);

                if (ret < 0) {
                    goto on_error;
                }

                args[0] = trq_err_log.address;
                args[1] = trq_err_log.func;
                args[2] = trq_err_log.target;
                args[3] = trq_err_log.src;

                num_valid_args = 4;
            }

            if (group_index == QDMA_ERR_GRP_DSC) {
                union qdma_glbl_dsc_err_log0 dsc_err_log0;
                union qdma_glbl_dsc_err_log1 dsc_err_log1;

                ret = access_ctx->read_direct_reg_func(
                    access_ctx->usr_ctx, QDMA_OFFSET_GLBL_DSC_ERR_LOG0, (uint32_t*) &dsc_err_log0, 1);

                if (ret < 0) {
                    goto on_error;
                }

                ret = access_ctx->read_direct_reg_func(
                    access_ctx->usr_ctx, QDMA_OFFSET_GLBL_DSC_ERR_LOG1, (uint32_t*) &dsc_err_log1, 1);

                if (ret < 0) {
                    goto on_error;
                }

                if (dsc_err_log0.valid) {
                    PMD_DRV_LOG(NOTICE, "DSC error log is valid...");
                    dsc_err_data.qid      = dsc_err_log0.qid;
                    dsc_err_data.type     = dsc_err_log1.err_type;
                    dsc_err_data.sub_type = dsc_err_log1.sub_type;
                    dsc_err_data.cidx     = dsc_err_log1.cidx;
                    dsc_err_data_valid    = true;
                }
            }

            for (reg_element_offset = 0; reg_element_offset < qdma_error_group_descriptor_length[group_index];
                 ++reg_element_offset) {

                const struct qdma_error_descriptor* err_descr =
                    &error_descriptors[grp_descriptor_base + reg_element_offset];

                // PMD_DRV_LOG(NOTICE, "Checking error descriptor %u - %s", reg_element_offset,
                // err_descr->label);

                if (grp_err_status & err_descr->bit_mask) {
                    clear_mask |= err_descr->bit_mask;

                    //                    PMD_DRV_LOG(NOTICE, "Adding error descriptor %u - %s to error
                    //                    vector",
                    //                                reg_element_offset, err_descr->label);

                    // TODO: Implement error group specific error handling and arg extraction
                    if (!qdma_add_to_err_vec(error_vector,
                                             err_descr,
                                             args,
                                             num_valid_args,
                                             (dsc_err_data_valid ? &dsc_err_data : NULL))) {

                        vec_full = true;

                        break;
                    }
                }
            }

            if (clear_mask) {
                ret = access_ctx->write_direct_reg_func(access_ctx->usr_ctx, register_addr, &clear_mask, 1);

                if (ret < 0) {
                    goto on_error;
                }
            }

            if (vec_full) {
                break;
            } else {
                check_groups[group_index] = false;
            }
        }
    }


    // Clear global error bits only if all errors could be fetched
    if (!vec_full) {
        global_clear_mask = global_error_stat;

        if (global_clear_mask) {
            ret = access_ctx->write_direct_reg_func(
                access_ctx->usr_ctx, QDMA_OFFSET_GLBL_ERR_STAT, &global_clear_mask, 1);

            if (ret < 0) {
                goto on_error;
            }
        }
    }

on_error:
    access_ctx->unlock_access_func(access_ctx->usr_ctx);

    return ret;
}

int qdma_query_sw_descr_ctx(struct qdma_access_ctx*   access_ctx,
                            enum data_dir             dir,
                            uint16_t                  queue_id,
                            struct qdma_sw_descr_ctx* sw_descr_ctx) {
    int                          ret;
    uint32_t                     reg = (dir == QDMA_C2H) ? QDMA_CTXT_SEL_SW_C2H : QDMA_CTXT_SEL_SW_H2C;
    uint32_t                     tmp_data[QDMA_REG_IND_CTXT_REG_COUNT];
    struct reg_serialization_ctx reg_ser_ctx;

    ret = qdma_read_indirect(access_ctx, reg, queue_id, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    if (ret < 0) {
        return ret;
    }

    reg_ser_init(&reg_ser_ctx, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->pidx, QDMA_SW_CTX_FIELD_NBITS_PIDX);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->irq_arm, QDMA_SW_CTX_FIELD_NBITS_IRQ_ARM);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->func_id, QDMA_SW_CTX_FIELD_NBITS_FNC_ID);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_SW_CTX_FIELD_NBITS_RSVD1);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->qen, QDMA_SW_CTX_FIELD_NBITS_QEN);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->fcrd_en, QDMA_SW_CTX_FIELD_NBITS_FCRD_EN);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->wbi_chk, QDMA_SW_CTX_FIELD_NBITS_WBL_CHK);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->wbi_intvl_en, QDMA_SW_CTX_FIELD_NBITS_WBL_INTVL_EN);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->at, QDMA_SW_CTX_FIELD_NBITS_AT);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->fetch_max, QDMA_SW_CTX_FIELD_NBITS_FETCH_MAX);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_SW_CTX_FIELD_NBITS_RSVD2);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->rng_sz, QDMA_SW_CTX_FIELD_NBITS_RNG_SZ);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->dsc_sz, QDMA_SW_CTX_FIELD_NBITS_DSC_SZ);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->bypass, QDMA_SW_CTX_FIELD_NBITS_BYPASS);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->is_mm, QDMA_SW_CTX_FIELD_NBITS_MM_CHN);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->wbk_en, QDMA_SW_CTX_FIELD_NBITS_WBK_EN);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->irq_en, QDMA_SW_CTX_FIELD_NBITS_IRQ_EN);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->port_id, QDMA_SW_CTX_FIELD_NBITS_PORT_ID);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_SW_CTX_FIELD_NBITS_IRQ_NO_LAST);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->err, QDMA_SW_CTX_FIELD_NBITS_ERR);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_SW_CTX_FIELD_NBITS_ERR_WB_SENT);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_SW_CTX_FIELD_NBITS_IRQ_REQ);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_SW_CTX_FIELD_NBITS_MRKR_DIS);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_SW_CTX_FIELD_NBITS_IS_MM);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->dsc_base, QDMA_SW_CTX_FIELD_NBITS_DSC_BASE);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->vec, QDMA_SW_CTX_FIELD_NBITS_VEC);
    REG_BIT_DESERIALIZE(reg_ser_ctx, sw_descr_ctx->int_aggr, QDMA_SW_CTX_FIELD_NBITS_INT_AGGR);

    return 0;
}

int qdma_query_hw_descr_ctx(struct qdma_access_ctx*   access_ctx,
                            enum data_dir             dir,
                            uint16_t                  queue_id,
                            struct qdma_hw_descr_ctx* hw_descr_ctx) {
    int                          ret;
    uint32_t                     reg = (dir == QDMA_C2H) ? QDMA_CTXT_SEL_HW_C2H : QDMA_CTXT_SEL_HW_H2C;
    uint32_t                     tmp_data[QDMA_REG_IND_CTXT_REG_COUNT];
    struct reg_serialization_ctx reg_ser_ctx;

    ret = qdma_read_indirect(access_ctx, reg, queue_id, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    if (ret < 0) {
        return ret;
    }

    reg_ser_init(&reg_ser_ctx, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    REG_BIT_DESERIALIZE(reg_ser_ctx, hw_descr_ctx->cidx, QDMA_HW_CTX_FIELD_NBITS_CIDX);
    REG_BIT_DESERIALIZE(reg_ser_ctx, hw_descr_ctx->crd_use, QDMA_HW_CTX_FIELD_NBITS_CRD_USE);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_HW_CTX_FIELD_NBITS_RSVD1);
    REG_BIT_DESERIALIZE(reg_ser_ctx, hw_descr_ctx->dsc_pnd, QDMA_HW_CTX_FIELD_NBITS_DSC_PND);
    REG_BIT_DESERIALIZE(reg_ser_ctx, hw_descr_ctx->idl_stb_b, QDMA_HW_CTX_FIELD_NBITS_IDL_STP_B);
    REG_BIT_DESERIALIZE(reg_ser_ctx, hw_descr_ctx->evt_pnd, QDMA_HW_CTX_FIELD_NBITS_EVT_PND);
    REG_BIT_DESERIALIZE(reg_ser_ctx, hw_descr_ctx->fetch_pnd, QDMA_HW_CTX_FIELD_NBITS_FETCH_PND);

    return 0;
}

int qdma_query_credit_descr_ctx(struct qdma_access_ctx*       access_ctx,
                                enum data_dir                 dir,
                                uint16_t                      queue_id,
                                struct qdma_credit_descr_ctx* credit_descr_ctx) {
    int                          ret;
    uint32_t                     reg = (dir == QDMA_C2H) ? QDMA_CTXT_SEL_CR_C2H : QDMA_CTXT_SEL_CR_H2C;
    uint32_t                     tmp_data[QDMA_REG_IND_CTXT_REG_COUNT];
    struct reg_serialization_ctx reg_ser_ctx;

    ret = qdma_read_indirect(access_ctx, reg, queue_id, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    if (ret < 0) {
        return ret;
    }

    reg_ser_init(&reg_ser_ctx, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    REG_BIT_DESERIALIZE(reg_ser_ctx, credit_descr_ctx->credt, QDMA_CRD_CTX_FIELD_NBITS_CREDT);

    return 0;
}

int qdma_query_prefetch_ctx(struct qdma_access_ctx*   access_ctx,
                            uint16_t                  queue_id,
                            struct qdma_prefetch_ctx* prefetch_ctx) {
    int                          ret;
    uint32_t                     reg = QDMA_CTXT_SEL_PFTCH;
    uint32_t                     tmp_data[QDMA_REG_IND_CTXT_REG_COUNT];
    struct reg_serialization_ctx reg_ser_ctx;

    ret = qdma_read_indirect(access_ctx, reg, queue_id, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    if (ret < 0) {
        return ret;
    }

    reg_ser_init(&reg_ser_ctx, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    REG_BIT_DESERIALIZE(reg_ser_ctx, prefetch_ctx->bypass, QDMA_PREFETCH_CTX_FIELD_NBITS_BYPASS);
    REG_BIT_DESERIALIZE(reg_ser_ctx, prefetch_ctx->buf_size_idx, QDMA_PREFETCH_CTX_FIELD_NBITS_BUF_SIZE_IDX);
    REG_BIT_DESERIALIZE(reg_ser_ctx, prefetch_ctx->port_id, QDMA_PREFETCH_CTX_FIELD_NBITS_PORT_ID);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_PREFETCH_CTX_FIELD_NBITS_RSVD1);
    REG_BIT_DESERIALIZE(reg_ser_ctx, prefetch_ctx->err, QDMA_PREFETCH_CTX_FIELD_NBITS_ERR);
    REG_BIT_DESERIALIZE(reg_ser_ctx, prefetch_ctx->pfch_en, QDMA_PREFETCH_CTX_FIELD_NBITS_PFCH_EN);
    REG_BIT_DESERIALIZE(reg_ser_ctx, prefetch_ctx->pfch, QDMA_PREFETCH_CTX_FIELD_NBITS_PFCH);
    REG_BIT_DESERIALIZE(reg_ser_ctx, prefetch_ctx->sw_credit, QDMA_PREFETCH_CTX_FIELD_NBITS_SW_CRDT);
    REG_BIT_DESERIALIZE(reg_ser_ctx, prefetch_ctx->valid, QDMA_PREFETCH_CTX_FIELD_NBITS_VALID);

    return 0;
}

int qdma_query_completion_ctx(struct qdma_access_ctx*     access_ctx,
                              uint16_t                    queue_id,
                              struct qdma_completion_ctx* completion_ctx) {

    int                          ret;
    uint32_t                     reg = QDMA_CTXT_SEL_CMPT;
    uint32_t                     tmp_data[QDMA_REG_IND_CTXT_REG_COUNT];
    struct reg_serialization_ctx reg_ser_ctx;

    ret = qdma_read_indirect(access_ctx, reg, queue_id, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    if (ret < 0) {
        return ret;
    }

    reg_ser_init(&reg_ser_ctx, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->en_stat_desc, QDMA_CMPL_CTX_FIELD_NBITS_EN_STAT_DESC);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->en_int, QDMA_CMPL_CTX_FIELD_NBITS_EN_INT);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->trig_mode, QDMA_CMPL_CTX_FIELD_NBITS_TRIG_MODE);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->fnc_id, QDMA_CMPL_CTX_FIELD_NBITS_FNC_ID);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_CMPL_CTX_FIELD_NBITS_RSVD1);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->counter_idx, QDMA_CMPL_CTX_FIELD_NBITS_COUNTER_IDX);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->timer_idx, QDMA_CMPL_CTX_FIELD_NBITS_TIMER_IDX);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->int_st, QDMA_CMPL_CTX_FIELD_NBITS_INT_ST);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->color, QDMA_CMPL_CTX_FIELD_NBITS_COLOR);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->qsize_idx, QDMA_CMPL_CTX_FIELD_NBITS_QSIZE_IDX);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->baddr, QDMA_CMPL_CTX_FIELD_NBITS_BADDR);
    completion_ctx->baddr <<= 6;
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->desc_size, QDMA_CMPL_CTX_FIELD_NBITS_DESC_SIZE);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->pidx, QDMA_CMPL_CTX_FIELD_NBITS_PIDX);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->valid, QDMA_CMPL_CTX_FIELD_NBITS_VALID);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->err, QDMA_CMPL_CTX_FIELD_NBITS_ERR);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->user_trig_pend, QDMA_CMPL_CTX_FIELD_NBITS_USER_TRIG_PEND);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->timer_running, QDMA_CMPL_CTX_FIELD_NBITS_TIMER_RUNNING);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->full_upd, QDMA_CMPL_CTX_FIELD_NBITS_FULL_UPD);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->ovf_chk_dis, QDMA_CMPL_CTX_FIELD_NBITS_OVF_CHK_DIS);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->at, QDMA_CMPL_CTX_FIELD_NBITS_AT);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->vec, QDMA_CMPL_CTX_FIELD_NBITS_VEC);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->int_aggr, QDMA_CMPL_CTX_FIELD_NBITS_INT_AGGR);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->dis_int_on_vf, QDMA_CMPL_CTX_FIELD_NBITS_DIS_INT_ON_VF);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_CMPL_CTX_FIELD_NBITS_RSVD2);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->dir_c2h, QDMA_CMPL_CTX_FIELD_NBITS_DIR_C2H);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_CMPL_CTX_FIELD_NBITS_RSVD3);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_CMPL_CTX_FIELD_NBITS_BADDR4_LOW);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_CMPL_CTX_FIELD_NBITS_RSVD4);
    REG_BIT_DESERIALIZE(reg_ser_ctx, completion_ctx->port_id, QDMA_CMPL_CTX_FIELD_NBITS_PORT_ID);
    REG_BIT_SKIP(reg_ser_ctx, QDMA_CMPL_CTX_FIELD_NBITS_RSVD5);

    return 0;
}

int qdma_query_function_map_ctx(struct qdma_access_ctx*       access_ctx,
                                uint16_t                      queue_id,
                                struct qdma_function_map_ctx* function_map_ctx) {
    return 0;
}

int qdma_clear_sw_descr_ctx(struct qdma_access_ctx* access_ctx, enum data_dir dir, uint16_t queue_id) {
    return qdma_clear_indirect(access_ctx, (dir == QDMA_C2H) ? QDMA_CTXT_SEL_SW_C2H : QDMA_CTXT_SEL_SW_H2C, queue_id);
}

int qdma_clear_hw_descr_ctx(struct qdma_access_ctx* access_ctx, enum data_dir dir, uint16_t queue_id) {
    return qdma_clear_indirect(access_ctx, (dir == QDMA_C2H) ? QDMA_CTXT_SEL_HW_C2H : QDMA_CTXT_SEL_HW_H2C, queue_id);
}

int qdma_clear_credit_descr_ctx(struct qdma_access_ctx* access_ctx, enum data_dir dir, uint16_t queue_id) {
    return qdma_clear_indirect(access_ctx, (dir == QDMA_C2H) ? QDMA_CTXT_SEL_CR_C2H : QDMA_CTXT_SEL_CR_H2C, queue_id);
}

int qdma_clear_prefetch_descr_ctx(struct qdma_access_ctx* access_ctx, uint16_t queue_id) {
    return qdma_clear_indirect(access_ctx, QDMA_CTXT_SEL_PFTCH, queue_id);
}

int qdma_clear_completion_descr_ctx(struct qdma_access_ctx* access_ctx, uint16_t queue_id) {
    return qdma_clear_indirect(access_ctx, QDMA_CTXT_SEL_CMPT, queue_id);
}

int qdma_set_sw_descr_ctx(struct qdma_access_ctx*         access_ctx,
                          enum data_dir                   dir,
                          uint16_t                        queue_id,
                          const struct qdma_sw_descr_ctx* sw_descr_ctx) {
    int                          ret;
    uint32_t                     reg = (dir == QDMA_C2H) ? QDMA_CTXT_SEL_SW_C2H : QDMA_CTXT_SEL_SW_H2C;
    uint32_t                     tmp_data[QDMA_REG_IND_CTXT_REG_COUNT];
    struct reg_serialization_ctx reg_ser_ctx;

    memset(tmp_data, 0, sizeof(tmp_data));
    reg_ser_init(&reg_ser_ctx, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->pidx, QDMA_SW_CTX_FIELD_NBITS_PIDX);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->irq_arm, QDMA_SW_CTX_FIELD_NBITS_IRQ_ARM);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->func_id, QDMA_SW_CTX_FIELD_NBITS_FNC_ID);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_SW_CTX_FIELD_NBITS_RSVD1);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->qen, QDMA_SW_CTX_FIELD_NBITS_QEN);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->fcrd_en, QDMA_SW_CTX_FIELD_NBITS_FCRD_EN);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->wbi_chk, QDMA_SW_CTX_FIELD_NBITS_WBL_CHK);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->wbi_intvl_en, QDMA_SW_CTX_FIELD_NBITS_WBL_INTVL_EN);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->at, QDMA_SW_CTX_FIELD_NBITS_AT);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->fetch_max, QDMA_SW_CTX_FIELD_NBITS_FETCH_MAX);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_SW_CTX_FIELD_NBITS_RSVD2);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->rng_sz, QDMA_SW_CTX_FIELD_NBITS_RNG_SZ);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->dsc_sz, QDMA_SW_CTX_FIELD_NBITS_DSC_SZ);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->bypass, QDMA_SW_CTX_FIELD_NBITS_BYPASS);
    REG_BIT_SERIALIZE(reg_ser_ctx, 0, QDMA_SW_CTX_FIELD_NBITS_MM_CHN);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->wbk_en, QDMA_SW_CTX_FIELD_NBITS_WBK_EN);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->irq_en, QDMA_SW_CTX_FIELD_NBITS_IRQ_EN);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->port_id, QDMA_SW_CTX_FIELD_NBITS_PORT_ID);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_SW_CTX_FIELD_NBITS_IRQ_NO_LAST);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->err, QDMA_SW_CTX_FIELD_NBITS_ERR);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->err_wb_sent, QDMA_SW_CTX_FIELD_NBITS_ERR_WB_SENT);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->irq_req, QDMA_SW_CTX_FIELD_NBITS_IRQ_REQ);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->mrkr_dis, QDMA_SW_CTX_FIELD_NBITS_MRKR_DIS);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->is_mm, QDMA_SW_CTX_FIELD_NBITS_IS_MM);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->dsc_base, QDMA_SW_CTX_FIELD_NBITS_DSC_BASE);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->vec, QDMA_SW_CTX_FIELD_NBITS_VEC);
    REG_BIT_SERIALIZE(reg_ser_ctx, sw_descr_ctx->int_aggr, QDMA_SW_CTX_FIELD_NBITS_INT_AGGR);

    PMD_DRV_LOG(NOTICE,
                "Q%u - %s: wrote sw context: nbits %u",
                queue_id,
                ((dir == QDMA_C2H) ? "c2h" : "h2c"),
                (reg_ser_ctx.word_idx * QDMA_WORD_SIZE_BITS + reg_ser_ctx.bit_idx));

    ret = qdma_write_indirect(access_ctx, reg, queue_id, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    if (ret < 0) {
        return ret;
    }

    return 0;
}

int qdma_set_hw_descr_ctx(struct qdma_access_ctx*         access_ctx,
                          enum data_dir                   dir,
                          uint16_t                        queue_id,
                          const struct qdma_hw_descr_ctx* hw_descr_ctx) {
    int                          ret;
    uint32_t                     reg = (dir == QDMA_C2H) ? QDMA_CTXT_SEL_HW_C2H : QDMA_CTXT_SEL_HW_H2C;
    uint32_t                     tmp_data[QDMA_REG_IND_CTXT_REG_COUNT];
    struct reg_serialization_ctx reg_ser_ctx;

    memset(tmp_data, 0, sizeof(tmp_data));
    reg_ser_init(&reg_ser_ctx, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    REG_BIT_SERIALIZE(reg_ser_ctx, hw_descr_ctx->cidx, QDMA_HW_CTX_FIELD_NBITS_CIDX);
    REG_BIT_SERIALIZE(reg_ser_ctx, hw_descr_ctx->crd_use, QDMA_HW_CTX_FIELD_NBITS_CRD_USE);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_HW_CTX_FIELD_NBITS_RSVD1);
    REG_BIT_SERIALIZE(reg_ser_ctx, hw_descr_ctx->dsc_pnd, QDMA_HW_CTX_FIELD_NBITS_DSC_PND);
    REG_BIT_SERIALIZE(reg_ser_ctx, hw_descr_ctx->idl_stb_b, QDMA_HW_CTX_FIELD_NBITS_IDL_STP_B);
    REG_BIT_SERIALIZE(reg_ser_ctx, hw_descr_ctx->evt_pnd, QDMA_HW_CTX_FIELD_NBITS_EVT_PND);
    REG_BIT_SERIALIZE(reg_ser_ctx, hw_descr_ctx->fetch_pnd, QDMA_HW_CTX_FIELD_NBITS_FETCH_PND);

    ret = qdma_write_indirect(access_ctx, reg, queue_id, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    if (ret < 0) {
        return ret;
    }

    return 0;
}

int qdma_set_credit_descr_ctx(struct qdma_access_ctx*             access_ctx,
                              enum data_dir                       dir,
                              uint16_t                            queue_id,
                              const struct qdma_credit_descr_ctx* credit_descr_ctx) {
    int                          ret;
    uint32_t                     reg = (dir == QDMA_C2H) ? QDMA_CTXT_SEL_CR_C2H : QDMA_CTXT_SEL_CR_H2C;
    uint32_t                     tmp_data[QDMA_REG_IND_CTXT_REG_COUNT];
    struct reg_serialization_ctx reg_ser_ctx;

    memset(tmp_data, 0, sizeof(tmp_data));
    reg_ser_init(&reg_ser_ctx, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    REG_BIT_SERIALIZE(reg_ser_ctx, credit_descr_ctx->credt, QDMA_CRD_CTX_FIELD_NBITS_CREDT);

    ret = qdma_write_indirect(access_ctx, reg, queue_id, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    if (ret < 0) {
        return ret;
    }

    return 0;
}

int qdma_set_prefetch_ctx(struct qdma_access_ctx*         access_ctx,
                          uint16_t                        queue_id,
                          const struct qdma_prefetch_ctx* prefetch_ctx) {
    int                          ret;
    uint32_t                     reg = QDMA_CTXT_SEL_PFTCH;
    uint32_t                     tmp_data[QDMA_REG_IND_CTXT_REG_COUNT];
    struct reg_serialization_ctx reg_ser_ctx;

    memset(tmp_data, 0, sizeof(tmp_data));
    reg_ser_init(&reg_ser_ctx, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    REG_BIT_SERIALIZE(reg_ser_ctx, prefetch_ctx->bypass, QDMA_PREFETCH_CTX_FIELD_NBITS_BYPASS);
    REG_BIT_SERIALIZE(reg_ser_ctx, prefetch_ctx->buf_size_idx, QDMA_PREFETCH_CTX_FIELD_NBITS_BUF_SIZE_IDX);
    REG_BIT_SERIALIZE(reg_ser_ctx, prefetch_ctx->port_id, QDMA_PREFETCH_CTX_FIELD_NBITS_PORT_ID);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_PREFETCH_CTX_FIELD_NBITS_RSVD1);
    REG_BIT_SERIALIZE(reg_ser_ctx, prefetch_ctx->err, QDMA_PREFETCH_CTX_FIELD_NBITS_ERR);
    REG_BIT_SERIALIZE(reg_ser_ctx, prefetch_ctx->pfch_en, QDMA_PREFETCH_CTX_FIELD_NBITS_PFCH_EN);
    REG_BIT_SERIALIZE(reg_ser_ctx, prefetch_ctx->pfch, QDMA_PREFETCH_CTX_FIELD_NBITS_PFCH);
    REG_BIT_SERIALIZE(reg_ser_ctx, prefetch_ctx->sw_credit, QDMA_PREFETCH_CTX_FIELD_NBITS_SW_CRDT);
    REG_BIT_SERIALIZE(reg_ser_ctx, prefetch_ctx->valid, QDMA_PREFETCH_CTX_FIELD_NBITS_VALID);

    ret = qdma_write_indirect(access_ctx, reg, queue_id, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    if (ret < 0) {
        return ret;
    }

    return 0;
}

int qdma_set_completion_ctx(struct qdma_access_ctx*           access_ctx,
                            uint16_t                          queue_id,
                            const struct qdma_completion_ctx* completion_ctx) {
    int                          ret;
    uint32_t                     reg = QDMA_CTXT_SEL_CMPT;
    uint32_t                     tmp_data[QDMA_REG_IND_CTXT_REG_COUNT];
    struct reg_serialization_ctx reg_ser_ctx;

    memset(tmp_data, 0, sizeof(tmp_data));
    reg_ser_init(&reg_ser_ctx, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->en_stat_desc, QDMA_CMPL_CTX_FIELD_NBITS_EN_STAT_DESC);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->en_int, QDMA_CMPL_CTX_FIELD_NBITS_EN_INT);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->trig_mode, QDMA_CMPL_CTX_FIELD_NBITS_TRIG_MODE);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->fnc_id, QDMA_CMPL_CTX_FIELD_NBITS_FNC_ID);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CMPL_CTX_FIELD_NBITS_RSVD1);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->counter_idx, QDMA_CMPL_CTX_FIELD_NBITS_COUNTER_IDX);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->timer_idx, QDMA_CMPL_CTX_FIELD_NBITS_TIMER_IDX);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->int_st, QDMA_CMPL_CTX_FIELD_NBITS_INT_ST);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->color, QDMA_CMPL_CTX_FIELD_NBITS_COLOR);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->qsize_idx, QDMA_CMPL_CTX_FIELD_NBITS_QSIZE_IDX);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->baddr >> 6, QDMA_CMPL_CTX_FIELD_NBITS_BADDR);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->desc_size, QDMA_CMPL_CTX_FIELD_NBITS_DESC_SIZE);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->pidx, QDMA_CMPL_CTX_FIELD_NBITS_PIDX);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->cidx, QDMA_CMPL_CTX_FIELD_NBITS_CIDX);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->valid, QDMA_CMPL_CTX_FIELD_NBITS_VALID);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->err, QDMA_CMPL_CTX_FIELD_NBITS_ERR);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->user_trig_pend, QDMA_CMPL_CTX_FIELD_NBITS_USER_TRIG_PEND);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->timer_running, QDMA_CMPL_CTX_FIELD_NBITS_TIMER_RUNNING);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->full_upd, QDMA_CMPL_CTX_FIELD_NBITS_FULL_UPD);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->ovf_chk_dis, QDMA_CMPL_CTX_FIELD_NBITS_OVF_CHK_DIS);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->at, QDMA_CMPL_CTX_FIELD_NBITS_AT);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->vec, QDMA_CMPL_CTX_FIELD_NBITS_VEC);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->int_aggr, QDMA_CMPL_CTX_FIELD_NBITS_INT_AGGR);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->dis_int_on_vf, QDMA_CMPL_CTX_FIELD_NBITS_DIS_INT_ON_VF);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CMPL_CTX_FIELD_NBITS_RSVD2);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->dir_c2h, QDMA_CMPL_CTX_FIELD_NBITS_DIR_C2H);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CMPL_CTX_FIELD_NBITS_RSVD3);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CMPL_CTX_FIELD_NBITS_BADDR4_LOW);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CMPL_CTX_FIELD_NBITS_RSVD4);
    REG_BIT_SERIALIZE(reg_ser_ctx, completion_ctx->port_id, QDMA_CMPL_CTX_FIELD_NBITS_PORT_ID);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CMPL_CTX_FIELD_NBITS_RSVD5);

    ret = qdma_write_indirect(access_ctx, reg, queue_id, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    if (ret < 0) {
        return ret;
    }

    return 0;
}

int qdma_set_function_map_ctx(struct qdma_access_ctx*             access_ctx,
                              uint16_t                            function_id,
                              const struct qdma_function_map_ctx* function_map_ctx) {
    int                          ret;
    uint32_t                     reg = QDMA_CTXT_SEL_FMAP;
    uint32_t                     tmp_data[QDMA_REG_IND_CTXT_REG_COUNT];
    struct reg_serialization_ctx reg_ser_ctx;

    memset(tmp_data, 0, sizeof(tmp_data));
    reg_ser_init(&reg_ser_ctx, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    REG_BIT_SERIALIZE(reg_ser_ctx, function_map_ctx->base_queue_id, QDMA_FMAP_CTX_FIELD_NBITS_QID_BASE);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_FMAP_CTX_FIELD_NBITS_RSVD1);
    REG_BIT_SERIALIZE(reg_ser_ctx, function_map_ctx->max_queue_count, QDMA_FMAP_CTX_FIELD_NBITS_QID_MAX);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_FMAP_CTX_FIELD_NBITS_RSVD2);

    ret = qdma_write_indirect(access_ctx, reg, function_id, tmp_data, QDMA_REG_IND_CTXT_REG_COUNT);

    if (ret < 0) {
        return ret;
    }

    return 0;
}

int qdma_function_level_reset(struct qdma_access_ctx* access_ctx, uint32_t timeout_us) {
    int ret;

    uint32_t value = QDMA_FLR_CTRL_BITMASK;

    ret = access_ctx->write_direct_reg_func(access_ctx->usr_ctx, QDMA_FLR_CTRL_REGISTER, &value, 1);

    if (ret) {
        return ret;
    }

    ret = access_ctx->poll_direct_reg_func(
        access_ctx->usr_ctx, QDMA_FLR_CTRL_REGISTER, QDMA_FLR_CTRL_BITMASK, 0, timeout_us);

    return ret;
}

int qdma_set_global_descr_config(struct qdma_access_ctx*             access_ctx,
                                 enum qdma_global_writeback_interval wb_intvl,
                                 uint8_t                             max_fetch,
                                 bool                                ctxt_fer_dis) {
    uint32_t                     reg     = QDMA_GLBL_DSC_CFG;
    uint32_t                     reg_val = 0;
    struct reg_serialization_ctx reg_ser_ctx;

    reg_ser_init(&reg_ser_ctx, &reg_val, 1);

    REG_BIT_SERIALIZE(reg_ser_ctx, (uint8_t) wb_intvl, QDMA_GLBL_DSC_CFG_FIELD_NBITS_WB_ACC_INT);
    REG_BIT_SERIALIZE(reg_ser_ctx, max_fetch, QDMA_GLBL_DSC_CFG_FIELD_NBITS_MAX_DSC_FETCH);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_GLBL_DSC_CFG_FIELD_NBITS_RSVD1);
    REG_BIT_SERIALIZE(reg_ser_ctx, (ctxt_fer_dis ? 1 : 0), QDMA_GLBL_DSC_CFG_FIELD_NBITS_CTXT_FER_DIS);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_GLBL_DSC_CFG_FIELD_NBITS_UNC_OVR_COR);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_GLBL_DSC_CFG_FIELD_NBITS_RSVD2);

    return access_ctx->write_direct_reg_func(access_ctx->usr_ctx, reg, &reg_val, 1);
}

int qdma_set_h2c_pcie_throt(struct qdma_access_ctx* access_ctx,
                            uint32_t                data_thresh,
                            bool                    req_throt_en_data,
                            uint16_t                req_throt,
                            bool                    req_throt_en_req) {
    uint32_t                     reg     = QDMA_H2C_REQ_THROT_PCIE;
    uint32_t                     reg_val = 0;
    struct reg_serialization_ctx reg_ser_ctx;

    reg_ser_init(&reg_ser_ctx, &reg_val, 1);

    REG_BIT_SERIALIZE(reg_ser_ctx, data_thresh, QDMA_H2C_REQ_THROT_PCIE_NBITS_DATA_THRESH);
    REG_BIT_SERIALIZE(reg_ser_ctx, req_throt_en_data ? 1 : 0, QDMA_H2C_REQ_THROT_PCIE_NBITS_REQ_THROT_EN_DATA);
    REG_BIT_SERIALIZE(reg_ser_ctx, req_throt, QDMA_H2C_REQ_THROT_PCIE_NBITS_REQ_THROT);
    REG_BIT_SERIALIZE(reg_ser_ctx, req_throt_en_req ? 1 : 0, QDMA_H2C_REQ_THROT_PCIE_NBITS_REQ_EN_REQ);


    return access_ctx->write_direct_reg_func(access_ctx->usr_ctx, reg, &reg_val, 1);
}

int qdma_config_prefetch_engine(struct qdma_access_ctx* access_ctx,
                                uint32_t                evt_qcnt_th,
                                uint32_t                pfch_qcnt,
                                uint16_t                num_pfch,
                                uint16_t                pfch_fl_th) {

    uint32_t                     reg;
    uint32_t                     reg_val = 0;
    struct reg_serialization_ctx reg_ser_ctx;
    int                          rc;

    // Writing prefetch config
    reg = QDMA_C2H_PFCH_CFG_BASE_OFFSET;
    reg_ser_init(&reg_ser_ctx, &reg_val, 1);

    REG_BIT_SERIALIZE(reg_ser_ctx, pfch_fl_th, QDMA_C2H_PFCH_CFG_FIELD_NBITS_PFCH_FL_TH);
    REG_BIT_SERIALIZE(reg_ser_ctx, 256, QDMA_C2H_PFCH_CFG_FIELD_NBITS_EVT_PFCH_FL_TH);

    rc = access_ctx->write_direct_reg_func(access_ctx->usr_ctx, reg, &reg_val, 1);
    if (rc < 0) {
        return rc;
    }

    // Writing prefetch config 1
    reg     = QDMA_C2H_PFCH_CFG_1_BASE_OFFSET;
    reg_val = 0;
    reg_ser_init(&reg_ser_ctx, &reg_val, 1);

    REG_BIT_SERIALIZE(reg_ser_ctx, pfch_qcnt, QDMA_C2H_PFCH_CFG_1_FIELD_NBITS_PFCH_QCNT);
    REG_BIT_SERIALIZE(reg_ser_ctx, evt_qcnt_th, QDMA_C2H_PFCH_CFG_1_FIELD_NBITS_EVT_QCNT_TH);

    rc = access_ctx->write_direct_reg_func(access_ctx->usr_ctx, reg, &reg_val, 1);
    if (rc < 0) {
        return rc;
    }

    // Writing prefetch config 2
    reg     = QDMA_C2H_PFCH_CFG_2_BASE_OFFSET;
    reg_val = 0;
    reg_ser_init(&reg_ser_ctx, &reg_val, 1);

    REG_BIT_SERIALIZE(reg_ser_ctx, num_pfch, QDMA_C2H_PFCH_CFG_2_FIELD_NBITS_NUM_PFCH);
    REG_BIT_SERIALIZE(reg_ser_ctx, 31, QDMA_C2H_PFCH_CFG_2_FIELD_NBITS_VAR_DESC_NUM_PFCH);
    REG_BIT_SERIALIZE(reg_ser_ctx, 1024, QDMA_C2H_PFCH_CFG_2_FIELD_NBITS_PFCH_LL_SZ_TH);
    REG_BIT_SERIALIZE(reg_ser_ctx, 0, QDMA_C2H_PFCH_CFG_2_FIELD_NBITS_VAR_DESC_NO_DROP);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_C2H_PFCH_CFG_2_FIELD_NBITS_REMAINING);

    rc = access_ctx->write_direct_reg_func(access_ctx->usr_ctx, reg, &reg_val, 1);
    if (rc < 0) {
        return rc;
    }

    return 0;
}

int qdma_config_global_tickrate(struct qdma_access_ctx* access_ctx, uint32_t tickrate_val) {

    uint32_t                     reg     = QDMA_C2H_INT_TIMER_TICK_BASE_OFFSET;
    uint32_t                     reg_val = 0;
    struct reg_serialization_ctx reg_ser_ctx;

    reg_ser_init(&reg_ser_ctx, &reg_val, 1);

    REG_BIT_SERIALIZE(reg_ser_ctx, tickrate_val, QDMA_C2H_INT_TIMER_TICK_FIELD_NBITS_TICKRATE);

    return access_ctx->write_direct_reg_func(access_ctx->usr_ctx, reg, &reg_val, 1);
}

int qdma_config_c2h_wb_coal(struct qdma_access_ctx* access_ctx, uint16_t tick_val, uint16_t tick_count) {
    int                          rc;
    uint32_t                     reg;
    uint32_t                     reg_val = 0;
    uint32_t                     coal_buf_sz_readval;
    struct reg_serialization_ctx reg_ser_ctx;

    // Read c2h writeback coalesence buffer size (read-only constant set during IP generation)
    rc = access_ctx->read_direct_reg_func(access_ctx->usr_ctx,
                                          QDMA_C2H_WRB_COAL_BUF_DEPTH_BASE_OFFSET,
                                          &coal_buf_sz_readval,
                                          QDMA_WORD_COUNT(coal_buf_sz_readval));

    if (rc < 0) {
        return rc;
    }

    reg = QDMA_C2H_WRB_COAL_CFG_BASE_OFFSET;
    reg_ser_init(&reg_ser_ctx, &reg_val, 1);

    REG_BIT_ZERO(reg_ser_ctx, QDMA_C2H_WRB_COAL_CFG_FIELD_NBITS_DONE_GLB_FLUSH);
    REG_BIT_SERIALIZE(reg_ser_ctx, 0, QDMA_C2H_WRB_COAL_CFG_FIELD_NBITS_SET_GLB_FLUSH);
    REG_BIT_SERIALIZE(reg_ser_ctx, tick_count, QDMA_C2H_WRB_COAL_CFG_FIELD_NBITS_TICK_CNT);
    REG_BIT_SERIALIZE(reg_ser_ctx, tick_val, QDMA_C2H_WRB_COAL_CFG_FIELD_NBITS_TICK_VAL);
    REG_BIT_SERIALIZE(reg_ser_ctx, coal_buf_sz_readval & 0xffU, QDMA_C2H_WRB_COAL_CFG_FIELD_NBITS_MAX_BUF_SZ);

    rc = access_ctx->write_direct_reg_func(access_ctx->usr_ctx, reg, &reg_val, 1);
    if (rc < 0) {
        return rc;
    }


    return rc;
}


int qdma_config_pcie_access_width(struct qdma_access_ctx*     access_ctx,
                                  enum qdma_pcie_access_width max_read_width,
                                  enum qdma_pcie_access_width max_write_width) {
    int      rc;
    uint32_t reg;
    uint32_t reg_val = 0;

    struct reg_serialization_ctx reg_ser_ctx;

    // Write width
    reg = QDMA_CFG_BLOCK_MAX_PLSZ_INFO;
    reg_ser_init(&reg_ser_ctx, &reg_val, 1);

    REG_BIT_ZERO(reg_ser_ctx, QDMA_CFG_ACCESS_WIDTH_FIELD_NBITS_MAX_ISSUED);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CFG_ACCESS_WIDTH_FIELD_NBITS_RESERVED1);
    REG_BIT_SERIALIZE(reg_ser_ctx, max_write_width, QDMA_CFG_ACCESS_WIDTH_FIELD_NBITS_MAX_PROG);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CFG_ACCESS_WIDTH_FIELD_NBITS_RESERVED2);

    rc = access_ctx->write_direct_reg_func(access_ctx->usr_ctx, reg, &reg_val, 1);
    if (rc < 0) {
        return rc;
    }

    // Read width
    reg = QDMA_CFG_BLOCK_MAX_RDRQ_INFO;
    reg_ser_init(&reg_ser_ctx, &reg_val, 1);

    REG_BIT_ZERO(reg_ser_ctx, QDMA_CFG_ACCESS_WIDTH_FIELD_NBITS_MAX_ISSUED);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CFG_ACCESS_WIDTH_FIELD_NBITS_RESERVED1);
    REG_BIT_SERIALIZE(reg_ser_ctx, max_read_width, QDMA_CFG_ACCESS_WIDTH_FIELD_NBITS_MAX_PROG);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CFG_ACCESS_WIDTH_FIELD_NBITS_RESERVED2);

    rc = access_ctx->write_direct_reg_func(access_ctx->usr_ctx, reg, &reg_val, 1);
    if (rc < 0) {
        return rc;
    }

    return rc;
}

int qdma_config_pcie_ctrl(struct qdma_access_ctx* access_ctx, bool enable_relaxed_ordering) {

    uint32_t                     reg     = QDMA_CFG_BLOCK_PCIE_CONTROL;
    uint32_t                     reg_val = 0;
    struct reg_serialization_ctx reg_ser_ctx;

    reg_ser_init(&reg_ser_ctx, &reg_val, 1);

    REG_BIT_SERIALIZE(
        reg_ser_ctx, (enable_relaxed_ordering ? 1U : 0U), QDMA_CFG_BLOCK_PCIE_CONTROL_RELAXED_ORDERING_NBITS);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CFG_BLOCK_PCIE_CONTROL_RRQ_DISABLE_NBITS);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CFG_BLOCK_PCIE_CONTROL_RESERVED0_NBITS);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CFG_BLOCK_PCIE_CONTROL_MGMT_AXIL_CTRL_NBITS);
    REG_BIT_ZERO(reg_ser_ctx, QDMA_CFG_BLOCK_PCIE_CONTROL_RESERVED1_NBITS);

    return access_ctx->write_direct_reg_func(access_ctx->usr_ctx, reg, &reg_val, 1);
}

#if 0

void qdma_update_h2c_pidx(_ioptr_t dmap_base_ioptr, uint16_t queue_id, uint16_t pidx, bool irq_arm) {

    uint32_t reg_offset = QDMA_DMAP_SEL_H2C_DSC_PIDX + (queue_id * QDMA_DMAP_REG_QUEUE_OFFSET);

    uint32_t reg_value = 0;

    volatile uint32_t* reg_dst = ((volatile uint32_t*)(dmap_base_ioptr + reg_offset));

    reg_value |= (pidx & 0xffff);

    *reg_dst = reg_value;

    rte_wmb();

    //PMD_DRV_LOG(NOTICE, "Updated h2c pidx of queue %u to %u @0x%x - reg space base %lx", queue_id, reg_value, reg_offset, (uintptr_t)dmap_base_ioptr);
}

void qdma_update_c2h_pidx(_ioptr_t dmap_base_ioptr, uint16_t queue_id, uint16_t pidx, bool irq_arm) {

    uint32_t reg_offset = QDMA_DMAP_SEL_C2H_DSC_PIDX + (queue_id * QDMA_DMAP_REG_QUEUE_OFFSET);

    uint32_t reg_value = 0;

    volatile uint32_t* reg_dst = ((volatile uint32_t*)(dmap_base_ioptr + reg_offset));

    reg_value |= (pidx & 0xffff);

    *reg_dst = reg_value;

    rte_wmb();

    PMD_DRV_LOG(NOTICE, "Updated c2h descr pidx of queue %u to %u @0x%x - reg space base %lx", queue_id, reg_value, reg_offset, (uintptr_t)dmap_base_ioptr);

}

void qdma_update_c2h_completion_cidx(_ioptr_t dmap_base_ioptr, uint16_t queue_id, uint16_t cidx, bool irq_arm, bool enable_status_descr) {

    uint32_t reg_offset = QDMA_DMAP_SEL_C2H_CMPT_CIDX + (queue_id * QDMA_DMAP_REG_QUEUE_OFFSET);

    uint32_t reg_value = 0;

    volatile uint32_t* reg_dst = ((volatile uint32_t*)(dmap_base_ioptr + reg_offset));

    reg_value |= (cidx & 0xffff);

    *reg_dst = reg_value;

    rte_wmb();

    PMD_DRV_LOG(NOTICE, "Updated c2h compl cidx of queue %u to %u @0x%x - reg space base %lx", queue_id, reg_value, reg_offset, (uintptr_t)dmap_base_ioptr);


//
//    volatile uint32_t* reg = REG_POINTER_OFFSET(
//        uint32_t, dmap_base_ioptr, QDMA_DMAP_SEL_C2H_CMPT_CIDX, (queue_id * QDMA_DMAP_REG_QUEUE_OFFSET));
//
//    uint32_t reg_update_val = ((uint32_t)cidx) | (irq_arm ? QDMA_DMAP_SEL_C2H_CMPT_CIDX_IRQ_BIT : 0x00U) |
//                              (enable_status_descr ? QDMA_DMAP_SEL_C2H_CMPT_CIDX_EN_STS_WB : 0x00U);
//
//    *reg = reg_update_val;
}

#endif

static int qdma_clear_indirect(struct qdma_access_ctx* access_ctx, enum ind_ctxt_cmd_sel cmd_sel, uint16_t queue_id) {
    union qdma_ind_ctxt_cmd cmd;
    int                     ret;

    cmd.word     = 0;
    cmd.bits.qid = queue_id;
    cmd.bits.op  = QDMA_CTXT_CMD_CLR;
    cmd.bits.sel = cmd_sel;

    access_ctx->lock_access_func(access_ctx->usr_ctx);

    ret = access_ctx->write_direct_reg_func(access_ctx->usr_ctx, QDMA_OFFSET_IND_CTXT_CMD, &cmd.word, 1);

    if (ret < 0)
        goto on_error;

    ret = access_ctx->poll_direct_reg_func(access_ctx->usr_ctx,
                                           QDMA_OFFSET_IND_CTXT_CMD,
                                           QDMA_IND_CTXT_CMD_BUSY_MASK,
                                           0,
                                           QDMA_POLL_REG_DEFAULT_TIMEOUT_US);

    if (ret == 0) {
        ret = -ETIMEDOUT;
    }

on_error:

    access_ctx->unlock_access_func(access_ctx->usr_ctx);

    if (ret > 0)
        ret = 0;

    return ret;
}

static int qdma_read_indirect(struct qdma_access_ctx* access_ctx,
                              enum ind_ctxt_cmd_sel   cmd_sel,
                              uint16_t                queue_id,
                              void*                   data_ptr,
                              uint32_t                num_words) {
    union qdma_ind_ctxt_cmd cmd;
    int                     ret;

    cmd.word     = 0;
    cmd.bits.qid = queue_id;
    cmd.bits.op  = QDMA_CTXT_CMD_RD;
    cmd.bits.sel = cmd_sel;

    access_ctx->lock_access_func(access_ctx->usr_ctx);

    ret = access_ctx->write_direct_reg_func(access_ctx->usr_ctx, QDMA_OFFSET_IND_CTXT_CMD, &cmd.word, 1);

    if (ret < 0)
        goto on_error;

    ret = access_ctx->poll_direct_reg_func(access_ctx->usr_ctx,
                                           QDMA_OFFSET_IND_CTXT_CMD,
                                           QDMA_IND_CTXT_CMD_BUSY_MASK,
                                           0,
                                           QDMA_POLL_REG_DEFAULT_TIMEOUT_US);

    if (ret == 0) {
        ret = -ETIMEDOUT;

        goto on_error;
    }


    ret = access_ctx->read_direct_reg_func(
        access_ctx->usr_ctx, QDMA_OFFSET_IND_CTXT_DATA, ((uint32_t*) data_ptr), num_words);


on_error:

    access_ctx->unlock_access_func(access_ctx->usr_ctx);

    if (ret > 0)
        ret = 0;

    return ret;
}

static int qdma_write_indirect(struct qdma_access_ctx* access_ctx,
                               enum ind_ctxt_cmd_sel   cmd_sel,
                               uint16_t                queue_id,
                               const void*             data_ptr,
                               uint32_t                num_words) {

    struct qdma_indirect_ctxt_regs reg_data;
    int                            ret;
    uint32_t                       index;
    const uint32_t*                src_word_ptr = (const uint32_t*) data_ptr;

    reg_data.cmd.word     = 0;
    reg_data.cmd.bits.qid = queue_id;
    reg_data.cmd.bits.op  = QDMA_CTXT_CMD_WR;
    reg_data.cmd.bits.sel = cmd_sel;

    access_ctx->lock_access_func(access_ctx->usr_ctx);

    if (num_words > QDMA_REG_IND_CTXT_REG_COUNT) {
        ret = -EINVAL;

        goto on_error;
    }

    for (index = 0; index < QDMA_REG_IND_CTXT_REG_COUNT; ++index) {
        reg_data.qdma_ind_ctxt_mask[index] = 0xffffffffU;

        if (index < num_words) {
            reg_data.qdma_ind_ctxt_data[index] = src_word_ptr[index];
        } else {
            reg_data.qdma_ind_ctxt_data[index] = 0;
        }
    }

    ret = access_ctx->write_direct_reg_func(access_ctx->usr_ctx,
                                            QDMA_OFFSET_IND_CTXT_DATA,
                                            (const uint32_t*) &reg_data,
                                            (QDMA_REG_IND_CTXT_REG_COUNT * 2) + 1);

    if (ret < 0)
        goto on_error;


    ret = access_ctx->poll_direct_reg_func(access_ctx->usr_ctx,
                                           QDMA_OFFSET_IND_CTXT_CMD,
                                           QDMA_IND_CTXT_CMD_BUSY_MASK,
                                           0,
                                           QDMA_POLL_REG_DEFAULT_TIMEOUT_US);

    if (ret == 0)
        ret = -ETIMEDOUT;

on_error:

    access_ctx->unlock_access_func(access_ctx->usr_ctx);

    if (ret > 0)
        ret = 0;

    return ret;
}

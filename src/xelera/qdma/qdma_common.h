/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright(c) 2022 Xelera Technologies GmbH
 */
#ifndef _QDMA_COMMON_H_
#define _QDMA_COMMON_H_

#include <stdbool.h>
#include <stdint.h>

#define QDMA_WORD_SIZE_BYTES 4U
#define QDMA_WORD_SIZE_BITS  32U

// 
#define QDMA_MAX_HW_QUEUE_COUNT 512

#define QDMA_POLL_REG_DEFAULT_TIMEOUT_US (5000000)

#define REG_POINTER_OFFSET(rtype, base, regbase, regoff) (rtype*) (((uint8_t*) base) + regbase + (regoff))

#define QDMA_WORD_COUNT(X) (sizeof(X) / QDMA_WORD_SIZE_BYTES)

#define QDMA_MEMORY_ALIGN 4096

#define QDMA_MAX_ERROR_ARGS 4

#define QDMA_NUM_ERROS_GRP_DSC      16
#define QDMA_NUM_ERROS_GRP_ST_C2H   18
#define QDMA_NUM_ERROS_GRP_ST_FATAL 16
#define QDMA_NUM_ERROS_GRP_ST_H2C   6
#define QDMA_NUM_ERROS_GRP_TRQ      6

#define QDMA_STATIC_ERROR_VEC_ENTRIES 16

#define QDMA_DEFAULT_GLBL_WB_INTV QDMA_WB_INTVL_128
#define QDMA_DEFAULT_GLBL_MAX_DESC_FETCH 6

#define QDMA_DEFAULT_H2C_PCIE_THROT_EN_DATA true
#define QDMA_DEFAULT_H2C_PCIE_THROT_DATA_THRESH 0x4000

enum qdma_type
{
    QDMA_TYPE_INVALID,
    QDMA_TYPE_SOFT_IP_QDMA,
    QDMA_TYPE_SOFT_IP_EQDMA,
    QDMA_TYPE_VERSAL_SOFT_IP,
    QDMA_TYPE_VERSAL_HARD_IP
};

enum qdma_global_writeback_interval
{
    QDMA_WB_INTVL_4,
    QDMA_WB_INTVL_8,
    QDMA_WB_INTVL_16,
    QDMA_WB_INTVL_32,
    QDMA_WB_INTVL_64,
    QDMA_WB_INTVL_128,
    QDMA_WB_INTVL_256,
    QDMA_WB_INTVL_512
};

enum qdma_pcie_access_width
{
    QDMA_ACCESS_WIDTH_128,
    QDMA_ACCESS_WIDTH_256,
    QDMA_ACCESS_WIDTH_512,
    QDMA_ACCESS_WIDTH_1024,
    QDMA_ACCESS_WIDTH_2048,
    QDMA_ACCESS_WIDTH_4096
};

enum qdma_error_group
{
    QDMA_ERR_GRP_NONE,
    QDMA_ERR_GRP_DSC,
    QDMA_ERR_GRP_ST_C2H,
    QDMA_ERR_GRP_ST_FATAL,
    QDMA_ERR_GRP_ST_H2C,
    QDMA_ERR_GRP_TRQ,
    QDMA_ERR_GRP_SBE,
    QDMA_ERR_GRP_DBE
};

struct qdma_error_descriptor
{
    int id;

    enum qdma_error_group error_group;

    const char* label;

    uint32_t flag_register;
    uint32_t mask_register;

    uint32_t bit_mask;
};


struct qdma_dsc_err_data
{
    uint16_t qid;
    uint16_t cidx;
    uint8_t type;
    uint8_t sub_type;
};

struct qdma_error_entry
{
    const struct qdma_error_descriptor* error_descriptor;

    uint32_t args[QDMA_MAX_ERROR_ARGS];
    uint32_t nvalid_args;

    bool dsc_err_info_valid;
    struct qdma_dsc_err_data dsc_err_data;
};

struct qdma_error_vector
{
    uint32_t capacity;
    uint32_t size;

    struct qdma_error_entry entries[];
};

struct qdma_static_error_vector
{
    struct qdma_error_vector vec;

    struct qdma_error_entry entries[QDMA_STATIC_ERROR_VEC_ENTRIES];
};

#define QDMA_STATIC_ERROR_VEC_INIT()          \
    {                                         \
        {QDMA_STATIC_ERROR_VEC_ENTRIES, 0}, { \
        }                                     \
    }

enum qdma_c2h_compl_entry_size
{
    QDMA_COMPL_ENTRY_8B,
    QDMA_COMPL_ENTRY_16B,
    QDMA_COMPL_ENTRY_32B,
    QDMA_COMPL_ENTRY_64B
};

// Typedef that allows visual differentiation between normal host virtual address and device mapped address
typedef void* _ioptr_t;


struct qdma_capabilities
{
    bool has_c2h_stream;
    bool has_c2h_mm;
    bool has_h2c_stream;
    bool has_h2c_mm;

    uint16_t max_num_supported_queues;

    uint16_t num_pfs;
};

struct qdma_hw_info
{
    enum qdma_type           type;
    uint8_t                  version;
    uint8_t                  config_id;
    uint16_t                 identifier;
    uint16_t                 system_id;
    bool                     is_hard_ip;
    struct qdma_capabilities capas;
};

struct qdma_c2h_writeback_info
{
    uint16_t min_writeback_thresh;
    uint16_t max_writeback_thresh;
    uint16_t min_writeback_timer_val;
    uint16_t max_writeback_timer_val;

    uint16_t default_writeback_thresh;
    uint16_t default_writeback_timer_val;
};

struct qdma_sw_descr_ctx
{
    uint16_t pidx;
    bool     irq_arm;
    uint8_t  func_id;
    bool     qen;
    bool     fcrd_en;
    bool     wbi_chk;
    bool     wbi_intvl_en;
    bool     at;
    uint8_t  fetch_max;
    uint8_t  rng_sz;
    uint8_t  dsc_sz;
    bool     bypass;
    bool     wbk_en;
    bool     irq_en;  //
    uint8_t  port_id;
    bool     irq_no_last;
    uint8_t  err;
    bool     error_irq_pending;
    bool     err_wb_sent;
    bool     irq_req;
    bool     mrkr_dis;
    bool     is_mm;
    uint64_t dsc_base;
    uint16_t vec;
    bool     int_aggr;
};

struct qdma_hw_descr_ctx
{
    uint16_t cidx;
    uint16_t crd_use;
    bool     dsc_pnd;
    bool     idl_stb_b;
    bool     evt_pnd;
    uint8_t  fetch_pnd;
};

struct qdma_credit_descr_ctx
{
    uint16_t credt;
};

struct qdma_completion_ctx
{
    bool                           en_stat_desc;
    bool                           en_int;
    uint8_t                        trig_mode;
    uint8_t                        fnc_id;
    uint8_t                        counter_idx;
    uint8_t                        timer_idx;
    uint8_t                        int_st;
    bool                           color;
    uint8_t                        qsize_idx;
    uint64_t                       baddr;
    enum qdma_c2h_compl_entry_size desc_size;
    uint16_t                       pidx;
    uint16_t                       cidx;
    bool                           valid;
    uint8_t                        err;
    bool                           user_trig_pend;
    bool                           timer_running;
    bool                           full_upd;
    bool                           ovf_chk_dis;
    bool                           at;
    uint16_t                       vec;
    bool                           int_aggr;
    bool                           dis_int_on_vf;
    bool                           dir_c2h;
    uint8_t                        port_id;
};

struct qdma_prefetch_ctx
{
    bool     bypass;
    uint8_t  buf_size_idx;
    uint8_t  port_id;
    bool     err;
    bool     pfch_en;
    bool     pfch;
    uint16_t sw_credit;
    bool     valid;
};

struct qdma_function_map_ctx
{
    uint16_t base_queue_id;
    uint16_t max_queue_count;
};

struct qdma_interrupt_ctx
{
    bool     valid;
    uint16_t vec;
    bool     int_st;
    bool     color;
    uint64_t baddr_4k;
    uint8_t  page_size;
    uint16_t pidx;
    bool     at;
    uint8_t  func;
};

struct qdma_debug_info
{
};

struct __attribute__((packed)) qdma_h2c_st_descr
{
    uint32_t metadata;

    uint16_t len;

    uint16_t reserved;

    uint64_t addr;

};

struct __attribute__((packed)) qdma_h2c_st_wb_status_descr
{
    uint16_t error;

    volatile uint16_t consumer_idx;

    volatile uint16_t producer_idx;

    uint16_t reserved1;
};

struct __attribute__((packed)) qdma_c2h_st_descr
{
    uint64_t addr;
};

union qdma_c2h_st_cmpt_descr
{
    volatile uint64_t data;
    struct __attribute__((packed))
    {
        /* For 2018.2 IP, this field determines the
         * Standard or User format of completion entry
         */
        volatile uint32_t data_frmt : 1;

        /* This field inverts every time PIDX wraps
         * the completion ring
         */
        volatile uint32_t color : 1;

        /* Indicates that C2H engine encountered
         * a descriptor error
         */
        volatile uint32_t err : 1;

        /* Indicates that the completion packet
         * consumes descriptor in C2H ring
         */
        volatile uint32_t desc_used : 1;

        /* Indicates length of the data packet */
        volatile uint32_t length : 16;

        /* Reserved field */
        volatile uint32_t user_rsv : 4;

        /* User logic defined data of
         * length based on CMPT entry
         * length
         */
        volatile uint8_t user_def[];
    };
};

struct __attribute__((packed)) qdma_completion_wb_status
{
    volatile uint16_t producer_index;

    volatile uint16_t consumer_index;

    uint32_t reserved;
};

//struct __attribute__((packed)) qdma_completion_status_descr
//{
//    volatile uint16_t producer_index;
//
//    volatile uint16_t consumer_index;
//
//    uint32_t reserved;
//};

const struct qdma_error_descriptor* qdma_get_err_descriptor(int id);

const char* qdma_get_err_group_name(uint32_t group_id);

static __inline uint16_t qdma_compl_size_to_bytes(enum qdma_c2h_compl_entry_size entry_size) {
    switch (entry_size) {
        case QDMA_COMPL_ENTRY_8B:
            return 8;
        case QDMA_COMPL_ENTRY_16B:
            return 16;
        case QDMA_COMPL_ENTRY_32B:
            return 32;
        case QDMA_COMPL_ENTRY_64B:
            return 64;
        default:
            return 0;
    }
}

static __inline uint8_t qdma_compl_size_to_reg(enum qdma_c2h_compl_entry_size entry_size) {
    switch (entry_size) {
        case QDMA_COMPL_ENTRY_8B:
            return 0;
        case QDMA_COMPL_ENTRY_16B:
            return 1;
        case QDMA_COMPL_ENTRY_32B:
            return 2;
        case QDMA_COMPL_ENTRY_64B:
            return 3;
        default:
            return 0;
    }
}

static __inline bool qdma_add_to_err_vec(struct qdma_error_vector*           err_vec,
                                         const struct qdma_error_descriptor* err_descr,
                                         const uint32_t*                     args,
                                         uint32_t                            num_args,
                                         const struct qdma_dsc_err_data* dsc_err_data) {
    struct qdma_error_entry* entry;
    uint32_t                 index;

    if (err_vec->size == err_vec->capacity) {
        return false;
    }

    entry = &err_vec->entries[err_vec->size];

    entry->error_descriptor = err_descr;

    if (!entry->error_descriptor)
        return false;

    if (num_args > QDMA_MAX_ERROR_ARGS)
        return false;

    for (index = 0; index < num_args; ++index) {
        entry->args[index] = args[index];
    }

    entry->nvalid_args = num_args;

    if(dsc_err_data) {
        entry->dsc_err_info_valid = true;
        entry->dsc_err_data = *dsc_err_data;
    }

    ++err_vec->size;

    return true;
}

#endif  // _QDMA_COMMON_H_

/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright(c) 2022 Xelera Technologies GmbH
 */
#include <xlsmartnic_regs.h>

#include <errno.h>

void reg_ser_init(struct reg_serialization_ctx* reg_ser_ctx, uint32_t* data_ptr, uint32_t num_words) {
    reg_ser_ctx->base_ptr = data_ptr;
    reg_ser_ctx->mem_size = num_words;
    reg_ser_ctx->bit_idx  = 0;
    reg_ser_ctx->word_idx = 0;
}

int reg_ser_fill(struct reg_serialization_ctx* reg_ser_ctx, uint32_t num_bits, bool set_clear) {
    uint32_t free_bits = (reg_ser_ctx->mem_size * XLSMARTNIC_REGSER_WORD_SIZE_BITS) -
                         ((reg_ser_ctx->word_idx * XLSMARTNIC_REGSER_WORD_SIZE_BITS) + reg_ser_ctx->bit_idx);

    if (num_bits > free_bits)
        return -ENOMEM;

    while (num_bits) {
        uint32_t* current_word_ptr = reg_ser_ctx->base_ptr + reg_ser_ctx->word_idx;

        uint8_t bits_cur_word = num_bits;

        if (bits_cur_word > (XLSMARTNIC_REGSER_WORD_SIZE_BITS - reg_ser_ctx->bit_idx)) {
            bits_cur_word = (XLSMARTNIC_REGSER_WORD_SIZE_BITS - reg_ser_ctx->bit_idx);
        }

        uint64_t mask = (1 << (bits_cur_word)) - 1;

        uint64_t masked_value = mask & (set_clear ? 0x01U : 0x00U);

        masked_value <<= reg_ser_ctx->bit_idx;

        *current_word_ptr |= masked_value;

        reg_ser_ctx->bit_idx += bits_cur_word;

        if (reg_ser_ctx->bit_idx == XLSMARTNIC_REGSER_WORD_SIZE_BITS) {
            reg_ser_ctx->bit_idx = 0;
            reg_ser_ctx->word_idx++;
        }

        num_bits -= bits_cur_word;
    }

    return 0;
}

int reg_ser_skip(struct reg_serialization_ctx* reg_ser_ctx, uint32_t num_bits) {
    uint32_t free_bits = (reg_ser_ctx->mem_size * XLSMARTNIC_REGSER_WORD_SIZE_BITS) -
                         ((reg_ser_ctx->word_idx * XLSMARTNIC_REGSER_WORD_SIZE_BITS) + reg_ser_ctx->bit_idx);

    if (num_bits > free_bits)
        return -ENOMEM;


    uint64_t combined_bit_idx = reg_ser_ctx->bit_idx + num_bits;

    uint32_t num_words          = combined_bit_idx / XLSMARTNIC_REGSER_WORD_SIZE_BITS;
    uint32_t num_remaining_bits = combined_bit_idx % XLSMARTNIC_REGSER_WORD_SIZE_BITS;

    reg_ser_ctx->word_idx += num_words;
    reg_ser_ctx->bit_idx = num_remaining_bits;


    return 0;
}

// Write `num_bits` bits from value starting at LSB to the buffer at current bit
// location
int reg_ser_write(struct reg_serialization_ctx* reg_ser_ctx, uint64_t value, uint32_t num_bits) {
    if (num_bits > 64)
        return -EINVAL;

    uint32_t free_bits = (reg_ser_ctx->mem_size * XLSMARTNIC_REGSER_WORD_SIZE_BITS) -
                         ((reg_ser_ctx->word_idx * XLSMARTNIC_REGSER_WORD_SIZE_BITS) + reg_ser_ctx->bit_idx);

    if (num_bits > free_bits)
        return -ENOMEM;

    while (num_bits) {
        uint32_t* current_word_ptr = reg_ser_ctx->base_ptr + reg_ser_ctx->word_idx;

        uint32_t bits_cur_word = num_bits;

        if (bits_cur_word > (XLSMARTNIC_REGSER_WORD_SIZE_BITS - reg_ser_ctx->bit_idx)) {
            bits_cur_word = (XLSMARTNIC_REGSER_WORD_SIZE_BITS - reg_ser_ctx->bit_idx);
        }

        uint64_t mask = (1ULL << (bits_cur_word)) - 1U;

        uint32_t masked_value = (uint32_t) (mask & value);

        value >>= bits_cur_word;

        masked_value <<= reg_ser_ctx->bit_idx;

        *current_word_ptr |= masked_value;

        reg_ser_ctx->bit_idx += bits_cur_word;

        if (reg_ser_ctx->bit_idx == XLSMARTNIC_REGSER_WORD_SIZE_BITS) {
            reg_ser_ctx->bit_idx = 0;
            reg_ser_ctx->word_idx++;
        }

        num_bits -= bits_cur_word;
    }

    return 0;
}

int reg_ser_read(struct reg_serialization_ctx* reg_ser_ctx, uint64_t* value, uint32_t num_bits) {
    if (num_bits > 64)
        return -EINVAL;

    uint32_t free_bits = (reg_ser_ctx->mem_size * XLSMARTNIC_REGSER_WORD_SIZE_BITS) -
                         ((reg_ser_ctx->word_idx * XLSMARTNIC_REGSER_WORD_SIZE_BITS) + reg_ser_ctx->bit_idx);

    if (num_bits > free_bits)
        return -ENOMEM;

    *value = 0;

    while (num_bits) {
        uint32_t* current_word_ptr = reg_ser_ctx->base_ptr + reg_ser_ctx->word_idx;

        uint32_t bits_cur_word = num_bits;

        if (bits_cur_word > (XLSMARTNIC_REGSER_WORD_SIZE_BITS - reg_ser_ctx->bit_idx)) {
            bits_cur_word = (XLSMARTNIC_REGSER_WORD_SIZE_BITS - reg_ser_ctx->bit_idx);
        }

        uint32_t mask = (1UL << (bits_cur_word)) - 1U;

        uint32_t shifted_word = (*current_word_ptr) >> reg_ser_ctx->bit_idx;

        uint32_t masked_value = mask & shifted_word;

        *value <<= bits_cur_word;

        *value |= masked_value;

        reg_ser_ctx->bit_idx += bits_cur_word;

        if (reg_ser_ctx->bit_idx == XLSMARTNIC_REGSER_WORD_SIZE_BITS) {
            reg_ser_ctx->bit_idx = 0;
            reg_ser_ctx->word_idx++;
        }

        num_bits -= bits_cur_word;
    }

    return 0;
}

/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright(c) 2022 Xelera Technologies GmbH
 */
#ifndef _XLSMARTNIC_REGS_H_
#define _XLSMARTNIC_REGS_H_

#include <stdbool.h>
#include <stdint.h>

// All register definitions specific to our SmartNIC logic should go here!
// QDMA registers are defined in the QDMA abstraction layer

#define XLSMARTNIC_REG_SIZE_ETH_CMAC   0x2000
#define XLSMARTNIC_REG_OFFSET_ETH_CMAC(idx) (0x00070000 + (idx * XLSMARTNIC_REG_SIZE_ETH_CMAC) + 0x10) // TODO: 0x10 is a bug offset in the partial design

#define XLSMARTNIC_REG_OFFSET_TOEPLITZ 0x6000
#define XLSMARTNIC_REG_SIZE_TOEPLITZ   0x1000

#define XLSMARTNIC_REG_OFFSET_PF_CTRL 0x7000
#define XLSMARTNIC_REG_SIZE_PF_CTRL   0x1000

#define XLSMARTNIC_REG_OFFSET_ROUTER_RX 0x8000
#define XLSMARTNIC_REG_SIZE_ROUTER_RX   0x2000

#define XLSMARTNIC_REG_OFFSET_ROUTER_TX 0xA000
#define XLSMARTNIC_REG_SIZE_ROUTER_TX   0x2000

#ifdef XLSMARTNIC_ENABLE_PERF_TEST
#define XLSMARTNIC_REG_OFFSET_IO 0x4000
#define XLSMARTNIC_REG_SIZE_IO 0x1000

// Aparently there is a new traffic mon register interface?!
#define XLSMARTNIC_REG_OFFSET_TRAFFIC_MON 0x2000
#define XLSMARTNIC_REG_SIZE_TRAFFIC_MON 0x200
#endif // XLSMARTNIC_ENABLE_PERF_TEST

#define XLSMARTNIC_TX_METADATA_CHKSUM_IPV4 (1U << 0)

#define XLSMARTNIC_RX_METADATA_CHKSUM_IPV4_VALIDATED (1U << 0)
#define XLSMARTNIC_RX_METADATA_CHKSUM_IPV4_VALID     (1U << 1)

// Bit flags specifying different offloads. Used for devices registers that don't know anything about DPDK
#define XLSMARTNIC_OFFLOAD_IP4_CHECKSUM_OUTER  ((uint32_t)1U << 0)
#define XLSMARTNIC_OFFLOAD_UDP_CHECKSUM_OUTER  ((uint32_t)1U << 1)
#define XLSMARTNIC_OFFLOAD_TCP_CHECKSUM_OUTER  ((uint32_t)1U << 2)
#define XLSMARTNIC_OFFLOAD_ICMP_CHECKSUM_OUTER ((uint32_t)1U << 3)


#define XLSMARTNIC_REGSER_WORD_SIZE_BITS 32

#define REG_BIT_SERIALIZE(ser_ctx, val, num_bits) \
    do {                                          \
        uint64_t tmp = (uint64_t) (val);          \
        reg_ser_write(&ser_ctx, tmp, num_bits);   \
    } while (0)

#define REG_BIT_DESERIALIZE(ser_ctx, val, num_bits) \
    do {                                            \
        uint64_t tmp;                               \
        reg_ser_read(&ser_ctx, &tmp, num_bits);     \
        val = (typeof((val))) tmp;                  \
    } while (0)

#define REG_BIT_ZERO(ser_ctx, num_bits)          \
    do {                                         \
        reg_ser_fill(&ser_ctx, num_bits, false); \
    } while (0)

#define REG_BIT_SKIP(ser_ctx, num_bits)   \
    do {                                  \
        reg_ser_skip(&ser_ctx, num_bits); \
    } while (0)

struct reg_serialization_ctx
{
    uint32_t* base_ptr;
    uint32_t  mem_size;  // Number of 32bit wor

    uint32_t bit_idx;
    uint32_t word_idx;
};

void reg_ser_init(struct reg_serialization_ctx* reg_ser_ctx, uint32_t* data_ptr, uint32_t num_words);

int reg_ser_fill(struct reg_serialization_ctx* reg_ser_ctx, uint32_t num_bits, bool set_clear);

int reg_ser_skip(struct reg_serialization_ctx* reg_ser_ctx, uint32_t num_bits);

int reg_ser_write(struct reg_serialization_ctx* reg_ser_ctx, uint64_t value, uint32_t num_bits);

int reg_ser_read(struct reg_serialization_ctx* reg_ser_ctx, uint64_t* value, uint32_t num_bits);

#endif  // _XLSMARTNIC_REGS_H_

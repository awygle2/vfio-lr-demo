/* SPDX-License-Identifier: BSD-3-Clause
 * Copyright(c) 2022 Xelera Technologies GmbH
 */
#ifndef _QDMA_REGISTERS_H_
#define _QDMA_REGISTERS_H_

#include "qdma_common.h"


#define QDMA_CSR              0x0U
#define QDMA_TRQ_SEL_QUEUE_PF 0x18000U
#define QDMA_PF_MAILBOX       0x22400U
#define QDMA_TRQ_MSIX         0x30000U


#define QDMA_CFG_BLOCK_IDENTIFIER    0x0000U
#define QDMA_CFG_BLOCK_MAX_PLSZ_INFO 0x0008U
#define QDMA_CFG_BLOCK_MAX_RDRQ_INFO 0x000CU
#define QDMA_CFG_BLOCK_SYSID         0x0010U
#define     QDMA_CFG_BLOCK_SYS_ID_HARD_IP_BIT 0x10000U
#define     QDMA_CFG_BLOCK_SYS_ID_MASK        0x0FFFFU

#define QDMA_CFG_BLOCK_PCIE_CONTROL  0x001CU
#define     QDMA_CFG_BLOCK_PCIE_CONTROL_RELAXED_ORDERING_NBITS 1
#define     QDMA_CFG_BLOCK_PCIE_CONTROL_RRQ_DISABLE_NBITS 1
#define     QDMA_CFG_BLOCK_PCIE_CONTROL_RESERVED0_NBITS 14
#define     QDMA_CFG_BLOCK_PCIE_CONTROL_MGMT_AXIL_CTRL_NBITS 2
#define     QDMA_CFG_BLOCK_PCIE_CONTROL_RESERVED1_NBITS 14

#define QDMA_CFG_BLOCK_MSI_ENABLE 0x0020U

#define QDMA_CFG_BLOCK_ID_MAGIC 0x1FD3

#define QDMA_GLBL2_SYSTEM_ID 0x0130U
#define QDMA_GLBL2_MISC_CAP  0x0134U
#define     QDMA_GLBL2_MISC_CAP_DEV_TYPE_START 28
#define     QDMA_GLBL2_MISC_CAP_DEV_TYPE_MASK  0xf0000000U
#define     QDMA_GLBL2_MISC_CAP_IP_TYPE_START  20
#define     QDMA_GLBL2_MISC_CAP_IP_TYPE_MASK   0x00f00000U

#define QDMA_DEV_TYPE_SOFT_ID 0
#define QDMA_DEV_TYPE_VERSAL_ID 1

#define QDMA_IP_TYPE_SOFT_QDMA_ID 0
#define QDMA_IP_TYPE_SOFT_EQDMA_ID 1

#define QDMA_IP_TYPE_VERSAL_HARD_QDMA_ID 0
#define QDMA_IP_TYPE_VERSAL_SOFT_QDMA_ID 1

#define QDMA_CFG_ACCESS_WIDTH_FIELD_NBITS_MAX_ISSUED  3
#define QDMA_CFG_ACCESS_WIDTH_FIELD_NBITS_RESERVED1   1
#define QDMA_CFG_ACCESS_WIDTH_FIELD_NBITS_MAX_PROG    3
#define QDMA_CFG_ACCESS_WIDTH_FIELD_NBITS_RESERVED2  25

#define QDMA_GLBL2_CHANNEL_INST_REG 0x0114U
#define QDMA_GLBL2_CHANNEL_MDMA     0x0118U
#define QDMA_GLBL2_CHANNEL_STREAM   0x011CU

#define QDMA_GLBL2_CHANNEL_QDMA_CAP 0x0120U

#define QDMA_GLOBAL_RING_SIZE_CFG_BASE 0x0204U

// Error registers
#define TOTAL_LEAF_ERROR_AGGREGATORS                        7

#define QDMA_OFFSET_GLBL_ERR_INT                            0xB04
#define QDMA_OFFSET_GLBL_ERR_STAT                           0x248
#define QDMA_OFFSET_GLBL_ERR_MASK                           0x24C
#define     QDMA_GLBL_ERR_STAT_RAM_SBE_MASK                 0x1
#define     QDMA_GLBL_ERR_STAT_RAM_DBE_MASK                 0x2
#define     QDMA_GLBL_ERR_STAT_DSC_MASK                     0x4
#define     QDMA_GLBL_ERR_STAT_TRQ_MASK                     0x8
#define     QDMA_GLBL_ERR_STAT_ST_C2H_MASK                  0x100
#define     QDMA_GLBL_ERR_STAT_IND_CTX_MASK                 0x7e00
#define     QDMA_GLBL_ERR_STAT_BRIDGE_MASK                  0x8000
#define     QDMA_GLBL_ERR_STAT_ST_H2C_MASK                  0x10000

#define QDMA_OFFSET_C2H_ERR_STAT                            0xAF0
#define QDMA_OFFSET_C2H_ERR_MASK                            0xAF4
#define QDMA_OFFSET_C2H_FATAL_ERR_STAT                      0xAF8
#define QDMA_OFFSET_C2H_FATAL_ERR_MASK                      0xAFC
#define QDMA_OFFSET_H2C_ERR_STAT                            0xE00
#define QDMA_OFFSET_H2C_ERR_MASK                            0xE04
#define QDMA_OFFSET_GLBL_DSC_ERR_STAT                       0x254
#define QDMA_OFFSET_GLBL_DSC_ERR_MASK                       0x258
#define QDMA_OFFSET_GLBL_DSC_ERR_LOG0                       0x25C
#define QDMA_OFFSET_GLBL_DSC_ERR_LOG1                       0x260
#define QDMA_OFFSET_GLBL_TRQ_ERR_STAT                       0x264
#define QDMA_OFFSET_GLBL_TRQ_ERR_MASK                       0x268
#define QDMA_OFFSET_GLBL_TRQ_LOG_A                          0x26C
#define QDMA_OFFSET_GLBL_GLBL_INTERRUPT_CFG                 0x2C4

#define QDMA_OFFSET_RAM_SBE_STAT                            0xF4
#define QDMA_OFFSET_RAM_SBE_MASK                            0xF0
#define QDMA_OFFSET_RAM_DBE_STAT                            0xFC
#define QDMA_OFFSET_RAM_DBE_MASK                            0xF8

#define QDMA_C2H_TIMER_THRESH_CFG_BASE_OFFSET 0x0A00U
#define QDMA_C2H_COUNT_THRESH_CFG_BASE_OFFSET 0x0A40U
#define QDMA_C2H_BUFFER_SIZE_CFG_BASE_OFFSET  0x0AB0U

#define QDMA_C2H_PFCH_CFG_1_BASE_OFFSET 0xA80
#define QDMA_C2H_PFCH_CFG_1_FIELD_NBITS_PFCH_QCNT 16
#define QDMA_C2H_PFCH_CFG_1_FIELD_NBITS_EVT_QCNT_TH 16

#define QDMA_C2H_PFCH_CFG_2_BASE_OFFSET 0xA84
#define QDMA_C2H_PFCH_CFG_2_FIELD_NBITS_NUM_PFCH 6
#define QDMA_C2H_PFCH_CFG_2_FIELD_NBITS_VAR_DESC_NUM_PFCH 6
#define QDMA_C2H_PFCH_CFG_2_FIELD_NBITS_PFCH_LL_SZ_TH 16
#define QDMA_C2H_PFCH_CFG_2_FIELD_NBITS_VAR_DESC_NO_DROP 1
#define QDMA_C2H_PFCH_CFG_2_FIELD_NBITS_REMAINING 3

#define QDMA_C2H_PFCH_CFG_BASE_OFFSET 0xB08
#define QDMA_C2H_PFCH_CFG_FIELD_NBITS_PFCH_FL_TH 16
#define QDMA_C2H_PFCH_CFG_FIELD_NBITS_EVT_PFCH_FL_TH 16

#define QDMA_C2H_INT_TIMER_TICK_BASE_OFFSET 0xB0C
#define QDMA_C2H_INT_TIMER_TICK_FIELD_NBITS_TICKRATE 32

#define QDMA_C2H_WRB_COAL_CFG_BASE_OFFSET 0xB50
#define QDMA_C2H_WRB_COAL_CFG_FIELD_NBITS_DONE_GLB_FLUSH 1
#define QDMA_C2H_WRB_COAL_CFG_FIELD_NBITS_SET_GLB_FLUSH 1
#define QDMA_C2H_WRB_COAL_CFG_FIELD_NBITS_TICK_CNT 12
#define QDMA_C2H_WRB_COAL_CFG_FIELD_NBITS_TICK_VAL 12
#define QDMA_C2H_WRB_COAL_CFG_FIELD_NBITS_MAX_BUF_SZ 8


#define QDMA_GLBL_DSC_CFG 0x250
#define QDMA_GLBL_DSC_CFG_FIELD_NBITS_WB_ACC_INT 3
#define QDMA_GLBL_DSC_CFG_FIELD_NBITS_MAX_DSC_FETCH 3
#define QDMA_GLBL_DSC_CFG_FIELD_NBITS_RSVD1 2
#define QDMA_GLBL_DSC_CFG_FIELD_NBITS_CTXT_FER_DIS 1
#define QDMA_GLBL_DSC_CFG_FIELD_NBITS_UNC_OVR_COR 1
#define QDMA_GLBL_DSC_CFG_FIELD_NBITS_RSVD2 22

#define QDMA_C2H_PFCH_CACHE_DEPTH_BASE_OFFSET   0xBE0
#define QDMA_C2H_WRB_COAL_BUF_DEPTH_BASE_OFFSET 0xBE4

#define QDMA_H2C_REQ_THROT_PCIE 0xE24
#define QDMA_H2C_REQ_THROT_PCIE_NBITS_DATA_THRESH 18
#define QDMA_H2C_REQ_THROT_PCIE_NBITS_REQ_THROT_EN_DATA 1
#define QDMA_H2C_REQ_THROT_PCIE_NBITS_REQ_THROT 12
#define QDMA_H2C_REQ_THROT_PCIE_NBITS_REQ_EN_REQ 1

#define QDMA_GLOBAL_RING_SIZE_CFG_COUNT 16
#define QDMA_C2H_TIMER_THRESH_CFG_COUNT 16
#define QDMA_C2H_COUNT_THRESH_CFG_COUNT 16
#define QDMA_C2H_BUFFER_SIZE_CFG_COUNT  16



/*
 * Q Context programming (indirect)
 */

#define QDMA_REG_IND_CTXT_REG_COUNT 8
#define QDMA_REG_IND_CTXT_WCNT_1    1
#define QDMA_REG_IND_CTXT_WCNT_2    2
#define QDMA_REG_IND_CTXT_WCNT_3    3
#define QDMA_REG_IND_CTXT_WCNT_4    4
#define QDMA_REG_IND_CTXT_WCNT_5    5
#define QDMA_REG_IND_CTXT_WCNT_6    6
#define QDMA_REG_IND_CTXT_WCNT_7    7
#define QDMA_REG_IND_CTXT_WCNT_8    8

/* ------------------------- QDMA_TRQ_SEL_IND (0x00800) ----------------*/

#define QDMA_OFFSET_IND_CTXT_DATA   0x804
#define QDMA_OFFSET_IND_CTXT_MASK   0x824
#define QDMA_OFFSET_IND_CTXT_CMD    0x844
#define QDMA_IND_CTXT_CMD_BUSY_MASK 0x1


/* ------------------------- QDMA Software Context ----------------------*/

#define QDMA_SW_CTX_FIELD_NBITS_PIDX         16
#define QDMA_SW_CTX_FIELD_NBITS_IRQ_ARM      1
#define QDMA_SW_CTX_FIELD_NBITS_FNC_ID       8
#define QDMA_SW_CTX_FIELD_NBITS_RSVD1        7
#define QDMA_SW_CTX_FIELD_NBITS_QEN          1
#define QDMA_SW_CTX_FIELD_NBITS_FCRD_EN      1
#define QDMA_SW_CTX_FIELD_NBITS_WBL_CHK      1
#define QDMA_SW_CTX_FIELD_NBITS_WBL_INTVL_EN 1
#define QDMA_SW_CTX_FIELD_NBITS_AT           1
#define QDMA_SW_CTX_FIELD_NBITS_FETCH_MAX    4
#define QDMA_SW_CTX_FIELD_NBITS_RSVD2        3
#define QDMA_SW_CTX_FIELD_NBITS_RNG_SZ       4
#define QDMA_SW_CTX_FIELD_NBITS_DSC_SZ       2
#define QDMA_SW_CTX_FIELD_NBITS_BYPASS       1
#define QDMA_SW_CTX_FIELD_NBITS_MM_CHN       1
#define QDMA_SW_CTX_FIELD_NBITS_WBK_EN       1
#define QDMA_SW_CTX_FIELD_NBITS_IRQ_EN       1
#define QDMA_SW_CTX_FIELD_NBITS_PORT_ID      3
#define QDMA_SW_CTX_FIELD_NBITS_IRQ_NO_LAST  1
#define QDMA_SW_CTX_FIELD_NBITS_ERR          2
#define QDMA_SW_CTX_FIELD_NBITS_ERR_WB_SENT  1
#define QDMA_SW_CTX_FIELD_NBITS_IRQ_REQ      1
#define QDMA_SW_CTX_FIELD_NBITS_MRKR_DIS     1
#define QDMA_SW_CTX_FIELD_NBITS_IS_MM        1
#define QDMA_SW_CTX_FIELD_NBITS_DSC_BASE     64
#define QDMA_SW_CTX_FIELD_NBITS_VEC          11
#define QDMA_SW_CTX_FIELD_NBITS_INT_AGGR     1

/* ------------------------- QDMA Hardware Context ----------------------*/

#define QDMA_HW_CTX_FIELD_NBITS_CIDX      16
#define QDMA_HW_CTX_FIELD_NBITS_CRD_USE   16
#define QDMA_HW_CTX_FIELD_NBITS_RSVD1     8
#define QDMA_HW_CTX_FIELD_NBITS_DSC_PND   1
#define QDMA_HW_CTX_FIELD_NBITS_IDL_STP_B 1
#define QDMA_HW_CTX_FIELD_NBITS_EVT_PND   1
#define QDMA_HW_CTX_FIELD_NBITS_FETCH_PND 4

/* ------------------------- QDMA Credit Context ------------------------*/

#define QDMA_CRD_CTX_FIELD_NBITS_CREDT 16
#define QDMA_CRD_CTX_FIELD_NBITS_RSVD1 16

/* ------------------------- QDMA Prefetch Context ----------------------*/

#define QDMA_PREFETCH_CTX_FIELD_NBITS_BYPASS       1
#define QDMA_PREFETCH_CTX_FIELD_NBITS_BUF_SIZE_IDX 4
#define QDMA_PREFETCH_CTX_FIELD_NBITS_PORT_ID      3
#define QDMA_PREFETCH_CTX_FIELD_NBITS_RSVD1        18
#define QDMA_PREFETCH_CTX_FIELD_NBITS_ERR          1
#define QDMA_PREFETCH_CTX_FIELD_NBITS_PFCH_EN      1
#define QDMA_PREFETCH_CTX_FIELD_NBITS_PFCH         1
#define QDMA_PREFETCH_CTX_FIELD_NBITS_SW_CRDT      16
#define QDMA_PREFETCH_CTX_FIELD_NBITS_VALID        1


/* ------------------------- QDMA Completion Context --------------------*/

#define QDMA_CMPL_CTX_FIELD_NBITS_EN_STAT_DESC   1
#define QDMA_CMPL_CTX_FIELD_NBITS_EN_INT         1
#define QDMA_CMPL_CTX_FIELD_NBITS_TRIG_MODE      3
#define QDMA_CMPL_CTX_FIELD_NBITS_FNC_ID         8
#define QDMA_CMPL_CTX_FIELD_NBITS_RSVD1          4
#define QDMA_CMPL_CTX_FIELD_NBITS_COUNTER_IDX    4
#define QDMA_CMPL_CTX_FIELD_NBITS_TIMER_IDX      4
#define QDMA_CMPL_CTX_FIELD_NBITS_INT_ST         2
#define QDMA_CMPL_CTX_FIELD_NBITS_COLOR          1
#define QDMA_CMPL_CTX_FIELD_NBITS_QSIZE_IDX      4
#define QDMA_CMPL_CTX_FIELD_NBITS_BADDR          58
#define QDMA_CMPL_CTX_FIELD_NBITS_DESC_SIZE      2
#define QDMA_CMPL_CTX_FIELD_NBITS_PIDX           16
#define QDMA_CMPL_CTX_FIELD_NBITS_CIDX           16
#define QDMA_CMPL_CTX_FIELD_NBITS_VALID          1
#define QDMA_CMPL_CTX_FIELD_NBITS_ERR            2
#define QDMA_CMPL_CTX_FIELD_NBITS_USER_TRIG_PEND 1
#define QDMA_CMPL_CTX_FIELD_NBITS_TIMER_RUNNING  1
#define QDMA_CMPL_CTX_FIELD_NBITS_FULL_UPD       1
#define QDMA_CMPL_CTX_FIELD_NBITS_OVF_CHK_DIS    1
#define QDMA_CMPL_CTX_FIELD_NBITS_AT             1
#define QDMA_CMPL_CTX_FIELD_NBITS_VEC            11
#define QDMA_CMPL_CTX_FIELD_NBITS_INT_AGGR       1
#define QDMA_CMPL_CTX_FIELD_NBITS_DIS_INT_ON_VF  1
#define QDMA_CMPL_CTX_FIELD_NBITS_RSVD2          1
#define QDMA_CMPL_CTX_FIELD_NBITS_DIR_C2H        1
#define QDMA_CMPL_CTX_FIELD_NBITS_RSVD3          28
#define QDMA_CMPL_CTX_FIELD_NBITS_BADDR4_LOW     4
#define QDMA_CMPL_CTX_FIELD_NBITS_RSVD4          1
#define QDMA_CMPL_CTX_FIELD_NBITS_PORT_ID        3
#define QDMA_CMPL_CTX_FIELD_NBITS_RSVD5          17


/* ------------------------- QDMA Interrupt Context ---------------------*/

#define QDMA_IRQ_CTX_FIELD_NBITS_VALID     1
#define QDMA_IRQ_CTX_FIELD_NBITS_VEC       11
#define QDMA_IRQ_CTX_FIELD_NBITS_RSVD1     1
#define QDMA_IRQ_CTX_FIELD_NBITS_INT_ST    1
#define QDMA_IRQ_CTX_FIELD_NBITS_COLOR     1
#define QDMA_IRQ_CTX_FIELD_NBITS_BADDR_4K  52
#define QDMA_IRQ_CTX_FIELD_NBITS_PAGE_SIZE 3
#define QDMA_IRQ_CTX_FIELD_NBITS_PIDX      12
#define QDMA_IRQ_CTX_FIELD_NBITS_AT        1
#define QDMA_IRQ_CTX_FIELD_NBITS_RSVD2     31
#define QDMA_IRQ_CTX_FIELD_NBITS_FUNC      12

/* ------------------------- QDMA Function Map Context -------------------*/

#define QDMA_FMAP_CTX_FIELD_NBITS_QID_BASE 11
#define QDMA_FMAP_CTX_FIELD_NBITS_RSVD1 21
#define QDMA_FMAP_CTX_FIELD_NBITS_QID_MAX 12
#define QDMA_FMAP_CTX_FIELD_NBITS_RSVD2 20

/* ------------------------- QDMA Direct Register Offsets ----------------*/

#define QDMA_DMAP_SEL_INT_CIDX      0x18000U
#define QDMA_DMAP_SEL_H2C_DSC_PIDX  0x18004U
#define QDMA_DMAP_SEL_C2H_DSC_PIDX  0x18008U
#define QDMA_DMAP_SEL_C2H_CMPT_CIDX 0x1800cU

#define QDMA_DMAP_SEL_H2C_DSC_PIDX_IRQ_BIT 0x00000100U

#define QDMA_DMAP_SEL_C2H_DSC_PIDX_IRQ_BIT 0x00000100U

#define QDMA_DMAP_SEL_C2H_CMPT_CIDX_IRQ_BIT   0x10000000U
#define QDMA_DMAP_SEL_C2H_CMPT_CIDX_EN_STS_WB 0x07000000U

#define QDMA_DMAP_REG_QUEUE_OFFSET 0x10U
#define QDMA_DMAP_RAG_QUEUE_SHIFT 4

#define QDMA_FLR_CTRL_REGISTER 0x22500
#define QDMA_FLR_CTRL_BITMASK  0x00000001U

union qdma_sw_descr_ctx_reg
{
    uint32_t word;

    struct __attribute__((packed))
    {
        uint16_t producer_index;

        uint16_t irq_arm     : 1;  // Who ever thought of distributing bits to values this way... ?!
        uint16_t function_id : 8;
        uint16_t reserved    : 7;

        uint8_t fetch_credit : 1;
        uint8_t wbi_check    : 1;
        uint8_t wbi_inv_en   : 1;
    };


};

union qdma_glbl2_channel_inst
{
    uint32_t word;

    struct __attribute__((packed))
    {
        uint8_t  h2c_mm_is_inst : 4;
        uint8_t  reserved1      : 4;
        uint8_t  c2h_mm_is_inst : 4;
        uint8_t  reserved2      : 4;
        uint8_t  h2c_st_is_inst : 1;
        uint8_t  c2h_st_is_inst : 1;
        uint16_t reserved3      : 14;
    };

};

union qdma_glbl2_channel_mdma
{
    uint32_t word;

    struct __attribute__((packed))
    {
        int8_t   h2c_mm_is_qdma : 4;
        uint8_t  reserved1      : 4;
        uint8_t  c2h_mm_is_qdma : 4;
        uint8_t  reserved2      : 4;
        uint8_t  h2c_st_is_qdma : 1;
        uint8_t  c2h_st_is_qdma : 1;
        uint16_t reserved3      : 14;
    };

};

union qdma_ram_sbe_sts_1_a
{
    struct __attribute__((packed))
    {
        uint32_t rc_rrq_odd_ram_sbe     : 1;
        uint32_t tag_odd_ram_sbe        : 1;
        uint32_t tag_even_ram_sbe       : 1;
        uint32_t pfch_ctx_cam_ram_0_sbe : 1;
        uint32_t pfch_ctx_cam_ram_sbe   : 1;
    };
};

union qdma_ram_dbe_sts_1_a
{
    struct __attribute__((packed))
    {
        uint32_t rc_rrq_odd_ram_dbe     : 1;
        uint32_t tag_odd_ram_dbe        : 1;
        uint32_t tag_even_ram_dbe       : 1;
        uint32_t pfch_ctx_cam_ram_0_dbe : 1;
        uint32_t pfch_ctx_cam_ram_dbe   : 1;
    };
};

union qdma_glbl_err_stat
{
    uint32_t word;

    struct __attribute__((packed))
    {
        uint32_t err_ram_sbe      : 1;
        uint32_t err_ram_dbe      : 1;
        uint32_t err_dsc          : 1;
        uint32_t err_trq          : 1;
        uint32_t err_h2c_mm_0     : 1;
        uint32_t err_h2c_mm_1     : 1;
        uint32_t err_c2h_mm_0     : 1;
        uint32_t err_c2h_mm_1     : 1;
        uint32_t err_c2h_st       : 1;
        uint32_t ind_ctxt_cmd_err : 6;
        uint32_t err_bdg          : 1;
        uint32_t err_h2c_st       : 1;
        uint32_t err_fab          : 1;
        uint32_t reserved         : 5;
    };

};

union qdma_glbl_dsc_err_sts
{
    struct __attribute__((packed))
    {
        uint32_t poison     : 1;
        uint32_t ur_ca      : 1;
        uint32_t bcnt       : 1;
        uint32_t param      : 1;
        uint32_t addr       : 1;
        uint32_t tag        : 1;
        uint32_t flr        : 1;
        uint32_t timeout    : 1;
        uint32_t dat_poison : 1;
        uint32_t reserved1  : 2;
        uint32_t flr_cancel : 1;
        uint32_t dma        : 1;
        uint32_t dsc        : 1;
        uint32_t rq_cancel  : 1;
        uint32_t dbe        : 1;
    };
};

union qdma_glbl_dsc_err_log0
{
    struct __attribute__((packed))
    {
        uint32_t qid      : 14;
        uint32_t reserved : 16;
        uint32_t sel      : 1;
        uint32_t valid    : 1;
    };
};

union qdma_glbl_dsc_err_log1
{
    struct __attribute__((packed))
    {
        uint32_t err_type  : 5;
        uint32_t sub_type  : 4;
        uint32_t reserved1 : 3;
        uint32_t cidx      : 16;
        uint32_t reserved2 : 4;
    };
};

union qdma_glbl_trq_err_log
{
    struct __attribute__((packed))
    {
        uint32_t address : 17;
        uint32_t func    : 10;
        uint32_t target  : 4;
        uint32_t src     : 1;
    };
};

union qdma_c2h_pfch_cache_depth
{
    uint32_t word;

    struct __attribute__((packed))
    {
        uint8_t cache_depth;

        uint8_t reserved1;

        uint8_t max_stdbuf;

        uint8_t reserved;
    };
};


// QDMA DMAP Registers

union qdma_dmap_sel_int_cidx
{
    uint32_t word;

    struct __attribute__((packed))
    {
        uint16_t sw_cidx;
        uint8_t  ring_idx;
        uint8_t  reserved;
    };
};

union qdma_dmap_sel_h2c_dsc_pidx
{
    uint32_t word;

    struct __attribute__((packed))
    {
        uint16_t h2c_pidx;
        uint16_t irq_arm  : 1;
        uint16_t reserved : 15;
    };
};

union qdma_dmap_sel_c2h_dsc_pidx
{
    uint32_t word;

    struct __attribute__((packed))
    {
        uint16_t c2h_pidx;
        uint16_t irq_arm  : 1;
        uint16_t reserved : 15;
    };
};

union qdma_dmap_sel_cmpt_cidx
{
    uint32_t word;

    struct __attribute__((packed))
    {
        uint16_t wrb_cidx;
        uint16_t c2h_count_thresh    : 4;
        uint16_t c2h_timer_cnt_index : 4;
        uint16_t trigger_mode        : 3;
        uint16_t en_sts_desc_wrb     : 1;
        uint16_t irq_en_wrb          : 1;
        uint16_t reserved            : 3;
    };
};

#endif  // _QDMA_REGISTERS_H_

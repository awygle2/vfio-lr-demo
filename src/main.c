#define _GNU_SOURCE

#include <linux/vfio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <endian.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include <immintrin.h>

#include "eqdma_soft_reg.h"
#include "qdma_access_common.h"

uint32_t read_reg(uint8_t* base, uint32_t offset) {
	return *((uint32_t*)(base + offset));
}

void write_reg(uint8_t* base, uint32_t offset, uint32_t value) {
	printf("writing 0x%08x to 0x%08x\n", value, offset);
	*((uint32_t*)(base + offset)) = value;
}

#pragma region "Xilinx Stuff"

/*
 * hw_monitor_reg() - polling a register repeatly until
 *	(the register value & mask) == val or time is up
 *
 * return -QDMA_BUSY_IIMEOUT_ERR if register value didn't match, 0 other wise
 */
int hw_monitor_reg(uint8_t *base, uint32_t reg, uint32_t mask,
		uint32_t val, uint32_t interval_us, uint32_t timeout_us)
{
	int count;
	uint32_t v;

	if (!interval_us)
		interval_us = QDMA_REG_POLL_DFLT_INTERVAL_US;
	if (!timeout_us)
		timeout_us = QDMA_REG_POLL_DFLT_TIMEOUT_US;

	count = timeout_us / interval_us;

	do {
		v = read_reg(base, reg);
		if ((v & mask) == val)
			return QDMA_SUCCESS;
		usleep(interval_us);
	} while (--count);

	v = read_reg(base, reg);
	if ((v & mask) == val)
		return QDMA_SUCCESS;

	return -QDMA_ERR_HWACC_BUSY_TIMEOUT;
}

/*
 * eqdma_indirect_reg_clear() - helper function to clear indirect
 *				context registers.
 *
 * return -QDMA_ERR_HWACC_BUSY_TIMEOUT if register
 *	value didn't match, QDMA_SUCCESS other wise
 */
static int eqdma_indirect_reg_clear(uint8_t *base,
		enum ind_ctxt_cmd_sel sel, uint16_t hw_qid)
{
	union qdma_ind_ctxt_cmd cmd;

	/* set command register */
	cmd.word = 0;
	cmd.bits.qid = hw_qid;
	cmd.bits.op = QDMA_CTXT_CMD_CLR;
	cmd.bits.sel = sel;
	write_reg(base, EQDMA_IND_CTXT_CMD_ADDR, cmd.word);

	/* check if the operation went through well */
	
	if (hw_monitor_reg(base, EQDMA_IND_CTXT_CMD_ADDR,
			IND_CTXT_CMD_BUSY_MASK, 0,
			QDMA_REG_POLL_DFLT_INTERVAL_US,
			QDMA_REG_POLL_DFLT_TIMEOUT_US)) {
		return -QDMA_ERR_HWACC_BUSY_TIMEOUT;
	}
}


/*
 * eqdma_indirect_reg_write() - helper function to write indirect
 *				context registers.
 *
 * return -QDMA_ERR_HWACC_BUSY_TIMEOUT if register
 *	value didn't match, QDMA_SUCCESS other wise
 */
static int eqdma_indirect_reg_write(uint8_t *base, enum ind_ctxt_cmd_sel sel,
		uint16_t hw_qid, uint32_t *data, uint16_t cnt)
{
	uint32_t index, reg_addr;
	struct qdma_indirect_ctxt_regs regs;
	uint32_t *wr_data = (uint32_t *)&regs;

	/* write the context data */
	for (index = 0; index < QDMA_IND_CTXT_DATA_NUM_REGS; index++) {
		if (index < cnt)
			regs.qdma_ind_ctxt_data[index] = data[index];
		else
			regs.qdma_ind_ctxt_data[index] = 0;
		regs.qdma_ind_ctxt_mask[index] = 0xFFFFFFFF;
	}

	regs.cmd.word = 0;
	regs.cmd.bits.qid = hw_qid;
	regs.cmd.bits.op = QDMA_CTXT_CMD_WR;
	regs.cmd.bits.sel = sel;
	reg_addr = EQDMA_IND_CTXT_DATA_ADDR;

	for (index = 0; index < ((2 * QDMA_IND_CTXT_DATA_NUM_REGS) + 1);
		 index++, reg_addr += sizeof(uint32_t))
		write_reg(base, reg_addr, wr_data[index]);

	/* check if the operation went through well */
	if (hw_monitor_reg(base, EQDMA_IND_CTXT_CMD_ADDR,
			IND_CTXT_CMD_BUSY_MASK, 0,
			QDMA_REG_POLL_DFLT_INTERVAL_US,
			QDMA_REG_POLL_DFLT_TIMEOUT_US)) {
		return -QDMA_ERR_HWACC_BUSY_TIMEOUT;
	}

	return QDMA_SUCCESS;
}

/*****************************************************************************/
/**
 * eqdma_fmap_context_write() - create fmap context and program it
 *
 * @dev_hndl:	device handle
 * @func_id:	function id of the device
 * @config:	pointer to the fmap data strucutre
 *
 * Return:	0   - success and < 0 - failure
 *****************************************************************************/
static int eqdma_fmap_context_write(uint8_t *base, 
		   const struct qdma_fmap_cfg *config, uint16_t func_id)
{
	uint32_t fmap[EQDMA_FMAP_NUM_WORDS] = {0};
	uint16_t num_words_count = 0;
	enum ind_ctxt_cmd_sel sel = QDMA_CTXT_SEL_FMAP;

	if (!base || !config) {
		return -QDMA_ERR_INV_PARAM;
	}

	fmap[num_words_count++] =
		FIELD_SET(EQDMA_FMAP_CTXT_W0_QID_MASK, config->qbase);
	fmap[num_words_count++] =
		FIELD_SET(EQDMA_FMAP_CTXT_W1_QID_MAX_MASK, config->qmax);

	return eqdma_indirect_reg_write(base, sel, func_id,
			fmap, num_words_count);
}


/*****************************************************************************/
/**
 * eqdma_sw_context_write() - create sw context and program it
 *
 * @dev_hndl:	device handle
 * @c2h:	is c2h queue
 * @hw_qid:	hardware qid of the queue
 * @ctxt:	pointer to the SW context data strucutre
 *
 * Return:	0   - success and < 0 - failure
 *****************************************************************************/
static int eqdma_sw_context_write(void *base, uint8_t c2h,
			 uint16_t hw_qid,
			 const struct qdma_descq_sw_ctxt *ctxt)
{
	uint32_t sw_ctxt[EQDMA_SW_CONTEXT_NUM_WORDS] = {0};
	uint16_t num_words_count = 0;
	uint32_t pasid_l, pasid_h;
	uint32_t virtio_desc_base_l, virtio_desc_base_m, virtio_desc_base_h;
	enum ind_ctxt_cmd_sel sel = c2h ?
			QDMA_CTXT_SEL_SW_C2H : QDMA_CTXT_SEL_SW_H2C;

	/* Input args check */
	if (!base || !ctxt) {
		return -QDMA_ERR_INV_PARAM;
	}

	pasid_l =
		FIELD_GET(EQDMA_SW_CTXT_PASID_GET_L_MASK, ctxt->pasid);
	pasid_h =
		FIELD_GET(EQDMA_SW_CTXT_PASID_GET_H_MASK, ctxt->pasid);

	virtio_desc_base_l = (uint32_t)FIELD_GET(
		EQDMA_SW_CTXT_VIRTIO_DSC_BASE_GET_L_MASK,
		ctxt->virtio_dsc_base);
	virtio_desc_base_m = (uint32_t)FIELD_GET(
		EQDMA_SW_CTXT_VIRTIO_DSC_BASE_GET_M_MASK,
		ctxt->virtio_dsc_base);
	virtio_desc_base_h = (uint32_t)FIELD_GET(
		EQDMA_SW_CTXT_VIRTIO_DSC_BASE_GET_H_MASK,
		ctxt->virtio_dsc_base);

	sw_ctxt[num_words_count++] =
		FIELD_SET(SW_IND_CTXT_DATA_W0_PIDX_MASK, ctxt->pidx) |
		FIELD_SET(SW_IND_CTXT_DATA_W0_IRQ_ARM_MASK, ctxt->irq_arm) |
		FIELD_SET(SW_IND_CTXT_DATA_W0_FNC_MASK, ctxt->fnc_id);

	sw_ctxt[num_words_count++] =
		FIELD_SET(SW_IND_CTXT_DATA_W1_QEN_MASK, ctxt->qen) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_FCRD_EN_MASK, ctxt->frcd_en) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_WBI_CHK_MASK, ctxt->wbi_chk) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_WBI_INTVL_EN_MASK,
				  ctxt->wbi_intvl_en) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_AT_MASK, ctxt->at) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_FETCH_MAX_MASK, ctxt->fetch_max) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_RNG_SZ_MASK, ctxt->rngsz_idx) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_DSC_SZ_MASK, ctxt->desc_sz) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_BYPASS_MASK, ctxt->bypass) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_MM_CHN_MASK, ctxt->mm_chn) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_WBK_EN_MASK, ctxt->wbk_en) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_IRQ_EN_MASK, ctxt->irq_en) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_PORT_ID_MASK, ctxt->port_id) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_IRQ_NO_LAST_MASK,
			ctxt->irq_no_last) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_ERR_MASK, ctxt->err) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_ERR_WB_SENT_MASK,
			ctxt->err_wb_sent) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_IRQ_REQ_MASK, ctxt->irq_req) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_MRKR_DIS_MASK, ctxt->mrkr_dis) |
		FIELD_SET(SW_IND_CTXT_DATA_W1_IS_MM_MASK, ctxt->is_mm);

	sw_ctxt[num_words_count++] = ctxt->ring_bs_addr & 0xffffffff;
	sw_ctxt[num_words_count++] = (ctxt->ring_bs_addr >> 32) & 0xffffffff;

	sw_ctxt[num_words_count++] =
		FIELD_SET(SW_IND_CTXT_DATA_W4_VEC_MASK, ctxt->vec) |
		FIELD_SET(SW_IND_CTXT_DATA_W4_INT_AGGR_MASK, ctxt->intr_aggr) |
		FIELD_SET(SW_IND_CTXT_DATA_W4_DIS_INTR_ON_VF_MASK,
				ctxt->dis_intr_on_vf) |
		FIELD_SET(SW_IND_CTXT_DATA_W4_VIRTIO_EN_MASK,
				ctxt->virtio_en) |
		FIELD_SET(SW_IND_CTXT_DATA_W4_PACK_BYP_OUT_MASK,
				ctxt->pack_byp_out) |
		FIELD_SET(SW_IND_CTXT_DATA_W4_IRQ_BYP_MASK, ctxt->irq_byp) |
		FIELD_SET(SW_IND_CTXT_DATA_W4_HOST_ID_MASK, ctxt->host_id) |
		FIELD_SET(SW_IND_CTXT_DATA_W4_PASID_L_MASK, pasid_l);

	sw_ctxt[num_words_count++] =
		FIELD_SET(SW_IND_CTXT_DATA_W5_PASID_H_MASK, pasid_h) |
		FIELD_SET(SW_IND_CTXT_DATA_W5_PASID_EN_MASK, ctxt->pasid_en) |
		FIELD_SET(SW_IND_CTXT_DATA_W5_VIRTIO_DSC_BASE_L_MASK,
				virtio_desc_base_l);

	sw_ctxt[num_words_count++] =
		FIELD_SET(SW_IND_CTXT_DATA_W6_VIRTIO_DSC_BASE_M_MASK,
				virtio_desc_base_m);

	sw_ctxt[num_words_count++] =
		FIELD_SET(SW_IND_CTXT_DATA_W7_VIRTIO_DSC_BASE_H_MASK,
				virtio_desc_base_h);

	return eqdma_indirect_reg_write(base, sel, hw_qid,
			sw_ctxt, num_words_count);
}

/*****************************************************************************/
/**
 * eqdma_pfetch_context_write() - create prefetch context and program it
 *
 * @dev_hndl:	device handle
 * @hw_qid:	hardware qid of the queue
 * @ctxt:	pointer to the prefetch context data strucutre
 *
 * Return:	0   - success and < 0 - failure
 *****************************************************************************/
static int eqdma_pfetch_context_write(uint8_t *base, uint16_t hw_qid,
		const struct qdma_descq_prefetch_ctxt *ctxt)
{
	uint32_t pfetch_ctxt[EQDMA_PFETCH_CONTEXT_NUM_WORDS] = {0};
	enum ind_ctxt_cmd_sel sel = QDMA_CTXT_SEL_PFTCH;
	uint32_t sw_crdt_l, sw_crdt_h;
	uint16_t num_words_count = 0;

	if (!base || !ctxt) {
		return -QDMA_ERR_INV_PARAM;
	}

	sw_crdt_l =
		FIELD_GET(QDMA_PFTCH_CTXT_SW_CRDT_GET_L_MASK, ctxt->sw_crdt);
	sw_crdt_h =
		FIELD_GET(QDMA_PFTCH_CTXT_SW_CRDT_GET_H_MASK, ctxt->sw_crdt);

	pfetch_ctxt[num_words_count++] =
		FIELD_SET(PREFETCH_CTXT_DATA_W0_BYPASS_MASK, ctxt->bypass) |
		FIELD_SET(PREFETCH_CTXT_DATA_W0_BUF_SZ_IDX_MASK,
				ctxt->bufsz_idx) |
		FIELD_SET(PREFETCH_CTXT_DATA_W0_PORT_ID_MASK, ctxt->port_id) |
		FIELD_SET(PREFETCH_CTXT_DATA_W0_NUM_PFCH_MASK,
				ctxt->num_pftch) |
		FIELD_SET(PREFETCH_CTXT_DATA_W0_VAR_DESC_MASK,
				ctxt->var_desc) |
		FIELD_SET(PREFETCH_CTXT_DATA_W0_ERR_MASK, ctxt->err) |
		FIELD_SET(PREFETCH_CTXT_DATA_W0_PFCH_EN_MASK, ctxt->pfch_en) |
		FIELD_SET(PREFETCH_CTXT_DATA_W0_PFCH_MASK, ctxt->pfch) |
		FIELD_SET(PREFETCH_CTXT_DATA_W0_SW_CRDT_L_MASK, sw_crdt_l);

	pfetch_ctxt[num_words_count++] =
		FIELD_SET(PREFETCH_CTXT_DATA_W1_SW_CRDT_H_MASK, sw_crdt_h) |
		FIELD_SET(PREFETCH_CTXT_DATA_W1_VALID_MASK, ctxt->valid);

	return eqdma_indirect_reg_write(base, sel, hw_qid,
			pfetch_ctxt, num_words_count);
}

/*****************************************************************************/
/**
 * eqdma_cmpt_context_write() - create completion context and program it
 *
 * @dev_hndl:	device handle
 * @hw_qid:	hardware qid of the queue
 * @ctxt:	pointer to the cmpt context data strucutre
 *
 * Return:	0   - success and < 0 - failure
 *****************************************************************************/
static int eqdma_cmpt_context_write(uint8_t *base, uint16_t hw_qid,
			   const struct qdma_descq_cmpt_ctxt *ctxt)
{
	uint32_t cmpt_ctxt[EQDMA_CMPT_CONTEXT_NUM_WORDS] = {0};
	uint16_t num_words_count = 0;
	uint32_t baddr4_high_l, baddr4_high_h,
			baddr4_low, pidx_l, pidx_h, pasid_l, pasid_h;
	enum ind_ctxt_cmd_sel sel = QDMA_CTXT_SEL_CMPT;

	/* Input args check */
	if (!base || !ctxt) {
		return -QDMA_ERR_INV_PARAM;
	}

	if (ctxt->trig_mode > QDMA_CMPT_UPDATE_TRIG_MODE_TMR_CNTR) {
		return -QDMA_ERR_INV_PARAM;
	}

	baddr4_high_l = (uint32_t)FIELD_GET(EQDMA_COMPL_CTXT_BADDR_HIGH_L_MASK,
			ctxt->bs_addr);
	baddr4_high_h = (uint32_t)FIELD_GET(EQDMA_COMPL_CTXT_BADDR_HIGH_H_MASK,
			ctxt->bs_addr);
	baddr4_low = (uint32_t)FIELD_GET(EQDMA_COMPL_CTXT_BADDR_LOW_MASK,
			ctxt->bs_addr);

	pidx_l = FIELD_GET(QDMA_COMPL_CTXT_PIDX_GET_L_MASK, ctxt->pidx);
	pidx_h = FIELD_GET(QDMA_COMPL_CTXT_PIDX_GET_H_MASK, ctxt->pidx);

	pasid_l =
		FIELD_GET(EQDMA_CMPL_CTXT_PASID_GET_L_MASK, ctxt->pasid);
	pasid_h =
		FIELD_GET(EQDMA_CMPL_CTXT_PASID_GET_H_MASK, ctxt->pasid);

	cmpt_ctxt[num_words_count++] =
		FIELD_SET(CMPL_CTXT_DATA_W0_EN_STAT_DESC_MASK,
				ctxt->en_stat_desc) |
		FIELD_SET(CMPL_CTXT_DATA_W0_EN_INT_MASK, ctxt->en_int) |
		FIELD_SET(CMPL_CTXT_DATA_W0_TRIG_MODE_MASK, ctxt->trig_mode) |
		FIELD_SET(CMPL_CTXT_DATA_W0_FNC_ID_MASK, ctxt->fnc_id) |
		FIELD_SET(CMPL_CTXT_DATA_W0_CNTER_IX_MASK,
				ctxt->counter_idx) |
		FIELD_SET(CMPL_CTXT_DATA_W0_TIMER_IX_MASK, ctxt->timer_idx) |
		FIELD_SET(CMPL_CTXT_DATA_W0_INT_ST_MASK, ctxt->in_st) |
		FIELD_SET(CMPL_CTXT_DATA_W0_COLOR_MASK, ctxt->color) |
		FIELD_SET(CMPL_CTXT_DATA_W0_QSIZE_IX_MASK, ctxt->ringsz_idx);

	cmpt_ctxt[num_words_count++] =
		FIELD_SET(CMPL_CTXT_DATA_W1_BADDR4_HIGH_L_MASK, baddr4_high_l);

	cmpt_ctxt[num_words_count++] =
		FIELD_SET(CMPL_CTXT_DATA_W2_BADDR4_HIGH_H_MASK, baddr4_high_h) |
		FIELD_SET(CMPL_CTXT_DATA_W2_DESC_SIZE_MASK, ctxt->desc_sz) |
		FIELD_SET(CMPL_CTXT_DATA_W2_PIDX_L_MASK, pidx_l);

	cmpt_ctxt[num_words_count++] =
		FIELD_SET(CMPL_CTXT_DATA_W3_PIDX_H_MASK, pidx_h) |
		FIELD_SET(CMPL_CTXT_DATA_W3_CIDX_MASK, ctxt->cidx) |
		FIELD_SET(CMPL_CTXT_DATA_W3_VALID_MASK, ctxt->valid) |
		FIELD_SET(CMPL_CTXT_DATA_W3_ERR_MASK, ctxt->err) |
		FIELD_SET(CMPL_CTXT_DATA_W3_USER_TRIG_PEND_MASK,
				ctxt->user_trig_pend);

	cmpt_ctxt[num_words_count++] =
		FIELD_SET(CMPL_CTXT_DATA_W4_TIMER_RUNNING_MASK,
				ctxt->timer_running) |
		FIELD_SET(CMPL_CTXT_DATA_W4_FULL_UPD_MASK, ctxt->full_upd) |
		FIELD_SET(CMPL_CTXT_DATA_W4_OVF_CHK_DIS_MASK,
				ctxt->ovf_chk_dis) |
		FIELD_SET(CMPL_CTXT_DATA_W4_AT_MASK, ctxt->at) |
		FIELD_SET(CMPL_CTXT_DATA_W4_VEC_MASK, ctxt->vec) |
		FIELD_SET(CMPL_CTXT_DATA_W4_INT_AGGR_MASK, ctxt->int_aggr) |
		FIELD_SET(CMPL_CTXT_DATA_W4_DIS_INTR_ON_VF_MASK,
				ctxt->dis_intr_on_vf) |
		FIELD_SET(CMPL_CTXT_DATA_W4_VIO_MASK, ctxt->vio) |
		FIELD_SET(CMPL_CTXT_DATA_W4_DIR_C2H_MASK, ctxt->dir_c2h) |
		FIELD_SET(CMPL_CTXT_DATA_W4_HOST_ID_MASK, ctxt->host_id) |
		FIELD_SET(CMPL_CTXT_DATA_W4_PASID_L_MASK, pasid_l);

	cmpt_ctxt[num_words_count++] =
		FIELD_SET(CMPL_CTXT_DATA_W5_PASID_H_MASK, pasid_h) |
		FIELD_SET(CMPL_CTXT_DATA_W5_PASID_EN_MASK,
				ctxt->pasid_en) |
		FIELD_SET(CMPL_CTXT_DATA_W5_BADDR4_LOW_MASK,
				baddr4_low) |
		FIELD_SET(CMPL_CTXT_DATA_W5_VIO_EOP_MASK, ctxt->vio_eop) |
		FIELD_SET(CMPL_CTXT_DATA_W5_SH_CMPT_MASK, ctxt->sh_cmpt);

	return eqdma_indirect_reg_write(base, sel, hw_qid,
			cmpt_ctxt, num_words_count);
}

void qdma_write_csr_values(uint8_t *base, uint32_t reg_offst,
		uint32_t idx, uint32_t cnt, const uint32_t *values)
{
	uint32_t index, reg_addr;

	for (index = idx; index < (idx + cnt); index++) {
		reg_addr = reg_offst + (index * sizeof(uint32_t));
		write_reg(base, reg_addr, values[index - idx]);
	}
}

/*****************************************************************************/
/**
 * eqdma_get_device_attributes() - Function to get the qdma device
 * attributes
 *
 * @dev_hndl:	device handle
 * @dev_info:	pointer to hold the device info
 *
 * Return:	0   - success and < 0 - failure
 *****************************************************************************/
int eqdma_get_device_attributes(uint8_t *base,
		struct qdma_dev_attributes *dev_info)
{
	uint8_t count = 0;
	uint32_t reg_val = 0;

	if (!base) {
		return -QDMA_ERR_INV_PARAM;
	}
	if (!dev_info) {
		return -QDMA_ERR_INV_PARAM;
	}

	/* number of PFs */
	reg_val = read_reg(base, QDMA_OFFSET_GLBL2_PF_BARLITE_INT);
	if (FIELD_GET(QDMA_GLBL2_PF0_BAR_MAP_MASK, reg_val))
		count++;
	if (FIELD_GET(QDMA_GLBL2_PF1_BAR_MAP_MASK, reg_val))
		count++;
	if (FIELD_GET(QDMA_GLBL2_PF2_BAR_MAP_MASK, reg_val))
		count++;
	if (FIELD_GET(QDMA_GLBL2_PF3_BAR_MAP_MASK, reg_val))
		count++;
	dev_info->num_pfs = count;

	/* Number of Qs */
	reg_val = read_reg(base, EQDMA_GLBL2_CHANNEL_CAP_ADDR);
	dev_info->num_qs =
			FIELD_GET(GLBL2_CHANNEL_CAP_MULTIQ_MAX_MASK, reg_val);

	/* FLR present */
	reg_val = read_reg(base, EQDMA_GLBL2_MISC_CAP_ADDR);
	dev_info->mailbox_en = FIELD_GET(EQDMA_GLBL2_MAILBOX_EN_MASK,
		reg_val);
	dev_info->flr_present = FIELD_GET(EQDMA_GLBL2_FLR_PRESENT_MASK,
		reg_val);
	dev_info->mm_cmpt_en  = 0;
	dev_info->debug_mode = FIELD_GET(EQDMA_GLBL2_DBG_MODE_EN_MASK,
		reg_val);
	dev_info->desc_eng_mode = FIELD_GET(EQDMA_GLBL2_DESC_ENG_MODE_MASK,
		reg_val);

	/* ST/MM enabled? */
	reg_val = read_reg(base, EQDMA_GLBL2_CHANNEL_MDMA_ADDR);
	dev_info->st_en = (FIELD_GET(GLBL2_CHANNEL_MDMA_C2H_ST_MASK, reg_val)
		&& FIELD_GET(GLBL2_CHANNEL_MDMA_H2C_ST_MASK, reg_val)) ? 1 : 0;
	dev_info->mm_en = (FIELD_GET(GLBL2_CHANNEL_MDMA_C2H_ENG_MASK, reg_val)
		&& FIELD_GET(GLBL2_CHANNEL_MDMA_H2C_ENG_MASK, reg_val)) ? 1 : 0;

	/* num of mm channels */
	/* TODO : Register not yet defined for this. Hard coding it to 1.*/
	dev_info->mm_channel_max = 1;

	dev_info->qid2vec_ctx = 0;
	dev_info->cmpt_ovf_chk_dis = 1;
	dev_info->mailbox_intr = 1;
	dev_info->sw_desc_64b = 1;
	dev_info->cmpt_desc_64b = 1;
	dev_info->dynamic_bar = 1;
	dev_info->legacy_intr = 1;
	dev_info->cmpt_trig_count_timer = 1;

	return QDMA_SUCCESS;
}

/*****************************************************************************/
/**
 * eqdma_set_default_global_csr() - function to set the global CSR register to
 * default values. The value can be modified later by using the set/get csr
 * functions
 *
 * @dev_hndl:	device handle
 *
 * Return:	0   - success and < 0 - failure
 *****************************************************************************/
int eqdma_set_default_global_csr(uint8_t *base)
{
	int rv = QDMA_SUCCESS;

	/* Default values */
	uint32_t cfg_val = 0, reg_val = 0;
	uint32_t rng_sz[QDMA_NUM_RING_SIZES] = {2049, 65, 129, 193, 257, 385,
		513, 769, 1025, 1537, 3073, 4097, 6145, 8193, 12289, 16385};
	uint32_t tmr_cnt[QDMA_NUM_C2H_TIMERS] = {1, 2, 4, 5, 8, 10, 15, 20, 25,
		30, 50, 75, 100, 125, 150, 200};
	uint32_t cnt_th[QDMA_NUM_C2H_COUNTERS] = {2, 4, 8, 16, 24, 32, 48, 64,
		80, 96, 112, 128, 144, 160, 176, 192};
	uint32_t buf_sz[QDMA_NUM_C2H_BUFFER_SIZES] = {4096, 256, 512, 1024,
		2048, 3968, 4096, 4096, 4096, 4096, 4096, 4096, 4096, 8192,
		9018, 16384};
	struct qdma_dev_attributes dev_cap;
	uint32_t eqdma_ip_version;

	if (!base) {
		return -QDMA_ERR_INV_PARAM;
	}

	eqdma_get_device_attributes(base, &dev_cap);

	/* Configuring CSR registers */
	/* Global ring sizes */
	qdma_write_csr_values(base, EQDMA_GLBL_RNG_SZ_1_ADDR, 0,
			QDMA_NUM_RING_SIZES, rng_sz);

	if (dev_cap.st_en || dev_cap.mm_cmpt_en) {
		/* Counter thresholds */
		qdma_write_csr_values(base, EQDMA_C2H_CNT_TH_ADDR, 0,
				QDMA_NUM_C2H_COUNTERS, cnt_th);

		/* Timer Counters */
		qdma_write_csr_values(base, EQDMA_C2H_TIMER_CNT_ADDR, 0,
				QDMA_NUM_C2H_TIMERS, tmr_cnt);

		/* Writeback Interval */
			reg_val =
				FIELD_SET(GLBL_DSC_CFG_MAXFETCH_MASK,
						DEFAULT_MAX_DSC_FETCH) |
				FIELD_SET(GLBL_DSC_CFG_WB_ACC_INT_MASK,
						DEFAULT_WRB_INT);

			write_reg(base, EQDMA_GLBL_DSC_CFG_ADDR,
					reg_val);
	}

	if (dev_cap.st_en) {
		/* Buffer Sizes */
		qdma_write_csr_values(base, EQDMA_C2H_BUF_SZ_ADDR, 0,
				QDMA_NUM_C2H_BUFFER_SIZES, buf_sz);

		/* Prefetch Configuration */
			reg_val = read_reg(base,
					EQDMA_C2H_PFCH_CACHE_DEPTH_ADDR);
			cfg_val = FIELD_GET(C2H_PFCH_CACHE_DEPTH_MASK, reg_val);
			reg_val = FIELD_SET(C2H_PFCH_CFG_1_QCNT_MASK,
					(cfg_val >> 2)) |
				FIELD_SET(C2H_PFCH_CFG_1_EVT_QCNT_TH_MASK,
						((cfg_val >> 2) - 4));

			write_reg(base, EQDMA_C2H_PFCH_CFG_1_ADDR,
					reg_val);

			reg_val = read_reg(base,
					EQDMA_C2H_PFCH_CFG_2_ADDR);
			reg_val |= FIELD_SET(C2H_PFCH_CFG_2_FENCE_MASK, 1);
			write_reg(base, EQDMA_C2H_PFCH_CFG_2_ADDR,
					reg_val);

		/* C2H interrupt timer tick */
		write_reg(base, EQDMA_C2H_INT_TIMER_TICK_ADDR,
				DEFAULT_C2H_INTR_TIMER_TICK);

		/* C2h Completion Coalesce Configuration */
		cfg_val = read_reg(base,
				EQDMA_C2H_WRB_COAL_BUF_DEPTH_ADDR);
		reg_val =
			FIELD_SET(C2H_WRB_COAL_CFG_TICK_CNT_MASK,
					DEFAULT_CMPT_COAL_TIMER_CNT) |
			FIELD_SET(C2H_WRB_COAL_CFG_TICK_VAL_MASK,
					DEFAULT_CMPT_COAL_TIMER_TICK) |
			FIELD_SET(C2H_WRB_COAL_CFG_MAX_BUF_SZ_MASK, cfg_val);
		write_reg(base, EQDMA_C2H_WRB_COAL_CFG_ADDR, reg_val);

		/* H2C throttle Configuration*/
			reg_val =
				FIELD_SET(H2C_REQ_THROT_PCIE_DATA_THRESH_MASK,
						EQDMA_H2C_THROT_DATA_THRESH) |
				FIELD_SET(H2C_REQ_THROT_PCIE_EN_DATA_MASK,
						EQDMA_THROT_EN_DATA) |
				FIELD_SET(H2C_REQ_THROT_PCIE_MASK,
						EQDMA_H2C_THROT_REQ_THRESH) |
				FIELD_SET(H2C_REQ_THROT_PCIE_EN_REQ_MASK,
						EQDMA_THROT_EN_REQ);

			write_reg(base, EQDMA_H2C_REQ_THROT_PCIE_ADDR,
					reg_val);
	}

	return QDMA_SUCCESS;
}

/*****************************************************************************/
/**
 * eqdma_indirect_intr_context_write() - create indirect interrupt context
 *					and program it
 *
 * @dev_hndl:   device handle
 * @ring_index: indirect interrupt ring index
 * @ctxt:	pointer to the interrupt context data strucutre
 *
 * Return:	0   - success and < 0 - failure
 *****************************************************************************/
static int eqdma_indirect_intr_context_write(uint8_t *base,
		uint16_t ring_index, const struct qdma_indirect_intr_ctxt *ctxt)
{
	uint32_t intr_ctxt[EQDMA_IND_INTR_CONTEXT_NUM_WORDS] = {0};
	enum ind_ctxt_cmd_sel sel = QDMA_CTXT_SEL_INT_COAL;
	uint32_t baddr_l, baddr_m, baddr_h, pasid_l, pasid_h;
	uint16_t num_words_count = 0;

	if (!base || !ctxt) {
		return -QDMA_ERR_INV_PARAM;
	}

	baddr_l = (uint32_t)FIELD_GET(QDMA_INTR_CTXT_BADDR_GET_L_MASK,
			ctxt->baddr_4k);
	baddr_m = (uint32_t)FIELD_GET(QDMA_INTR_CTXT_BADDR_GET_M_MASK,
			ctxt->baddr_4k);
	baddr_h = (uint32_t)FIELD_GET(QDMA_INTR_CTXT_BADDR_GET_H_MASK,
			ctxt->baddr_4k);

	pasid_l =
		FIELD_GET(EQDMA_INTR_CTXT_PASID_GET_L_MASK, ctxt->pasid);
	pasid_h =
		FIELD_GET(EQDMA_INTR_CTXT_PASID_GET_H_MASK, ctxt->pasid);

	intr_ctxt[num_words_count++] =
		FIELD_SET(INTR_CTXT_DATA_W0_VALID_MASK, ctxt->valid) |
		FIELD_SET(INTR_CTXT_DATA_W0_VEC_MASK, ctxt->vec) |
		FIELD_SET(INTR_CTXT_DATA_W0_INT_ST_MASK, ctxt->int_st) |
		FIELD_SET(INTR_CTXT_DATA_W0_COLOR_MASK, ctxt->color) |
		FIELD_SET(INTR_CTXT_DATA_W0_BADDR_4K_L_MASK, baddr_l);

	intr_ctxt[num_words_count++] =
		FIELD_SET(INTR_CTXT_DATA_W1_BADDR_4K_M_MASK, baddr_m);

	intr_ctxt[num_words_count++] =
		FIELD_SET(INTR_CTXT_DATA_W2_BADDR_4K_H_MASK, baddr_h) |
		FIELD_SET(INTR_CTXT_DATA_W2_PAGE_SIZE_MASK, ctxt->page_size) |
		FIELD_SET(INTR_CTXT_DATA_W2_PIDX_MASK, ctxt->pidx) |
		FIELD_SET(INTR_CTXT_DATA_W2_AT_MASK, ctxt->at) |
		FIELD_SET(INTR_CTXT_DATA_W2_HOST_ID_MASK, ctxt->host_id) |
		FIELD_SET(INTR_CTXT_DATA_W2_PASID_L_MASK, pasid_l);

	intr_ctxt[num_words_count++] =
		FIELD_SET(INTR_CTXT_DATA_W3_PASID_H_MASK, pasid_h) |
		FIELD_SET(INTR_CTXT_DATA_W3_PASID_EN_MASK, ctxt->pasid_en) |
		FIELD_SET(INTR_CTXT_DATA_W3_FUNC_MASK, ctxt->func_id);

	return eqdma_indirect_reg_write(base, sel, ring_index,
			intr_ctxt, num_words_count);
}

struct eqdma_hw_err_info {
	enum eqdma_error_idx idx;
	const char *err_name;
	uint32_t mask_reg_addr;
	uint32_t stat_reg_addr;
	uint32_t leaf_err_mask;
	uint32_t global_err_mask;
};

static struct eqdma_hw_err_info eqdma_err_info[EQDMA_ERRS_ALL] = {
	/* Descriptor errors */
	{
		EQDMA_DSC_ERR_POISON,
		"Poison error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_POISON_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
	},
	{
		EQDMA_DSC_ERR_UR_CA,
		"Unsupported request or completer aborted error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_UR_CA_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
	},
	{
		EQDMA_DSC_ERR_BCNT,
		"Unexpected Byte count in completion error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_BCNT_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
	},
	{
		EQDMA_DSC_ERR_PARAM,
		"Parameter mismatch error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_PARAM_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
	},
	{
		EQDMA_DSC_ERR_ADDR,
		"Address mismatch error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_ADDR_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
	},
	{
		EQDMA_DSC_ERR_TAG,
		"Unexpected tag error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_TAG_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
	},
	{
		EQDMA_DSC_ERR_FLR,
		"FLR error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_FLR_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
	},
	{
		EQDMA_DSC_ERR_TIMEOUT,
		"Timed out error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_TIMEOUT_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
	},
	{
		EQDMA_DSC_ERR_DAT_POISON,
		"Poison data error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_DAT_POISON_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
		 
	},
	{
		EQDMA_DSC_ERR_FLR_CANCEL,
		"Descriptor fetch cancelled due to FLR error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_FLR_CANCEL_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
		 
	},
	{
		EQDMA_DSC_ERR_DMA,
		"DMA engine error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_DMA_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
		 
	},
	{
		EQDMA_DSC_ERR_DSC,
		"Invalid PIDX update error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_DSC_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
		 
	},
	{
		EQDMA_DSC_ERR_RQ_CANCEL,
		"Descriptor fetch cancelled due to disable register status error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_RQ_CANCEL_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
		 
	},
	{
		EQDMA_DSC_ERR_DBE,
		"UNC_ERR_RAM_DBE error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_DBE_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
		 
	},
	{
		EQDMA_DSC_ERR_SBE,
		"UNC_ERR_RAM_SBE error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_SBE_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
		 
	},
	{
		EQDMA_DSC_ERR_PORT_ID,
		"Port ID Error",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		GLBL_DSC_ERR_STS_PORT_ID_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
		 
	},
	{
		EQDMA_DSC_ERR_ALL,
		"All Descriptor errors",
		EQDMA_GLBL_DSC_ERR_MSK_ADDR,
		EQDMA_GLBL_DSC_ERR_STS_ADDR,
		EQDMA_GLBL_DSC_ERR_ALL_MASK,
		GLBL_ERR_STAT_ERR_DSC_MASK,
		 
	},

	/* TRQ errors */
	{
		EQDMA_TRQ_ERR_CSR_UNMAPPED,
		"Access targeted unmapped register space via CSR pathway error",
		EQDMA_GLBL_TRQ_ERR_MSK_ADDR,
		EQDMA_GLBL_TRQ_ERR_STS_ADDR,
		GLBL_TRQ_ERR_STS_CSR_UNMAPPED_MASK,
		GLBL_ERR_STAT_ERR_TRQ_MASK,
		 
	},
	{
		EQDMA_TRQ_ERR_VF_ACCESS,
		"VF attempted to access Global register space or Function map",
		EQDMA_GLBL_TRQ_ERR_MSK_ADDR,
		EQDMA_GLBL_TRQ_ERR_STS_ADDR,
		GLBL_TRQ_ERR_STS_VF_ACCESS_ERR_MASK,
		GLBL_ERR_STAT_ERR_TRQ_MASK,
		 
	},
	{
		EQDMA_TRQ_ERR_TCP_CSR_TIMEOUT,
		"Timeout on request to dma internal csr register",
		EQDMA_GLBL_TRQ_ERR_MSK_ADDR,
		EQDMA_GLBL_TRQ_ERR_STS_ADDR,
		GLBL_TRQ_ERR_STS_TCP_CSR_TIMEOUT_MASK,
		GLBL_ERR_STAT_ERR_TRQ_MASK,
		 
	},
	{
		EQDMA_TRQ_ERR_QSPC_UNMAPPED,
		"Access targeted unmapped register via queue space pathway",
		EQDMA_GLBL_TRQ_ERR_MSK_ADDR,
		EQDMA_GLBL_TRQ_ERR_STS_ADDR,
		GLBL_TRQ_ERR_STS_QSPC_UNMAPPED_MASK,
		GLBL_ERR_STAT_ERR_TRQ_MASK,
		 
	},
	{
		EQDMA_TRQ_ERR_QID_RANGE,
		"Qid range error",
		EQDMA_GLBL_TRQ_ERR_MSK_ADDR,
		EQDMA_GLBL_TRQ_ERR_STS_ADDR,
		GLBL_TRQ_ERR_STS_QID_RANGE_MASK,
		GLBL_ERR_STAT_ERR_TRQ_MASK,
		 
	},
	{
		EQDMA_TRQ_ERR_TCP_QSPC_TIMEOUT,
		"Timeout on request to dma internal queue space register",
		EQDMA_GLBL_TRQ_ERR_MSK_ADDR,
		EQDMA_GLBL_TRQ_ERR_STS_ADDR,
		GLBL_TRQ_ERR_STS_TCP_QSPC_TIMEOUT_MASK,
		GLBL_ERR_STAT_ERR_TRQ_MASK,
		 
	},
	{
		EQDMA_TRQ_ERR_ALL,
		"All TRQ errors",
		EQDMA_GLBL_TRQ_ERR_MSK_ADDR,
		EQDMA_GLBL_TRQ_ERR_STS_ADDR,
		EQDMA_GLBL_TRQ_ERR_ALL_MASK,
		GLBL_ERR_STAT_ERR_TRQ_MASK,
		 
	},

	/* C2H Errors*/
	{
		EQDMA_ST_C2H_ERR_MTY_MISMATCH,
		"MTY mismatch error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_MTY_MISMATCH_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_LEN_MISMATCH,
		"Packet length mismatch error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_LEN_MISMATCH_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_SH_CMPT_DSC,
		"A Shared CMPT queue has encountered a descriptor error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_SH_CMPT_DSC_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_QID_MISMATCH,
		"Qid mismatch error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_QID_MISMATCH_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_DESC_RSP_ERR,
		"Descriptor error bit set",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_DESC_RSP_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_ENG_WPL_DATA_PAR_ERR,
		"Data parity error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_ENG_WPL_DATA_PAR_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_MSI_INT_FAIL,
		"MSI got a fail response error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_MSI_INT_FAIL_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_ERR_DESC_CNT,
		"Descriptor count error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_ERR_DESC_CNT_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_PORTID_CTXT_MISMATCH,
		"Port id in packet and pfetch ctxt mismatch error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_PORT_ID_CTXT_MISMATCH_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_CMPT_INV_Q_ERR,
		"Writeback on invalid queue error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_WRB_INV_Q_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_CMPT_QFULL_ERR,
		"Completion queue gets full error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_WRB_QFULL_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_CMPT_CIDX_ERR,
		"Bad CIDX update by the software error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_WRB_CIDX_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_CMPT_PRTY_ERR,
		"C2H completion Parity error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_WRB_PRTY_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_AVL_RING_DSC,
		"Available ring fetch returns descriptor with error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_AVL_RING_DSC_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_HDR_ECC_UNC,
		"multi-bit ecc error on c2h packet header",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_HDR_ECC_UNC_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_HDR_ECC_COR,
		"single-bit ecc error on c2h packet header",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_HDR_ECC_COR_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_WRB_PORT_ID_ERR,
		"Port ID error",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		C2H_ERR_STAT_WRB_PORT_ID_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_C2H_ERR_ALL,
		"All C2h errors",
		EQDMA_C2H_ERR_MASK_ADDR,
		EQDMA_C2H_ERR_STAT_ADDR,
		EQDMA_C2H_ERR_ALL_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},

	/* C2H fatal errors */
	{
		EQDMA_ST_FATAL_ERR_MTY_MISMATCH,
		"Fatal MTY mismatch error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_MTY_MISMATCH_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_LEN_MISMATCH,
		"Fatal Len mismatch error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_LEN_MISMATCH_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_QID_MISMATCH,
		"Fatal Qid mismatch error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_QID_MISMATCH_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_TIMER_FIFO_RAM_RDBE,
		"RAM double bit fatal error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_TIMER_FIFO_RAM_RDBE_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_PFCH_II_RAM_RDBE,
		"RAM double bit fatal error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_PFCH_LL_RAM_RDBE_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_CMPT_CTXT_RAM_RDBE,
		"RAM double bit fatal error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_WRB_CTXT_RAM_RDBE_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_PFCH_CTXT_RAM_RDBE,
		"RAM double bit fatal error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_PFCH_CTXT_RAM_RDBE_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_DESC_REQ_FIFO_RAM_RDBE,
		"RAM double bit fatal error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_DESC_REQ_FIFO_RAM_RDBE_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_INT_CTXT_RAM_RDBE,
		"RAM double bit fatal error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_INT_CTXT_RAM_RDBE_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_CMPT_COAL_DATA_RAM_RDBE,
		"RAM double bit fatal error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_WRB_COAL_DATA_RAM_RDBE_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_CMPT_FIFO_RAM_RDBE,
		"RAM double bit fatal error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_CMPT_FIFO_RAM_RDBE_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_QID_FIFO_RAM_RDBE,
		"RAM double bit fatal error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_QID_FIFO_RAM_RDBE_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_PAYLOAD_FIFO_RAM_RDBE,
		"RAM double bit fatal error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_PLD_FIFO_RAM_RDBE_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_WPL_DATA_PAR,
		"RAM double bit fatal error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_WPL_DATA_PAR_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_AVL_RING_FIFO_RAM_RDBE,
		"RAM double bit fatal error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_AVL_RING_FIFO_RAM_RDBE_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_HDR_ECC_UNC,
		"RAM double bit fatal error",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		C2H_FATAL_ERR_STAT_HDR_ECC_UNC_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},
	{
		EQDMA_ST_FATAL_ERR_ALL,
		"All fatal errors",
		EQDMA_C2H_FATAL_ERR_MASK_ADDR,
		EQDMA_C2H_FATAL_ERR_STAT_ADDR,
		EQDMA_C2H_FATAL_ERR_ALL_MASK,
		GLBL_ERR_STAT_ERR_C2H_ST_MASK,
		 
	},

	/* H2C St errors */
	{
		EQDMA_ST_H2C_ERR_ZERO_LEN_DESC,
		"Zero length descriptor error",
		EQDMA_H2C_ERR_MASK_ADDR,
		EQDMA_H2C_ERR_STAT_ADDR,
		H2C_ERR_STAT_ZERO_LEN_DS_MASK,
		GLBL_ERR_STAT_ERR_H2C_ST_MASK,
		 
	},
	{
		EQDMA_ST_H2C_ERR_SDI_MRKR_REQ_MOP,
		"A non-EOP descriptor received",
		EQDMA_H2C_ERR_MASK_ADDR,
		EQDMA_H2C_ERR_STAT_ADDR,
		H2C_ERR_STAT_SDI_MRKR_REQ_MOP_ERR_MASK,
		GLBL_ERR_STAT_ERR_H2C_ST_MASK,
		 
	},
	{
		EQDMA_ST_H2C_ERR_NO_DMA_DSC,
		"No DMA descriptor received error",
		EQDMA_H2C_ERR_MASK_ADDR,
		EQDMA_H2C_ERR_STAT_ADDR,
		H2C_ERR_STAT_NO_DMA_DS_MASK,
		GLBL_ERR_STAT_ERR_H2C_ST_MASK,
		 
	},
	{
		EQDMA_ST_H2C_ERR_SBE,
		"Single bit error detected on H2C-ST data error",
		EQDMA_H2C_ERR_MASK_ADDR,
		EQDMA_H2C_ERR_STAT_ADDR,
		H2C_ERR_STAT_SBE_MASK,
		GLBL_ERR_STAT_ERR_H2C_ST_MASK,
		 
	},
	{
		EQDMA_ST_H2C_ERR_DBE,
		"Double bit error detected on H2C-ST data error",
		EQDMA_H2C_ERR_MASK_ADDR,
		EQDMA_H2C_ERR_STAT_ADDR,
		H2C_ERR_STAT_DBE_MASK,
		GLBL_ERR_STAT_ERR_H2C_ST_MASK,
		 
	},
	{
		EQDMA_ST_H2C_ERR_PAR,
		"Internal data parity error",
		EQDMA_H2C_ERR_MASK_ADDR,
		EQDMA_H2C_ERR_STAT_ADDR,
		H2C_ERR_STAT_PAR_ERR_MASK,
		GLBL_ERR_STAT_ERR_H2C_ST_MASK,
		 
	},
	{
		EQDMA_ST_H2C_ERR_ALL,
		"All H2C errors",
		EQDMA_H2C_ERR_MASK_ADDR,
		EQDMA_H2C_ERR_STAT_ADDR,
		EQDMA_H2C_ERR_ALL_MASK,
		GLBL_ERR_STAT_ERR_H2C_ST_MASK,
		 
	},

	/* SBE errors */
	{
		EQDMA_SBE_1_ERR_RC_RRQ_EVEN_RAM,
		"RC RRQ Even RAM single bit ECC error.",
		EQDMA_RAM_SBE_MSK_1_A_ADDR,
		EQDMA_RAM_SBE_STS_1_A_ADDR,
		RAM_SBE_STS_1_A_RC_RRQ_EVEN_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_1_ERR_TAG_ODD_RAM,
		"Tag Odd Ram single bit ECC error.",
		EQDMA_RAM_SBE_MSK_1_A_ADDR,
		EQDMA_RAM_SBE_STS_1_A_ADDR,
		RAM_SBE_STS_1_A_TAG_ODD_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_1_ERR_TAG_EVEN_RAM,
		"Tag Even Ram single bit ECC error.",
		EQDMA_RAM_SBE_MSK_1_A_ADDR,
		EQDMA_RAM_SBE_STS_1_A_ADDR,
		RAM_SBE_STS_1_A_TAG_EVEN_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_1_ERR_PFCH_CTXT_CAM_RAM_0,
		"Pfch Ctxt CAM RAM 0 single bit ECC error.",
		EQDMA_RAM_SBE_MSK_1_A_ADDR,
		EQDMA_RAM_SBE_STS_1_A_ADDR,
		RAM_SBE_STS_1_A_PFCH_CTXT_CAM_RAM_0_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_1_ERR_PFCH_CTXT_CAM_RAM_1,
		"Pfch Ctxt CAM RAM 1 single bit ECC error.",
		EQDMA_RAM_SBE_MSK_1_A_ADDR,
		EQDMA_RAM_SBE_STS_1_A_ADDR,
		RAM_SBE_STS_1_A_PFCH_CTXT_CAM_RAM_1_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_1_ERR_ALL,
		"All SBE Errors.",
		EQDMA_RAM_SBE_MSK_1_A_ADDR,
		EQDMA_RAM_SBE_STS_1_A_ADDR,
		EQDMA_SBE_ERR_ALL_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_MI_H2C0_DAT,
		"H2C MM data buffer single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_MI_H2C0_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_MI_H2C1_DAT,
		"H2C MM data buffer single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_MI_H2C1_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_MI_H2C2_DAT,
		"H2C MM data buffer single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_MI_H2C2_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_MI_H2C3_DAT,
		"H2C MM data buffer single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_MI_H2C3_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_MI_C2H0_DAT,
		"C2H MM data buffer single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_MI_C2H0_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_MI_C2H1_DAT,
		"C2H MM data buffer single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_MI_C2H1_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
{
		EQDMA_SBE_ERR_MI_C2H2_DAT,
		"C2H MM data buffer single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_MI_C2H2_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_MI_C2H3_DAT,
		"C2H MM data buffer single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_MI_C2H3_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_H2C_RD_BRG_DAT,
		"Bridge master read single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_H2C_RD_BRG_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_H2C_WR_BRG_DAT,
		"Bridge master write single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_H2C_WR_BRG_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_C2H_RD_BRG_DAT,
		"Bridge slave read data buffer single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_C2H_RD_BRG_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_C2H_WR_BRG_DAT,
		"Bridge slave write data buffer single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_C2H_WR_BRG_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_FUNC_MAP,
		"Function map RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_FUNC_MAP_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_DSC_HW_CTXT,
		"Descriptor engine hardware context RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_DSC_HW_CTXT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_DSC_CRD_RCV,
		"Descriptor engine receive credit context RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_DSC_CRD_RCV_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_DSC_SW_CTXT,
		"Descriptor engine software context RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_DSC_SW_CTXT_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_DSC_CPLI,
		"Descriptor engine fetch completion information RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_DSC_CPLI_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_DSC_CPLD,
		"Descriptor engine fetch completion data RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_DSC_CPLD_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_MI_TL_SLV_FIFO_RAM,
		"TL Slavle FIFO RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_MI_TL_SLV_FIFO_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_TIMER_FIFO_RAM,
		"Timer fifo RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_TIMER_FIFO_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_QID_FIFO_RAM,
		"C2H ST QID FIFO RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_QID_FIFO_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_WRB_COAL_DATA_RAM,
		"Writeback Coalescing RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_WRB_COAL_DATA_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_INT_CTXT_RAM,
		"Interrupt context RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_INT_CTXT_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_DESC_REQ_FIFO_RAM,
		"C2H ST descriptor request RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_DESC_REQ_FIFO_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_PFCH_CTXT_RAM,
		"C2H ST prefetch RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_PFCH_CTXT_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_WRB_CTXT_RAM,
		"C2H ST completion context RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_WRB_CTXT_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_PFCH_LL_RAM,
		"C2H ST prefetch list RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_PFCH_LL_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_PEND_FIFO_RAM,
		"Pend FIFO RAM single bit ECC error",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_PEND_FIFO_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_RC_RRQ_ODD_RAM,
		"RC RRQ Odd RAM single bit ECC error.",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		RAM_SBE_STS_A_RC_RRQ_ODD_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},
	{
		EQDMA_SBE_ERR_ALL,
		"All SBE errors",
		EQDMA_RAM_SBE_MSK_A_ADDR,
		EQDMA_RAM_SBE_STS_A_ADDR,
		EQDMA_SBE_ERR_ALL_MASK,
		GLBL_ERR_STAT_ERR_RAM_SBE_MASK,
		 
	},


	/* DBE errors */
	{
		EQDMA_DBE_1_ERR_RC_RRQ_EVEN_RAM,
		"RC RRQ Odd RAM double bit ECC error.",
		EQDMA_RAM_DBE_MSK_1_A_ADDR,
		EQDMA_RAM_DBE_STS_1_A_ADDR,
		RAM_DBE_STS_1_A_RC_RRQ_EVEN_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_1_ERR_TAG_ODD_RAM,
		"Tag Odd Ram double bit ECC error.",
		EQDMA_RAM_DBE_MSK_1_A_ADDR,
		EQDMA_RAM_DBE_STS_1_A_ADDR,
		RAM_DBE_STS_1_A_TAG_ODD_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_1_ERR_TAG_EVEN_RAM,
		"Tag Even Ram double bit ECC error.",
		EQDMA_RAM_DBE_MSK_1_A_ADDR,
		EQDMA_RAM_DBE_STS_1_A_ADDR,
		RAM_DBE_STS_1_A_TAG_EVEN_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_1_ERR_PFCH_CTXT_CAM_RAM_0,
		"Pfch Ctxt CAM RAM 0 double bit ECC error.",
		EQDMA_RAM_DBE_MSK_1_A_ADDR,
		EQDMA_RAM_DBE_STS_1_A_ADDR,
		RAM_DBE_STS_1_A_PFCH_CTXT_CAM_RAM_0_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_1_ERR_PFCH_CTXT_CAM_RAM_1,
		"Pfch Ctxt CAM RAM double bit ECC error.",
		EQDMA_RAM_DBE_MSK_1_A_ADDR,
		EQDMA_RAM_DBE_STS_1_A_ADDR,
		RAM_DBE_STS_1_A_PFCH_CTXT_CAM_RAM_0_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_1_ERR_ALL,
		"All DBE errors",
		EQDMA_RAM_DBE_MSK_1_A_ADDR,
		EQDMA_RAM_DBE_STS_1_A_ADDR,
		EQDMA_DBE_ERR_ALL_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_MI_H2C0_DAT,
		"H2C MM data buffer double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_MI_H2C0_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_MI_H2C1_DAT,
		"H2C MM data buffer double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_MI_H2C1_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_MI_H2C2_DAT,
		"H2C MM data buffer double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_MI_H2C2_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_MI_H2C3_DAT,
		"H2C MM data buffer double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_MI_H2C3_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_MI_C2H0_DAT,
		"C2H MM data buffer double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_MI_C2H0_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_MI_C2H1_DAT,
		"C2H MM data buffer double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_MI_C2H1_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_MI_C2H2_DAT,
		"C2H MM data buffer double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_MI_C2H2_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_MI_C2H3_DAT,
		"C2H MM data buffer double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_MI_C2H3_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_H2C_RD_BRG_DAT,
		"Bridge master read double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_H2C_RD_BRG_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_H2C_WR_BRG_DAT,
		"Bridge master write double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_H2C_WR_BRG_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_C2H_RD_BRG_DAT,
		"Bridge slave read data buffer double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_C2H_RD_BRG_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_C2H_WR_BRG_DAT,
		"Bridge slave write data buffer double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_C2H_WR_BRG_DAT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_FUNC_MAP,
		"Function map RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_FUNC_MAP_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_DSC_HW_CTXT,
		"Descriptor engine hardware context RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_DSC_HW_CTXT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_DSC_CRD_RCV,
		"Descriptor engine receive credit context RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_DSC_CRD_RCV_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_DSC_SW_CTXT,
		"Descriptor engine software context RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_DSC_SW_CTXT_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_DSC_CPLI,
		"Descriptor engine fetch completion information RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_DSC_CPLI_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_DSC_CPLD,
		"Descriptor engine fetch completion data RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_DSC_CPLD_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_MI_TL_SLV_FIFO_RAM,
		"TL Slave FIFO RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_MI_TL_SLV_FIFO_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_TIMER_FIFO_RAM,
		"Timer fifo RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_TIMER_FIFO_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_QID_FIFO_RAM,
		"C2H ST QID FIFO RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_QID_FIFO_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_WRB_COAL_DATA_RAM,
		"Writeback Coalescing RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_WRB_COAL_DATA_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_INT_CTXT_RAM,
		"Interrupt context RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_INT_CTXT_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_DESC_REQ_FIFO_RAM,
		"C2H ST descriptor request RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_DESC_REQ_FIFO_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_PFCH_CTXT_RAM,
		"C2H ST prefetch RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_PFCH_CTXT_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_WRB_CTXT_RAM,
		"C2H ST completion context RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_WRB_CTXT_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_PFCH_LL_RAM,
		"C2H ST prefetch list RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_PFCH_LL_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_PEND_FIFO_RAM,
		"Pend FIFO RAM double bit ECC error",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_PEND_FIFO_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_RC_RRQ_ODD_RAM,
		"RC RRQ Odd RAM double bit ECC error.",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		RAM_DBE_STS_A_RC_RRQ_ODD_RAM_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	{
		EQDMA_DBE_ERR_ALL,
		"All DBE errors",
		EQDMA_RAM_DBE_MSK_A_ADDR,
		EQDMA_RAM_DBE_STS_A_ADDR,
		EQDMA_DBE_ERR_ALL_MASK,
		GLBL_ERR_STAT_ERR_RAM_DBE_MASK,
		 
	},
	/* MM C2H Engine 0 errors */
	{
		EQDMA_MM_C2H_WR_SLR_ERR,
		"MM C2H0 WR SLV Error",
		EQDMA_C2H_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_C2H_MM_STATUS_ADDR,
		C2H_MM_ERR_CODE_ENABLE_WR_SLV_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_C2H_RD_SLR_ERR,
		"MM C2H0 RD SLV Error",
		EQDMA_C2H_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_C2H_MM_STATUS_ADDR,
		C2H_MM_ERR_CODE_ENABLE_RD_SLV_ERR_MASK,
		GLBL_ERR_STAT_ERR_C2H_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_C2H_WR_FLR_ERR,
		"MM C2H0 WR FLR Error",
		EQDMA_C2H_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_C2H_MM_STATUS_ADDR,
		C2H_MM_ERR_CODE_ENABLE_WR_FLR_MASK,
		GLBL_ERR_STAT_ERR_C2H_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_C2H_UR_ERR,
		"MM C2H0 Unsupported Request Error",
		EQDMA_C2H_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_C2H_MM_STATUS_ADDR,
		C2H_MM_ERR_CODE_ENABLE_WR_UR_MASK,
		GLBL_ERR_STAT_ERR_C2H_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_C2H_WR_UC_RAM_ERR,
		"MM C2H0 Write Uncorrectable RAM Error",
		EQDMA_C2H_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_C2H_MM_STATUS_ADDR,
		C2H_MM_ERR_CODE_ENABLE_WR_UC_RAM_MASK,
		GLBL_ERR_STAT_ERR_C2H_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_C2H_ERR_ALL,
		"All MM C2H Errors",
		EQDMA_C2H_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_C2H_MM_STATUS_ADDR,
		EQDMA_MM_C2H_ERR_ALL_MASK,
		GLBL_ERR_STAT_ERR_C2H_MM_0_MASK,
		 
	},
	/* MM H2C Engine 0 Errors */
	{
		EQDMA_MM_H2C0_RD_HDR_POISON_ERR,
		"MM H2C0 Read cmpt header pison Error",
		EQDMA_H2C_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_H2C_MM_STATUS_ADDR,
		H2C_MM_ERR_CODE_ENABLE_RD_HRD_POISON_ERR_MASK,
		GLBL_ERR_STAT_ERR_H2C_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_H2C0_RD_UR_CA_ERR,
		"MM H2C0 Read cmpt unsupported request Error",
		EQDMA_H2C_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_H2C_MM_STATUS_ADDR,
		H2C_MM_ERR_CODE_ENABLE_RD_UR_CA_MASK,
		GLBL_ERR_STAT_ERR_H2C_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_H2C0_RD_HDR_BYTE_ERR,
		"MM H2C0 Read cmpt hdr byte cnt Error",
		EQDMA_H2C_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_H2C_MM_STATUS_ADDR,
		H2C_MM_ERR_CODE_ENABLE_RD_HDR_BYTE_ERR_MASK,
		GLBL_ERR_STAT_ERR_H2C_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_H2C0_RD_HDR_PARAM_ERR,
		"MM H2C0 Read cmpt hdr param mismatch Error",
		EQDMA_H2C_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_H2C_MM_STATUS_ADDR,
		H2C_MM_ERR_CODE_ENABLE_RD_HDR_PARA_MASK,
		GLBL_ERR_STAT_ERR_H2C_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_H2C0_RD_HDR_ADR_ERR,
		"MM H2C0 Read cmpt hdr address mismatch Error",
		EQDMA_H2C_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_H2C_MM_STATUS_ADDR,
		H2C_MM_ERR_CODE_ENABLE_RD_HDR_ADR_ERR_MASK,
		GLBL_ERR_STAT_ERR_H2C_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_H2C0_RD_FLR_ERR,
		"MM H2C0 Read flr Error",
		EQDMA_H2C_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_H2C_MM_STATUS_ADDR,
		H2C_MM_ERR_CODE_ENABLE_RD_FLR_ERR_MASK,
		GLBL_ERR_STAT_ERR_H2C_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_H2C0_RD_DAT_POISON_ERR,
		"MM H2C0 Read data poison Error",
		EQDMA_H2C_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_H2C_MM_STATUS_ADDR,
		H2C_MM_ERR_CODE_ENABLE_RD_DAT_POISON_ERR_MASK,
		GLBL_ERR_STAT_ERR_H2C_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_H2C0_RD_RQ_DIS_ERR,
		"MM H2C0 Read request disable Error",
		EQDMA_H2C_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_H2C_MM_STATUS_ADDR,
		H2C_MM_ERR_CODE_ENABLE_RD_RQ_DIS_ERR_MASK,
		GLBL_ERR_STAT_ERR_H2C_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_H2C0_WR_DEC_ERR,
		"MM H2C0 Write desc Error",
		EQDMA_H2C_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_H2C_MM_STATUS_ADDR,
		H2C_MM_ERR_CODE_ENABLE_WR_DEC_ERR_MASK,
		GLBL_ERR_STAT_ERR_H2C_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_H2C0_WR_SLV_ERR,
		"MM H2C0 Write slv Error",
		EQDMA_H2C_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_H2C_MM_STATUS_ADDR,
		H2C_MM_ERR_CODE_ENABLE_WR_SLV_ERR_MASK,
		GLBL_ERR_STAT_ERR_H2C_MM_0_MASK,
		 
	},
	{
		EQDMA_MM_H2C0_ERR_ALL,
		"All MM H2C Errors",
		EQDMA_H2C_MM_ERR_CODE_ENABLE_MASK_ADDR,
		EQDMA_H2C_MM_STATUS_ADDR,
		EQDMA_MM_H2C0_ERR_ALL_MASK,
		GLBL_ERR_STAT_ERR_H2C_MM_0_MASK,
		 
	},

};

static int32_t all_eqdma_hw_errs[EQDMA_TOTAL_LEAF_ERROR_AGGREGATORS] = {

	EQDMA_DSC_ERR_ALL,
	EQDMA_TRQ_ERR_ALL,
	EQDMA_ST_C2H_ERR_ALL,
	EQDMA_ST_FATAL_ERR_ALL,
	EQDMA_ST_H2C_ERR_ALL,
	EQDMA_SBE_1_ERR_ALL,
	EQDMA_SBE_ERR_ALL,
	EQDMA_DBE_1_ERR_ALL,
	EQDMA_DBE_ERR_ALL,
	EQDMA_MM_C2H_ERR_ALL,
	EQDMA_MM_H2C0_ERR_ALL
};

/*****************************************************************************/
/**
 * qdma_hw_error_enable() - Function to enable all or a specific error
 *
 * @dev_hndl: device handle
 * @err_idx: error index
 *
 * Return:	0   - success and < 0 - failure
 *****************************************************************************/
int eqdma_hw_error_enable(uint8_t *base, uint32_t err_idx)
{
	uint32_t idx = 0, i = 0;
	uint32_t reg_val = 0;
	struct qdma_dev_attributes dev_cap;

	if (!base) {
		return -QDMA_ERR_INV_PARAM;
	}

	if (err_idx > EQDMA_ERRS_ALL) {
		return -QDMA_ERR_INV_PARAM;
	}

	eqdma_get_device_attributes(base, &dev_cap);

	if (err_idx == EQDMA_ERRS_ALL) {
		for (i = 0; i < EQDMA_TOTAL_LEAF_ERROR_AGGREGATORS; i++) {

			idx = all_eqdma_hw_errs[i];

			/* Don't access streaming registers in
			 * MM only bitstreams
			 */
			if (!dev_cap.st_en) {
				if (idx == EQDMA_ST_C2H_ERR_ALL ||
					idx == EQDMA_ST_FATAL_ERR_ALL ||
					idx == EQDMA_ST_H2C_ERR_ALL)
					continue;
			}

			reg_val = eqdma_err_info[idx].leaf_err_mask;
			write_reg(base,
				eqdma_err_info[idx].mask_reg_addr, reg_val);

			reg_val = read_reg(base,
					EQDMA_GLBL_ERR_MASK_ADDR);
			reg_val |= FIELD_SET(
				eqdma_err_info[idx].global_err_mask, 1);
			write_reg(base, EQDMA_GLBL_ERR_MASK_ADDR,
					reg_val);
		}

	} else {
		/* Don't access streaming registers in MM only bitstreams
		 *  QDMA_C2H_ERR_MTY_MISMATCH to QDMA_H2C_ERR_ALL are all
		 *  ST errors
		 */
		if (!dev_cap.st_en) {
			if (err_idx >= EQDMA_ST_C2H_ERR_MTY_MISMATCH &&
					err_idx <= EQDMA_ST_H2C_ERR_ALL)
				return QDMA_SUCCESS;
		}

		reg_val = read_reg(base,
				eqdma_err_info[err_idx].mask_reg_addr);
		reg_val |= FIELD_SET(eqdma_err_info[err_idx].leaf_err_mask, 1);
		write_reg(base,
				eqdma_err_info[err_idx].mask_reg_addr, reg_val);

		reg_val = read_reg(base, EQDMA_GLBL_ERR_MASK_ADDR);
		reg_val |=
			FIELD_SET(eqdma_err_info[err_idx].global_err_mask, 1);
		write_reg(base, EQDMA_GLBL_ERR_MASK_ADDR, reg_val);
	}

	return QDMA_SUCCESS;
}

#pragma endregion


int main() {
	int container, group, device, i;
	struct vfio_group_status group_status = { .argsz = sizeof(group_status) };
	struct vfio_iommu_type1_info iommu_info = { .argsz = sizeof(iommu_info) };
	struct vfio_device_info device_info = { .argsz = sizeof(device_info) };
	struct vfio_iommu_type1_dma_map dma_map = { .argsz = sizeof(dma_map) };

	container = open("/dev/vfio/vfio", O_RDWR);
	if (container < 0) {
		perror("Error opening /dev/vfio/vfio");
		return -1;
	}

	if (ioctl(container, VFIO_GET_API_VERSION) != VFIO_API_VERSION) {
		perror("Unknown VFIO API version");
		return -1;
	}

	if (!ioctl(container, VFIO_CHECK_EXTENSION, VFIO_TYPE1_IOMMU)) {
		perror("VFIO_TYPE1_IOMMU not supported!");
		return -1;
	}

	// TODO: figure this out from the PCIe BDF, also take it as an argument
	group = open("/dev/vfio/47", O_RDWR);
	ioctl(group, VFIO_GROUP_GET_STATUS, &group_status);
	if (!(group_status.flags & VFIO_GROUP_FLAGS_VIABLE)) {
		perror("VFIO group is not viable!");
		return -1;
	}

	ioctl(group, VFIO_GROUP_SET_CONTAINER, &container);
	ioctl(container, VFIO_SET_IOMMU, VFIO_TYPE1_IOMMU);
	ioctl(container, VFIO_IOMMU_GET_INFO, &iommu_info);

	printf("iommu_info.flags: %d\n", iommu_info.flags);


	// dma mapping for descriptor rings
	const int RING_SIZE = 4096;
	const int H2C_DESCR_SIZE = 16;
	const int C2H_DESCR_SIZE = 8;
	const int CMPLT_DESCR_SIZE = 64;
	const int TOTAL_SIZE = 4096 * 65 * 4;
	void* dma_vaddr = mmap(0, TOTAL_SIZE, PROT_READ | PROT_WRITE,
			     MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
	dma_map.vaddr = (uintptr_t)dma_vaddr;
	dma_map.size = TOTAL_SIZE;
	dma_map.iova = (uintptr_t)dma_vaddr; // iova == vaddr so i can use pointers directly (i believe)
	dma_map.flags = VFIO_DMA_MAP_FLAG_READ | VFIO_DMA_MAP_FLAG_WRITE;

	printf("mapping vaddr %llx with size %llu to iova %llx\n", dma_map.vaddr, dma_map.size, dma_map.iova);

	int ret = ioctl(container, VFIO_IOMMU_MAP_DMA, &dma_map);
	if (ret < 0) {
		perror("error in mapping DMA for rings");
		return -1;
	}

	uint8_t* h2c_ring_base = dma_vaddr;
	uint8_t* c2h_ring_base = h2c_ring_base + (4096 * 65);
	uint8_t* cmplt_ring_base = c2h_ring_base + (4096 * 65);
	uint8_t* intr_ring_base = cmplt_ring_base + (4096 * 65);

	device = ioctl(group, VFIO_GROUP_GET_DEVICE_FD, "0000:41:00.0");
	ioctl(device, VFIO_DEVICE_GET_INFO, &device_info);

	printf("device_info.num_regions: %d\n", device_info.num_regions);

	uint8_t* register_space;
	size_t config_space_offset;
	for (i = 0; i < device_info.num_regions; i++) {
		struct vfio_region_info reg = { .argsz = sizeof(reg) };
		reg.index = i;
		ioctl(device, VFIO_DEVICE_GET_REGION_INFO, &reg);

		printf("Region %d is at offset %#llx and has size %#llx with flags %x\n", reg.index, reg.offset, reg.size, reg.flags);

		if (reg.index == 0) {
			printf("mmap args: %lld, %d, %llx", reg.size, device, reg.offset);
			register_space = mmap(NULL, reg.size, PROT_READ | PROT_WRITE, MAP_SHARED, device, reg.offset);
		}
		if (reg.index == 7) {
			config_space_offset = reg.offset;
		}
	}

	// read device and vendor ID from config space
	uint32_t id_shadow;
	pread(device, &id_shadow, sizeof(uint32_t), config_space_offset);
	printf("read 0x%08x from config space at offset 0x%lx\n", id_shadow, config_space_offset);

	// enable bus mastering
	uint32_t command_reg_shadow;
	pread(device, &command_reg_shadow, sizeof(uint32_t), config_space_offset + 0x04);
	if (command_reg_shadow & 0x4) {
		printf("Bus mastering was already enabled!\n");
	}
	if (command_reg_shadow & 0x2) {
		printf("Memory space access was already enabled!\n");
	}
	command_reg_shadow |= 0x6;
	pwrite(device, &command_reg_shadow, sizeof(uint32_t), config_space_offset + 0x04);

	// just some stuff for confirmation
	read_reg(register_space, 0x0);
	read_reg(register_space, 0x134);
	read_reg(register_space, 0x104);
	read_reg(register_space, 0x120);
	read_reg(register_space, 0x118);
	read_reg(register_space, 0x12c);

	// clear most contexts for queues 0 through 512 (dunno why these contexts or only those queues)
	for (uint32_t qid = 0; qid < 0x200; qid++) {
		for (uint32_t sel = 0; sel < 8; sel++) {
			uint32_t val = (qid << 7) | (sel << 1);
			write_reg(register_space, 0x844, val);
		}
	}

	// clear FMAP contexts
	eqdma_indirect_reg_clear(register_space, QDMA_CTXT_SEL_FMAP, 0);
	eqdma_indirect_reg_clear(register_space, QDMA_CTXT_SEL_FMAP, 1);

	// set defaults (matches kernel)
	eqdma_set_default_global_csr(register_space);

	// two spurious error reporting thingies (for MM mode I believe)
	write_reg(register_space, 0x1004, 1);
	write_reg(register_space, 0x1204, 1);

	// set up error reporting
	eqdma_hw_error_enable(register_space, EQDMA_ERRS_ALL);

	// set defaults again
	eqdma_set_default_global_csr(register_space);

	// do this again
	write_reg(register_space, 0x1004, 1);
	write_reg(register_space, 0x1204, 1);
	
	// turn off interrupt coalescing in queue 1?
	eqdma_indirect_reg_clear(register_space, QDMA_CTXT_SEL_INT_COAL, 1);

	struct qdma_indirect_intr_ctxt sw_intr_ctxt = {
		.baddr_4k = 0,
		.vec = 2,
		.pidx = 0,
		.valid = 1,
		.int_st = 0,
		.color = 1,
		.page_size = 0,
		.at = 0,
		.host_id = 0,
		.pasid = 0,
		.pasid_en = 0,
		.func_id = 1,
	};
	eqdma_indirect_intr_context_write(register_space, 1, &sw_intr_ctxt);

	// finish modprobe

	// clear most contexts for queues 0 through 512 (dunno why these contexts or only those queues)
	for (uint32_t qid = 0; qid < 0x200; qid++) {
		for (uint32_t sel = 0; sel < 8; sel++) {
			uint32_t val = (qid << 7) | (sel << 1);
			write_reg(register_space, 0x844, val);
		}
	}

	// clear FMAP contexts
	eqdma_indirect_reg_clear(register_space, QDMA_CTXT_SEL_FMAP, 0);
	eqdma_indirect_reg_clear(register_space, QDMA_CTXT_SEL_FMAP, 1);

	// set defaults (matches kernel)
	eqdma_set_default_global_csr(register_space);

	// do this again
	write_reg(register_space, 0x1004, 1);
	write_reg(register_space, 0x1204, 1);

	// finish qmax

	// set up FMAP context with QMAX value (32)
	struct qdma_fmap_cfg fmap_ctxt = {
		.qbase = 0,
		.qmax = 32
	};

	eqdma_fmap_context_write(register_space, &fmap_ctxt, 0);

	// finish queue add

	// first thing is allocating 520 and then 1032 bytes for descriptor rings. does this make sense?
	// yeah, kind of. except it includes the 8 bytes of the completion status descriptor weirdly.

	// clear c2h contexts
	eqdma_indirect_reg_clear(register_space, QDMA_CTXT_SEL_SW_C2H, 0);
	eqdma_indirect_reg_clear(register_space, QDMA_CTXT_SEL_HW_C2H, 0);
	eqdma_indirect_reg_clear(register_space, QDMA_CTXT_SEL_CR_C2H, 0);
	eqdma_indirect_reg_clear(register_space, QDMA_CTXT_SEL_PFTCH, 0);
	eqdma_indirect_reg_clear(register_space, QDMA_CTXT_SEL_CMPT, 0);


	// write c2h sw context
	// TODO: magic numbers, esp for dsc_sz
	struct qdma_descq_sw_ctxt sw_ctxt_c2h = {
		.pidx = 0,
		.irq_arm = 0,
		.fnc_id = 0,
		.qen = 1,
		.frcd_en = 1,
		.wbi_chk = 0, // only because that's what the driver does
		.wbi_intvl_en = 1, // ignored in stream mode
		.at = 0,
		.fetch_max = 7,
		.rngsz_idx = 1, // empirically, the minimum legal size is 0x601
		.desc_sz = 0, // 8 bytes for C2H Stream DMA
		.bypass = 0,
		.mm_chn = 0,
		.wbk_en = 0, // what the driver does
		.irq_en = 0, // required for C2H ST
		.port_id = 0,
		.irq_no_last = 0, // N/A for C2H ST
		.err = 0,
		.err_wb_sent = 0,
		.irq_req = 0,
		.mrkr_dis = 0,
		.is_mm = 0,
		.ring_bs_addr = (uintptr_t)c2h_ring_base,
		.vec = 0, // what the driver does, think it won't be used
		.intr_aggr = 0, // what the driver does, think it won't be used
	};
	eqdma_sw_context_write(register_space, 1, 0, &sw_ctxt_c2h);

	// write prefetch context
	struct qdma_descq_prefetch_ctxt prefetch_ctxt = {
		.bypass = 0,
		.bufsz_idx = 0,
		.port_id = 0,
		.err = 0,
		.pfch_en = 0, // what the driver does, we'll eventually turn this on
		.pfch = 0,
		.sw_crdt = 0,
		.valid = 1,
	};
	eqdma_pfetch_context_write(register_space, 0, &prefetch_ctxt);

	// write completion context
	struct qdma_descq_cmpt_ctxt cmplt_ctxt = {
		.en_stat_desc = 1,
		.en_int = 0, // turning off interrupts - we'll see if it's ok
		.trig_mode = 1,
		.fnc_id = 0,
		.counter_idx = 0,
		.timer_idx = 0,
		.in_st = 0,
		.color = 1,
		.ringsz_idx = 2, // TODO set this for real
		.bs_addr = (uintptr_t)cmplt_ring_base,
		.desc_sz = 0, // 8 bytes of user data - what are we doing with this currently?
		.pidx = 0,
		.cidx = 0,
		.valid = 1,
		.err = 0,
		.user_trig_pend = 0,
		.timer_running = 0,
		.full_upd = 0,
		.ovf_chk_dis = 0,
		.at = 0,
		.vec = 0,
		.int_aggr = 0,
		.dis_intr_on_vf = 0,
		.dir_c2h = 0, // wut.
	};
	eqdma_cmpt_context_write(register_space, 0, &cmplt_ctxt);

	// enable status descriptor, set trigger mode to Every, and disable IRQs
	write_reg(register_space, 0x1800c, 0x09000000);
	// initialize PIDX to ring size - 1
	write_reg(register_space, 0x18008, 0x3f);

	// reset h2c contexts
	eqdma_indirect_reg_clear(register_space, QDMA_CTXT_SEL_SW_H2C, 0);
	eqdma_indirect_reg_clear(register_space, QDMA_CTXT_SEL_HW_H2C, 0);
	eqdma_indirect_reg_clear(register_space, QDMA_CTXT_SEL_CR_H2C, 0);

	// write h2c sw context
		struct qdma_descq_sw_ctxt sw_ctxt_h2c = {
		.pidx = 0,
		.irq_arm = 0,
		.fnc_id = 0,
		.qen = 1,
		.frcd_en = 0, // what the driver does
		.wbi_chk = 1, // only because that's what the driver does
		.wbi_intvl_en = 1, // ignored in stream mode
		.at = 0,
		.fetch_max = 7,
		.rngsz_idx = 1, // no reason, fix this when we set up actual rings
		.desc_sz = 1, // 16 bytes for H2C Stream DMA
		.bypass = 0,
		.mm_chn = 0,
		.wbk_en = 1, // what the driver does
		.irq_en = 0,
		.port_id = 0,
		.irq_no_last = 0,
		.err = 0,
		.err_wb_sent = 0,
		.irq_req = 0,
		.mrkr_dis = 1,
		.is_mm = 0,
		.ring_bs_addr = (uintptr_t)h2c_ring_base,
		.vec = 0, // what the driver does, think it won't be used
		.intr_aggr = 0, // what the driver does, think it won't be used
	};
	eqdma_sw_context_write(register_space, 0, 0, &sw_ctxt_h2c);

	// finish queue start

	printf("Attempting to write to the card now!\n");

	struct vfio_iommu_type1_dma_map data_map = { .argsz = sizeof(dma_map) };
	void* data_buffer = mmap(0, 8192, PROT_READ | PROT_WRITE,
			     MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
	data_map.vaddr = (uintptr_t)data_buffer;
	data_map.size = 8192;
	data_map.iova = (uintptr_t)data_buffer; // iova == vaddr so i can use pointers directly (i believe)
	data_map.flags = VFIO_DMA_MAP_FLAG_READ | VFIO_DMA_MAP_FLAG_WRITE;

	printf("mapping vaddr %llx with size %llu to iova %llx\n", dma_map.vaddr, dma_map.size, dma_map.iova);

	ret = ioctl(container, VFIO_IOMMU_MAP_DMA, &data_map);
	if (ret < 0) {
		perror("error in mapping DMA for data");
		return -1;
	}

	uint8_t* data_bytes = (uint8_t*)data_buffer;
	for (int i = 0; i < 4096 / sizeof(int); i++) {
		*(int*)(data_buffer + (i * sizeof(int))) = i;
	}

	// metadata
	h2c_ring_base[3] = 0x10;
	h2c_ring_base[2] = 0;
	h2c_ring_base[1] = 0;
	h2c_ring_base[0] = 0xd;

	// len (4k)
	h2c_ring_base[4] = 0;
	h2c_ring_base[5] = 0x10;

	// reserved
	h2c_ring_base[6] = 0;
	h2c_ring_base[7] = 0;

	// address
	uintptr_t buf_addr = (uintptr_t)(data_buffer);
	h2c_ring_base[8] = (buf_addr >> 0) & 0xFF;
	h2c_ring_base[9] = (buf_addr >> 8) & 0xFF;
	h2c_ring_base[10] = (buf_addr >> 16) & 0xFF;
	h2c_ring_base[11] = (buf_addr >> 24) & 0xFF;
	h2c_ring_base[12] = (buf_addr >> 32) & 0xFF;
	h2c_ring_base[13] = (buf_addr >> 40) & 0xFF;
	h2c_ring_base[14] = (buf_addr >> 48) & 0xFF;
	h2c_ring_base[15] = (buf_addr >> 56) & 0xFF;

	_mm_sfence();

	printf("Descriptor ring before write attempt\n");
	uint8_t* indexing = h2c_ring_base;
	while (indexing < h2c_ring_base + (16 * 4096)) {
		if (*(uint32_t*)(indexing) != 0) {
			printf("0x%08x\n", *(uint32_t*)(indexing));
		}
		indexing += sizeof(uint32_t);
	}

	// advance PIDX
	write_reg(register_space, 0x18004, 0x00001);

	sleep(5);

	printf("Completion ring after write attempt\n");
	indexing = cmplt_ring_base;
	while (indexing < cmplt_ring_base + (16 * 4096)) {
		if (*(uint32_t*)(indexing) != 0) {
			printf("%p: 0x%08x\n", indexing, *(uint32_t*)(indexing));
		}
		indexing += sizeof(uint32_t);
	}

	printf("Descriptor ring after write attempt\n");
	indexing = h2c_ring_base;
	while (indexing < h2c_ring_base + (16 * 4096)) {
		if (*(uint32_t*)(indexing) != 0) {
			printf("%p: 0x%08x\n", indexing, *(uint32_t*)(indexing));
		}
		indexing += sizeof(uint32_t);
	}

	const int QDMA_GLBL_ERR_STAT = 0x248;
	printf("Error status register reports %#x\n", read_reg(register_space, QDMA_GLBL_ERR_STAT));
	printf("The C2H error register reports %#x\n", read_reg(register_space, 0xAF0));
	printf("QDMA_GLBL_DSC_ERR_STS reports %#x\n", read_reg(register_space, 0x254));
	printf("QDMA_GLBL_DSC_ERR_MSK reports %#x\n", read_reg(register_space, 0x258));
	printf("QDMA_GLBL_DSC_ERR_LOG0 reports %#x\n", read_reg(register_space, 0x25c));
	printf("QDMA_GLBL_TRQ_ERR_STS reports %#x\n", read_reg(register_space, 0x264));
	printf("QDMA_H2C_DBG_REG0 reports %#x\n", read_reg(register_space, 0xE0C));
	printf("QDMA_H2C_DBG_REG1 reports %#x\n", read_reg(register_space, 0xE10));
	printf("QDMA_H2C_DBG_REG2 reports %#x\n", read_reg(register_space, 0xE14));


	return 0;
}
